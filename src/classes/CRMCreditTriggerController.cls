/******************************************************************************* 
Name              : CRMCreditTriggerController
Revision History  : TBD
					 
Created/Modified by   Created/Modified Date     Requested by          Related Task/Issue             
----------------------------------------------------------------------------------------
1. Mark Robinson      02/12/2014         	    Jim Horty             
*******************************************************************************/
public class CRMCreditTriggerController {

	public static void linkCRMCreditToSite (List<CRM_Credit__c> crmCredits) {
		List<String> siteIds = new List<String>();
		Map<String, String> siteIdBySiteIDString = new Map<String, String>();
		
		for (CRM_Credit__c crmCredit : crmCredits) {
			 if (crmCredit.Site_ID__c != null) siteIds.add(crmCredit.Site_ID__c);
		}
		
		for (Site__c site : [SELECT Id, Site_ID__c From Site__c WHERE Site_ID__c IN :siteIds]) {
			siteIdBySiteIDString.put(site.Site_ID__c, site.Id);
		}
		
		// SET PARENT SITE
		for (CRM_Credit__c crmCredit : crmCredits) {
			if (crmCredit.Site__c == null) crmCredit.Site__c = siteIdBySiteIDString.get(crmCredit.Site_ID__c);
			
		}				
	}
	
	public static testMethod void linkCRMCreditToSite_Test() {
		Account account = TestingUtils.createAccount('TestAccount', true);
		Site__c site = TestingUtils.createSite(account);
		Business_Contact_Email__c bce = TestingUtils.createBusinessContactEmail(site);
		Cortera_Report_Credit__c crc = TestingUtils.createCorteraReportCredit(site);
		CRM_Credit__c cc = TestingUtils.createCRMCredit(site);
	}
	
}