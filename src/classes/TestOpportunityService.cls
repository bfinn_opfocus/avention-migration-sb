/* Class:       TestOpportunityService
 * Created On:  11/19/2015
 * Created by:  OpFocus Team
 * Description: Unit test for OpportunityService class
 */

@isTest
private class TestOpportunityService
{
	private static final Boolean testCreateQuote = true;
	private static final Boolean testCreateQuoteFailure = true;

	@isTest
	static void testCreateQuote() {
		System.assert(testCreateQuote, 'Test disabled.');
		initData();
		Utils.ButtonResult results = null;
		Test.startTest();
			results = OpportunityService.createQuote(testOpp.Id, testAcct.Id);
		Test.stopTest();

		System.assertEquals(true, results.success, 'Failed to create quote as expected.');
		System.assertEquals(1, [select Id from SBQQ__Quote__c 
			where SBQQ__Opportunity2__c=:testOpp.Id].size(),
			'Failed to create a Quote from our Opportunity.');
	}

	@isTest
	static void testCreateQuoteFailure() {
		System.assert(testCreateQuoteFailure, 'Test disabled.');
		initData();
		Utils.ButtonResult results = null;
		Test.startTest();
			results = OpportunityService.createQuote(testOpp.Id, testOpp.Id);
		Test.stopTest();

		System.assertEquals(false, results.success, 'Created Quote when should have returned an error.');
	}

	private static void initData() {
        Id stdPBId = Test.getStandardPricebookId();

        List<Product2> lstProducts = new List<Product2>();
        lstProducts.add(new Product2(Name = 'Product Name 1',
            ProductCode = '12345', IsActive = true, Family = OrderHelper.DATA_SOLUTIONS, 
            Fair_Value__c='Product Fair Value', Future_Contractual_Obligation__c='Over-the-life',
            Delivery_Method__c='Email'));
        
        lstProducts.add(new Product2(Name = 'Product Name 2',
            ProductCode = '67890', IsActive = true, Family = OrderHelper.DATA_SOLUTIONS, 
            Fair_Value__c='Product1 Fair Value', Future_Contractual_Obligation__c='One-time',
            Delivery_Method__c='USPS'));
        insert lstProducts;

        List<PricebookEntry> lstPbe = new List<PricebookEntry>();
        for (Product2 product : lstProducts) {
        	lstPbe.add(new PricebookEntry(Pricebook2Id=stdPBId, 
	            Product2Id=product.Id, UnitPrice=1000, isActive=true));
        }
        insert lstPbe;

        testAcct = new Account(Name='Test Account');
        insert testAcct;

        testOpp = new Opportunity(Name='Test Opp', StageName='10% - Prospect', 
            CloseDate=Date.today().addDays(30));
        insert testOpp;

        List<OpportunityLineItem> lstOlis = new List<OpportunityLineItem>();
        for (PricebookEntry pbe : lstPbe) {
	        lstOlis.add(new OpportunityLineItem(OpportunityId = testOpp.Id,
	            PricebookEntryId = pbe.Id, Quantity = 1,  
	            TotalPrice = 1 * pbe.UnitPrice));
        }
        insert lstOlis;

    }

    private static Account testAcct;
    private static Opportunity testOpp;
}