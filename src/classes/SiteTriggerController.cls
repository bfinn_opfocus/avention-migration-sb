/******************************************************************************* 
Name              : SiteTriggerController
Revision History  : TBD
					 
Created/Modified by   Created/Modified Date     Requested by          Related Task/Issue             
----------------------------------------------------------------------------------------
1. Mark Robinson      02/12/2014         	    Jim Horty             
*******************************************************************************/
public class SiteTriggerController {

	public static void linkSiteToAccount(List<Site__c> sites) {
		List<String> custNums = new List<String>();
		Map<String, String> accountsByCustNum = new Map<String, String>();
		
		for (Site__c site : sites) {
			if (site.CUSTNUM__c != '') {
				if (site.CUSTNUM__c != null) {
					custNums.add(site.CUSTNUM__c);	
				}
			}
		}
		
		for (Account account : [SELECT Id, PMD_Ship_To_Customer_ID__c FROM Account WHERE PMD_Ship_To_Customer_ID__c IN :custNums]) {
			accountsByCustNum.put(account.PMD_Ship_To_Customer_ID__c, account.Id);
		}
		
		for (Site__c site : sites) {
			if (site.Account__c == null) site.Account__c = accountsByCustNum.get(site.CUSTNUM__c);
		}
	}
	
	public static testMethod void linkSiteToAccount_Test() {
		Account account = TestingUtils.createAccount('TestAccount', true);
		Site__c site = TestingUtils.createSite(account);
	}
}