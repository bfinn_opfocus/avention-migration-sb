public class ContractCreateQuickTrialController {
	
	Contact contact;
	User user;

	public ContractCreateQuickTrialController (ApexPages.StandardController acon) {
		contact = [SELECT
					Id,
					Site_Name__c,
					Product_Calc__c,
					Seats__c,
					SendEmailCalc__c,
					Days__c,
					Exclude_DandB_Worldbase_Companies__c,
					Product__c
				   FROM Contact WHERE Id = :acon.getId() LIMIT 1];
		user = null;
	}
	
	public void doRedirect () {
		
	}
	
}