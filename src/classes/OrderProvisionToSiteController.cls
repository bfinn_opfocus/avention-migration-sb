/******************************************************************************* 
Name              : OrderProvisionToSiteController
Revision History  : Apex controller class used by OrderProvisionToSite VF page.
					 
Created/Modified by   Created/Modified Date     Requested by          Related Task/Issue             
----------------------------------------------------------------------------------------
1. Mark Robinson      04/01/2014         	    John Hughes 
2. Mark Robinson	  07/31/2014				John Hughes			  Updating logic to support new Site_Config object.            
*******************************************************************************/
public class OrderProvisionToSiteController {

	Order__c theOrder;
	List<String> orderProductIds = new List<String>();
	Map<Id, List<Site_Configuration__c>> siteConfigsByOrderProductId;

	public class OrderProvisionException extends Exception {} 
	 
	public OrderProvisionToSiteController(ApexPages.StandardController acon) {
		theOrder = [SELECT Id,
						   Renewal_Order__c,
						   Order_Number__c,
						   Site_Provision_Status__c,
						   Site_Account_PMD_Id__c,
						   Site_Address_1__c,
						   Site_City__c,
						   Site_State__c,
						   Site_Postal_Code__c,
						   Site_Country__c,
						   Payment_Terms__c,
						   Contract_Number__c,
						   Contract_Start_Date__c,
						   Contract_End_Date__c,
						   Site_Account__r.Id,
						   Site_Account__r.Name,
						   Site_Account__r.Fax,
						   Site_Account__r.PMD_Ship_To_Customer_ID__c,
						   System_Administrator__r.Id,
						   System_Administrator__r.Title,
						   System_Administrator__r.FirstName,
						   System_Administrator__r.LastName,
						   System_Administrator_Phone__c,
						   System_Administrator_Email__c,
						   Setup_As_Admin_User__c,
						   Email_Access_Pool_Rollover_for_Renewal__c,
						   Exclude_D_B_Worldbase_Companies__c,
						   (Select Id, Password__c, Users__c, Require_Password__c, Product__r.Id, Sales_Price__c From Order_Products__r)
			        FROM Order__c WHERE ID = :acon.getId() LIMIT 1];
			        
		loadSiteConfigsByOrderProduct();
	}
	
	private void loadSiteConfigsByOrderProduct() {
		siteConfigsByOrderProductId = new Map<Id, List<Site_Configuration__c>>();
		orderProductIds.clear();
		for (Order_Product__c orderProduct : theOrder.Order_Products__r) {
			orderProductIds.add(orderProduct.Id);
		}
		for (Site_Configuration__c siteConfig : [SELECT s.Site__c, s.Seats__c, s.Order_Product__c, s.New_Site_Match_Code__c, s.Name, s.Id, s.Order_Product__r.Users__c, s.Site__r.Id, s.Site__r.Site_ID__c FROM Site_Configuration__c s WHERE Order_Product__c IN :orderProductIds]) {
			List<Site_Configuration__c> siteConfigs;
			if (siteConfigsByOrderProductId.containsKey(siteConfig.Order_Product__c)) {
				siteConfigs = siteConfigsByOrderProductId.get(siteConfig.Order_Product__c);
				siteConfigs.add(siteConfig);
			} else {
				siteConfigs = new List<Site_Configuration__c>();
				siteConfigs.add(siteConfig);
				siteConfigsByOrderProductId.put(siteConfig.Order_Product__c, siteConfigs);
			}
		}
	}
	
	public PageReference doProvisionToSite() {
		system.debug('++++ doProvisionToSite +++');
		
		PageReference retVal;
		
		if (validateFieldData(theOrder)) {
			// CREATE NEW SITE RECORD IF ALL ORDER PRODUCTS HAVE NULL SITE
			//AggregateResult[] groupedResults = [SELECT COUNT(Id)thecount FROM Order_Product__c WHERE Order__c = :theOrder.Id AND Site__c <> ''];
			//Integer countProductIds = integer.valueOf(groupedResults[0].get('thecount'));
			
			//if (1==1){//if (countProductIds == 0) { // INSERT
				logProvisionToSiteInvocation();
				expireSitePrivs(theOrder);
				createNewSite(theOrder);
			//}
			/*else { // UPDATE
				// EXPIRE TRIAL SCHEMAS
				expireSitePrivs(theOrder);
				// UPDATE SITE PRIVS
				createNewSite(theOrder);
			}*/
			retVal = (new ApexPages.StandardController(theOrder)).view();
			
			theOrder.Site_Provision_Status__c = 'In process';
			update theOrder;
		}
		
		return retVal;
	}
	
	private Boolean validateFieldData (Order__c orderToValidate) {
		Boolean retVal = true;
		List<String> productIds = new List<String>();
		
		if (!userHasSiteProvisionAccess( UserInfo.getUserId() )) {
			retVal = false;
			ApexPages.addMessages(new OrderProvisionException('You do not have acess to Site Provision feature.'));
			return retVal;
		}
				
		if (orderToValidate.Site_Account__r.PMD_Ship_To_Customer_ID__c == null) {
			retVal = false;
			ApexPages.addMessages(new OrderProvisionException('PMD Ship To Customer ID is required.'));
		}
		if (orderToValidate.System_Administrator__r.FirstName == null || orderToValidate.System_Administrator__r.LastName == null) {
			retVal = false;
			ApexPages.addMessages(new OrderProvisionException('System Administrator Name is required.'));
		}
		if (orderToValidate.System_Administrator_Email__c == null) {
			retVal = false;
			ApexPages.addMessages(new OrderProvisionException('System Administrator Email is required.'));
		}
		if (orderToValidate.System_Administrator_Phone__c == null) {
			retVal = false;
			ApexPages.addMessages(new OrderProvisionException('System Administrator Phone is required.'));
		}
		if (orderToValidate.Contract_Number__c == null) {
			retVal = false;
			ApexPages.addMessages(new OrderProvisionException('Contract Number is required.'));
		}
		
		// ENSURE ORDER STATUS IS NOT IN PROCESS
		if (orderToValidate.Site_Provision_Status__c == 'In process') {
			retVal = false;
			ApexPages.addMessages(new OrderProvisionException('Site provision in process.'));
			return retVal;			
		}
		// ENSURE ORDER HAS ORDER PRODUCTS		
		if (orderToValidate.Order_Products__r.size() == 0) {
			retVal = false;
			ApexPages.addMessages(new OrderProvisionException('No Order Products exist on Order.'));
			return retVal;			
		}

		// ENSURE PRODUCTS HAVE AT LEAST 1 RELATED SCHEMA RECORD
		for (Order_Product__c orderProduct : orderToValidate.Order_Products__r) {
			productIds.add(orderProduct.Product__r.Id);
		}
		for (Product2 product : [SELECT Id, Name, (Select Id From Product_Tags__r) FROM Product2 WHERE Id IN :productIds]) {
			if (product.Product_Tags__r.size() == 0) {
				retVal = false;
				ApexPages.addMessages(new OrderProvisionException('All Products must have Product Schema: ' + product.Name));				
			} 
		}
		
		// ENSURE AT LEAST 1 SITE CONFIGURATION RECORD EXISTS
		if (siteConfigsByOrderProductId.size() == 0) {
			retVal = false;
			ApexPages.addMessages(new OrderProvisionException('No Site Configurations exist on Order.'));
			return retVal;
		} 
		
		// ENSURE SUM OF SITE CONFIGURATION SEATS EQUALS ORDER_PRODUCT USERS
		Map<Id, Decimal> seatCountByOrderProductId = new Map<Id, Decimal>();
		for (List<Site_Configuration__c> siteConfigs : siteConfigsByOrderProductId.values()) {
			for (Site_Configuration__c siteConfig : siteConfigs) {
				system.debug('++++ Users / Seats : ' + string.valueOf(siteConfig.Order_Product__r.Users__c) + ' ' + string.valueOf(siteConfig.Seats__c) );
				if (seatCountByOrderProductId.containsKey(siteConfig.Order_Product__r.Id)) {
					seatCountByOrderProductId.put(siteConfig.Order_Product__r.Id, seatCountByOrderProductId.get(siteConfig.Order_Product__r.Id) + siteConfig.Seats__c);
				} else {
					seatCountByOrderProductId.put(siteConfig.Order_Product__r.Id, siteConfig.Seats__c);
				}
			}
		}
		for (Order_Product__c orderProduct : this.theOrder.Order_Products__r) {
			if (orderProduct.Users__c != seatCountByOrderProductId.get(orderProduct.Id)) {
				system.debug('++++ User count != Seat count : ' + string.valueOf(orderProduct.Users__c) + ' ' + string.valueOf(seatCountByOrderProductId.get(orderProduct.Id)) );
				retVal = false;
				ApexPages.addMessages(new OrderProvisionException('Site Configuration seat count does not match Order User count.'));
				return retVal;
			} 
		}
	
		system.debug('++++ validateFieldData : ' + retVal);

		return retVal;
	}
	
	private void createNewSite (Order__c orderToProvision) {
		system.debug('++++ createNewSite +++');
		
		String siteAcctNameAndSiteMatchCode = '';
		Set<String> siteMatchCodes = new Set<String>();
		List<Site__c> sitesToCreate = new List<Site__c>();
		Map<String, Site__c> sitesByMatchCode = new Map<String, Site__c>();

		for (List<Site_Configuration__c> siteConfigs : siteConfigsByOrderProductId.values()) {
			for (Site_Configuration__c siteConfig : siteConfigs) {
				if (siteConfig.Site__c == null) {
					siteMatchCodes.add(siteConfig.New_Site_Match_Code__c);
					sitesByMatchCode.put(siteConfig.New_Site_Match_Code__c, null); // new 
				}else {
					//system.debug('+++++++++ MR : ' + siteConfig.New_Site_Match_Code__c);
					//siteMatchCodes.add(siteConfig.New_Site_Match_Code__c);
					sitesByMatchCode.put(siteConfig.New_Site_Match_Code__c, 
									     new Site__c (Id=siteConfig.Site__r.Id, Site_ID__c=siteConfig.Site__r.Site_ID__c)
									     );
				}
			}
		}
		system.debug('+++ SET siteMatchCodes size : ' + siteMatchCodes.size());
		for (String siteMatchCode : sitesByMatchCode.keySet()) {//for (String siteMatchCode : siteMatchCodes) { // new 
			system.debug('+++ sitesByMatchCode.containsKey : ' + sitesByMatchCode.containsKey(siteMatchCode));
			String strSiteId = getNewSiteIDString(
												orderToProvision.Site_Account__r.Name,
												orderToProvision.Contract_Number__c,
												siteMatchCode);
			if (sitesByMatchCode.get(siteMatchCode) != null) strSiteId = sitesByMatchCode.get(siteMatchCode).Site_ID__c; // new
			Site__c siteToCreate;
			system.debug('+++ sitesByMatchCode.get(siteMatchCode) : ' + sitesByMatchCode.get(siteMatchCode));
			system.debug('+++ strSiteId : ' + strSiteId);
			if (sitesByMatchCode.get(siteMatchCode) == null) {
				 siteToCreate = new Site__c (
					Site_ID__c = strSiteId,
					Name = strSiteId,
					Display_Name__c = orderToProvision.Site_Account__r.Name,
					Contact_Name__c = orderToProvision.System_Administrator__r.FirstName + ' ' + orderToProvision.System_Administrator__r.LastName,
					Contact_Title__c = orderToProvision.System_Administrator__r.Title,
					Address__c = orderToProvision.Site_Address_1__c,
					City__c = orderToProvision.Site_City__c,
					Postal_Code__c = orderToProvision.Site_Postal_Code__c,
					State_or_Province__c = orderToProvision.Site_State__c,
					Country__c = orderToProvision.Site_Country__c,
					Phone_Number__c = orderToProvision.System_Administrator_Phone__c,
					Fax_Number__c = orderToProvision.Site_Account__r.Fax,
					Payment_Terms__c = orderToProvision.Payment_Terms__c,
					Email_Address__c = orderToProvision.System_Administrator_Email__c,
					Notes__c = '',
					Require_Domain__c = false,
					Require_Password__c = false,
					Check_Password__c = false,
					CUSTNUM__c = orderToProvision.Site_Account__r.PMD_Ship_To_Customer_ID__c,
					CUSTTYPE__c = 'CLIENT',
					Domain_Registration__c = 0,
					Require_Project__c = 0,
					Require_Workgroup__c = 0,
					Start_Date__c = datetime.newInstance( orderToProvision.Contract_Start_Date__c.year(), 
														  orderToProvision.Contract_Start_Date__c.month(),
														  orderToProvision.Contract_Start_Date__c.day() ) ,
					Site_Allows_Perm_PW__c = 0,
					Silent_Registration__c = false,
					Custom_Registration__c = false,
					Custom_Registration_URL__c = '0',
					Rolling_User_ID__c = false,
					User_Level_Auth__c = false,
					Account__c = orderToProvision.Site_Account__c,
					Contact__c = orderToProvision.System_Administrator__r.Id,
					Provision_Order_Contract_Number__c = orderToProvision.Contract_Number__c
				);
				sitesToCreate.add(siteToCreate);
				sitesByMatchCode.put(siteMatchCode, siteToCreate);
			} else {
				 siteToCreate = new Site__c (
					Site_ID__c = strSiteId,
					Contact_Name__c = orderToProvision.System_Administrator__r.FirstName + ' ' + orderToProvision.System_Administrator__r.LastName,
					Contact_Title__c = orderToProvision.System_Administrator__r.Title,
					Address__c = orderToProvision.Site_Address_1__c,
					City__c = orderToProvision.Site_City__c,
					Postal_Code__c = orderToProvision.Site_Postal_Code__c,
					State_or_Province__c = orderToProvision.Site_State__c,
					Country__c = orderToProvision.Site_Country__c,
					Phone_Number__c = orderToProvision.System_Administrator_Phone__c,
					Fax_Number__c = orderToProvision.Site_Account__r.Fax,
					Payment_Terms__c = orderToProvision.Payment_Terms__c,
					Email_Address__c = orderToProvision.System_Administrator_Email__c,
					Account__c = orderToProvision.Site_Account__c,
					Contact__c = orderToProvision.System_Administrator__r.Id,
					Provision_Order_Contract_Number__c = orderToProvision.Contract_Number__c
				);
				sitesToCreate.add(siteToCreate);
				sitesByMatchCode.put(siteMatchCode, siteToCreate);
			}
			
			
		}
		upsert sitesToCreate Site_ID__c;
		
		// ASSIGN SITE TO SITE CONFIGURATION
		List<Site_Configuration__c> siteConfigsToUpdate = new List<Site_Configuration__c>();
		for (List<Site_Configuration__c> siteConfigs : siteConfigsByOrderProductId.values()) {
			for (Site_Configuration__c siteConfig : siteConfigs) {
				system.debug('sitesByMatchCode.get(siteConfig.New_Site_Match_Code__c).Id : ' + sitesByMatchCode.get(siteConfig.New_Site_Match_Code__c).Id);
				siteConfig.Site__c = sitesByMatchCode.get(siteConfig.New_Site_Match_Code__c).Id;
				siteConfigsToUpdate.add(siteConfig);
			}
		}
		update siteConfigsToUpdate;
		
		loadSiteConfigsByOrderProduct();
		
		createNewSitePrivs(orderToProvision, sitesByMatchCode);
	}
	
	private void createNewSitePrivs(Order__c orderToProvision, Map<String, Site__c> sitesByMatchCode) {
		system.debug('++++ createNewSitePriv +++');
		
		List<String> productIds = new List<String>();
		List<Product_Schema__c> productSchemas = new List<Product_Schema__c>();
		Map<String, Order_Product__c> orderProductsByProductId = new Map<String, Order_Product__c>();
		Map<String, Site_Priv__c> sitePrivsToCreateMap = new Map<String, Site_Priv__c>();
		List<String> strKeys = new List<String>();
		Map<String, Double> allowedUsersBySitePrivKey = new Map<String, Double>();
		
		for (Order_Product__c orderProduct : orderToProvision.Order_Products__r) {
			system.debug('+++ orderProduct.Product__r.Id : ' + orderProduct.Product__r.Id);
			productIds.add(orderProduct.Product__r.Id);
			orderProductsByProductId.put(orderProduct.Product__r.Id, orderProduct);
		}
		system.debug('+++ productIds.size : ' + productIds.size());
		
		productSchemas.addAll(
			[SELECT
			Product__c,
			Schema_Name__c,
			Call_Add_Invite_Proc__c,
			Call_Credit_Pool_Update_Proc__c,
			Insert_Site_Priv_Record__c,
			Exclude_D_B_Worldbase_Companies__c
			FROM Product_Schema__c WHERE Product__c IN :productIds AND Insert_Site_Priv_Record__c = true]
		);
		
		// COLLECT PERSISTED SITE PRIVS 
		for (List<Site_Configuration__c> siteConfigs : siteConfigsByOrderProductId.values()) {
			for (Site_Configuration__c siteConfig : siteConfigs) {
				for (Product_Schema__c productSchema : productSchemas) {
					String strKey = siteConfig.Site__r.Site_ID__c + '_' + productSchema.Schema_Name__c;
					strKeys.add(strKey);
				}
			}
		}
		for (Site_Priv__c sp : [SELECT Key__c, Allowed_Users__c FROM Site_Priv__c WHERE Key__c IN :strKeys]) {
			allowedUsersBySitePrivKey.put(sp.Key__c, sp.Allowed_Users__c);
		}

		for (List<Site_Configuration__c> siteConfigs : siteConfigsByOrderProductId.values()) {
			for (Site_Configuration__c siteConfig : siteConfigs) {
				for (Product_Schema__c productSchema : productSchemas) {
					
					system.debug('siteConfig.Site__c : ' + siteConfig.Site__c);
					
					if (siteConfig.Order_Product__c == orderProductsByProductId.get(productSchema.Product__c).Id) {
						String strKey = siteConfig.Site__r.Site_ID__c + '_' + productSchema.Schema_Name__c;
						Site_Priv__c newSitePriv =
							new Site_Priv__c (
								Key__c = strKey,
								Schema_Name__c = productSchema.Schema_Name__c,
								Password__c = orderProductsByProductId.get(productSchema.Product__c).Password__c, 
								Allocated_Users__c = 0,
								Require_Password__c = orderProductsByProductId.get(productSchema.Product__c).Require_Password__c,
								Active_Till__c = datetime.newInstance( orderToProvision.Contract_End_Date__c.year(), 
																	   orderToProvision.Contract_End_Date__c.month(),
																	   orderToProvision.Contract_End_Date__c.day() ).addDays(1) ,
								CUSTTYPE__c = 'CLIENT',
								Email__c = orderToProvision.System_Administrator_Email__c,
								Site__c = siteConfig.Site__r.Id,
								Call_Add_Invite_Proc__c = productSchema.Call_Add_Invite_Proc__c,
								Call_Credit_Pool_Update_Proc__c = productSchema.Call_Credit_Pool_Update_Proc__c,
								Insert_Site_Priv_Record__c = productSchema.Insert_Site_Priv_Record__c,
								Credit_Pool_Users__c = orderProductsByProductId.get(productSchema.Product__c).Users__c,
								AdminLevel__c = orderToProvision.Setup_As_Admin_User__c == 'YES' ? 1 : 0,
								Keep_Current_Balance__c = orderToProvision.Email_Access_Pool_Rollover_for_Renewal__c,
								Sales_Price__c = orderProductsByProductId.get(productSchema.Product__c).Sales_Price__c,
								Site_ID__c = siteConfig.Site__r.Site_ID__c
							);
						// POPULATE ALLOWED USERS
						if (sitePrivsToCreateMap.containsKey(strKey))
							sitePrivsToCreateMap.get(strKey).Allowed_Users__c += siteConfig.Seats__c;
						else
							newSitePriv.Allowed_Users__c = siteConfig.Seats__c;
							
						if (allowedUsersBySitePrivKey.containsKey(strKey)) {
							system.debug('+++ Renewal : ' + theOrder.Renewal_Order__c);  
							system.debug('+++ Renewal : ' + newSitePriv.Schema_Name__c); 
							system.debug('+++ Renewal : ' + newSitePriv.Allowed_Users__c);
							system.debug('+++ Renewal : ' + allowedUsersBySitePrivKey.get(strKey));
							if (theOrder.Renewal_Order__c != 'Yes') {
								if (newSitePriv.Allowed_Users__c != null) 
									newSitePriv.Allowed_Users__c = allowedUsersBySitePrivKey.get(strKey) + newSitePriv.Allowed_Users__c;
							}
						}
						
						// SPECIAL CONSIDERATIONS
						if (productSchema.Call_Credit_Pool_Update_Proc__c) newSitePriv.Allowed_Users__c = -1;
						
						if (productSchema.Exclude_D_B_Worldbase_Companies__c && orderToProvision.Exclude_D_B_Worldbase_Companies__c == 'Yes') {
							if (!sitePrivsToCreateMap.containsKey(strKey)) sitePrivsToCreateMap.put(strKey, newSitePriv);
						} else if (!productSchema.Exclude_D_B_Worldbase_Companies__c) {
							if (!sitePrivsToCreateMap.containsKey(strKey)) sitePrivsToCreateMap.put(strKey, newSitePriv);
						}
					}

				}
			}
		}
		
		upsert sitePrivsToCreateMap.values() Key__c;
	}
	
	private void expireSitePrivs(Order__c orderToProvision) {
		// V 1.1
		system.debug('++++ expireSitePrivs +++');
		
		Datetime expireDate = datetime.now().addDays(-1);
		
		List<String> productIds = new List<String>();
		List<Product_Schema__c> productSchemas = new List<Product_Schema__c>();
		Map<String, Order_Product__c> orderProductsByProductId = new Map<String, Order_Product__c>();
		List<Site_Priv__c> sitePrivsToExpire = new List<Site_Priv__c>();
		List<String> sitePrivKeys = new List<String>();
		Map<String, String> siteIDStringsByProductId = new Map<String, String>();

		Map<String, Product_Schema__c> orderProductBySchemaName = new Map<String, Product_Schema__c>();
		Map<String, Site_Priv__c> sitePrivsBySchemaName = new Map<String, Site_Priv__c>();
		Map<String, Double> nbrOfUsersBySchemaName = new Map<String, Double>();
		List<Site_Priv__c> sitePrivsToExpireOrExtend = new List<Site_Priv__c>();
		Set<String> siteIds = new Set<String>();
		List<String> siteIdList = new List<String>();

		// COLLECT PERSISTED SITES
		for (Order_Product__c orderProduct : [SELECT  Product__r.Id, Users__c, (select Id, Site__r.Site_ID__c from site_configurations__r)
											  FROM Order_Product__c
											  WHERE Order__c = :orderToProvision.Id]) {
			for (Site_Configuration__c siteConfig : orderProduct.Site_Configurations__r) {
				if (orderProduct.Site_Configurations__r != null) siteIds.add(siteConfig.Site__r.Site_ID__c);	
			}
		}
		siteIdList.addAll(siteIds);
		
		for (String theSiteIDString : siteIdList) {
			system.debug('+++ theSiteIDString : ' + theSiteIDString);
			
			// V1 CODE
			for (Order_Product__c orderProduct : [SELECT Product__r.Id, Product__r.Name, Users__c, (select Id, Site__r.Site_ID__c from site_configurations__r) FROM Order_Product__c
											      WHERE Order__r.Site_Account__c = :orderToProvision.Site_Account__r.Id
											      AND Order__r.Contract_End_Date__c > TODAY]){
				for (Site_Configuration__c siteConfig : orderProduct.Site_Configurations__r) {
					if (siteConfig.Site__r.Site_ID__c == theSiteIDString) {
						//system.debug('++++ orderProduct : ' + orderProduct.Product__r.Name);
						productIds.add(orderProduct.Product__r.Id);
						orderProductsByProductId.put(orderProduct.Product__r.Id, orderProduct);
						siteIDStringsByProductId.put(orderProduct.Product__r.Id, siteConfig.Site__r.Site_ID__c);					
					}					
				}
			}
			
			productSchemas.addAll(
				[SELECT
				Product__c,
				Schema_Name__c,
				Call_Add_Invite_Proc__c,
				Call_Credit_Pool_Update_Proc__c,
				Insert_Site_Priv_Record__c,
				Exclude_D_B_Worldbase_Companies__c
				FROM Product_Schema__c WHERE Product__c IN :productIds]
			);
			
			for (Product_Schema__c productSchema : productSchemas) {
				sitePrivKeys.add(  siteIDStringsByProductId.get(productSchema.Product__c) + '_' + productSchema.Schema_Name__c );
				orderProductBySchemaName.put(productSchema.Schema_Name__c, productSchema);
			}
			
			for (Site_Priv__c sitePriv : [SELECT Id, Schema_Name__c FROM Site_Priv__c WHERE Site_ID__c = :theSiteIDString ]) { // Site-79181
				if (!orderProductBySchemaName.containsKey(sitePriv.Schema_Name__c)) {
					system.debug('+++ expiring ' + sitePriv.Schema_Name__c);
					sitePriv.Active_Till__c = expireDate;
					sitePrivsToExpireOrExtend.add(sitePriv);
				} /* 8/20/2014 - Mark Robinson - code block was designed to reset Active Til dates on existing
											     Tags / Site Privs
				  else {
					system.debug('+++ extending ' + sitePriv.Schema_Name__c);		
					sitePriv.Active_Till__c = datetime.newInstance( orderToProvision.Contract_End_Date__c.year(), 
															   		orderToProvision.Contract_End_Date__c.month(),
															   		orderToProvision.Contract_End_Date__c.day() ).addDays(1);
					sitePrivsToExpireOrExtend.add(sitePriv);
				}*/
			}
			// V1 CODE
					
		}
		
		update sitePrivsToExpireOrExtend;

		
		/*
		List<String> productIds = new List<String>();
		List<Product_Schema__c> productSchemas = new List<Product_Schema__c>();
		Map<String, Order_Product__c> orderProductsByProductId = new Map<String, Order_Product__c>();
		Map<String, Site_Priv__c> sitePrivsToCreateMap = new Map<String, Site_Priv__c>();
		List<String> strKeys = new List<String>();
		Map<String, Double> allowedUsersBySitePrivKey = new Map<String, Double>();
		
		for (Order_Product__c orderProduct : orderToProvision.Order_Products__r) {
			system.debug('+++ orderProduct.Product__r.Id : ' + orderProduct.Product__r.Id);
			productIds.add(orderProduct.Product__r.Id);
			orderProductsByProductId.put(orderProduct.Product__r.Id, orderProduct);
		}
		system.debug('+++ productIds.size : ' + productIds.size());
		
		productSchemas.addAll(
			[SELECT
			Product__c,
			Schema_Name__c,
			Call_Add_Invite_Proc__c,
			Call_Credit_Pool_Update_Proc__c,
			Insert_Site_Priv_Record__c,
			Exclude_D_B_Worldbase_Companies__c
			FROM Product_Schema__c WHERE Product__c IN :productIds AND Insert_Site_Priv_Record__c = true]
		);
		
		// COLLECT PERSISTED SITE PRIVS 
		for (List<Site_Configuration__c> siteConfigs : siteConfigsByOrderProductId.values()) {
			for (Site_Configuration__c siteConfig : siteConfigs) {
				for (Product_Schema__c productSchema : productSchemas) {
					String strKey = siteConfig.Site__r.Site_ID__c + '_' + productSchema.Schema_Name__c;
					strKeys.add(strKey);
				}
			}
		}
		for (Site_Priv__c sp : [SELECT Key__c, Allowed_Users__c FROM Site_Priv__c WHERE Key__c IN :strKeys]) {
			allowedUsersBySitePrivKey.put(sp.Key__c, sp.Allowed_Users__c);
		}

		for (List<Site_Configuration__c> siteConfigs : siteConfigsByOrderProductId.values()) {
			for (Site_Configuration__c siteConfig : siteConfigs) {
				for (Product_Schema__c productSchema : productSchemas) {
					
					system.debug('siteConfig.Site__c : ' + siteConfig.Site__c);
					
					if (siteConfig.Order_Product__c == orderProductsByProductId.get(productSchema.Product__c).Id) {
						String strKey = siteConfig.Site__r.Site_ID__c + '_' + productSchema.Schema_Name__c;
						Site_Priv__c newSitePriv =
							new Site_Priv__c (
								Key__c = strKey,
								Schema_Name__c = productSchema.Schema_Name__c,
								Password__c = orderProductsByProductId.get(productSchema.Product__c).Password__c, 
								Allocated_Users__c = 0,
								Require_Password__c = orderProductsByProductId.get(productSchema.Product__c).Require_Password__c,
								Active_Till__c = datetime.newInstance( orderToProvision.Contract_End_Date__c.year(), 
																	   orderToProvision.Contract_End_Date__c.month(),
																	   orderToProvision.Contract_End_Date__c.day() ).addDays(1) ,
								CUSTTYPE__c = 'CLIENT',
								Email__c = orderToProvision.System_Administrator_Email__c,
								Site__c = siteConfig.Site__r.Id,
								Call_Add_Invite_Proc__c = productSchema.Call_Add_Invite_Proc__c,
								Call_Credit_Pool_Update_Proc__c = productSchema.Call_Credit_Pool_Update_Proc__c,
								Insert_Site_Priv_Record__c = productSchema.Insert_Site_Priv_Record__c,
								Credit_Pool_Users__c = orderProductsByProductId.get(productSchema.Product__c).Users__c,
								AdminLevel__c = orderToProvision.Setup_As_Admin_User__c == 'YES' ? 1 : 0,
								Keep_Current_Balance__c = orderToProvision.Email_Access_Pool_Rollover_for_Renewal__c,
								Sales_Price__c = orderProductsByProductId.get(productSchema.Product__c).Sales_Price__c,
								Site_ID__c = siteConfig.Site__r.Site_ID__c
							);
						// POPULATE ALLOWED USERS
						if (sitePrivsToCreateMap.containsKey(strKey))
							sitePrivsToCreateMap.get(strKey).Allowed_Users__c += siteConfig.Seats__c;
						else
							newSitePriv.Allowed_Users__c = siteConfig.Seats__c;
							
						if (allowedUsersBySitePrivKey.containsKey(strKey)) {

						}
						

					}

				}
			}
		}
		*/
		
		/*
		system.debug('++++ expireSitePrivs +++');
		
		Datetime expireDate = datetime.now().addDays(-1);
		
		List<String> productIds = new List<String>();
		List<Product_Schema__c> productSchemas = new List<Product_Schema__c>();
		Map<String, Order_Product__c> orderProductsByProductId = new Map<String, Order_Product__c>();
		List<Site_Priv__c> sitePrivsToExpire = new List<Site_Priv__c>();
		List<String> sitePrivKeys = new List<String>();
		Map<String, String> siteIDStringsByProductId = new Map<String, String>();

		Map<String, Product_Schema__c> orderProductBySchemaName = new Map<String, Product_Schema__c>();
		Map<String, Site_Priv__c> sitePrivsBySchemaName = new Map<String, Site_Priv__c>();
		Map<String, Double> nbrOfUsersBySchemaName = new Map<String, Double>();
		List<Site_Priv__c> sitePrivsToExpireOrExtend = new List<Site_Priv__c>();
		Set<String> siteIds = new Set<String>();
		List<String> siteIdList = new List<String>();

		// COLLECT PERSISTED SITES 
		for (List<Site_Configuration__c> siteConfigs : siteConfigsByOrderProductId.values()) {
			for (Site_Configuration__c siteConfig : siteConfigs) {
				siteIds.add(siteConfig.Site__r.Id);
			}
		}
		siteIdList.addAll(siteIds);
		
		for (Order_Product__c orderProduct : [SELECT Site__r.Site_ID__c, Product__r.Id, Users__c FROM Order_Product__c
											  WHERE Order__r.Site_Account__c = :orderToProvision.Site_Account__r.Id
											  AND Order__r.Contract_End_Date__c > TODAY
										      AND Site__c = :siteIdList[0] ]) { // Site-79181
				
				productIds.add(orderProduct.Product__r.Id);
				orderProductsByProductId.put(orderProduct.Product__r.Id, orderProduct);
				siteIDStringsByProductId.put(orderProduct.Product__r.Id, orderProduct.Site__r.Site_ID__c);
		}
		
		productSchemas.addAll(
			[SELECT
			Product__c,
			Schema_Name__c,
			Call_Add_Invite_Proc__c,
			Call_Credit_Pool_Update_Proc__c,
			Insert_Site_Priv_Record__c,
			Exclude_D_B_Worldbase_Companies__c
			FROM Product_Schema__c WHERE Product__c IN :productIds]
		);
		
		for (Product_Schema__c productSchema : productSchemas) {
			sitePrivKeys.add(  siteIDStringsByProductId.get(productSchema.Product__c) + '_' + productSchema.Schema_Name__c );
			orderProductBySchemaName.put(productSchema.Schema_Name__c, productSchema);
		}
		
		for (Site_Priv__c sitePriv : [SELECT Id, Schema_Name__c FROM Site_Priv__c WHERE Site__c = :siteIdList[0] ]) { // Site-79181
			if (!orderProductBySchemaName.containsKey(sitePriv.Schema_Name__c)) {
				system.debug('+++ expiring ' + sitePriv.Schema_Name__c);
				sitePriv.Active_Till__c = expireDate;
				sitePrivsToExpireOrExtend.add(sitePriv);
			} else {
				system.debug('+++ extending ' + sitePriv.Schema_Name__c);		
				sitePriv.Active_Till__c = datetime.newInstance( orderToProvision.Contract_End_Date__c.year(), 
														   		orderToProvision.Contract_End_Date__c.month(),
														   		orderToProvision.Contract_End_Date__c.day() ).addDays(1);
				sitePrivsToExpireOrExtend.add(sitePriv);
			}
		}
		
		update sitePrivsToExpireOrExtend;
	*/	
	}
	
	private string getNewSiteIDString(String siteAccountName, String contractNumber, String siteMatchCode) {
		String retVal = '';
		siteAccountName = siteAccountName.replaceAll('\\s+', '');
		Integer contractAndSiteMatchCodeLength = contractNumber.length() + siteMatchCode.length();
		
		if (siteAccountName.length() > 20 - contractAndSiteMatchCodeLength)
			retVal = siteAccountName.substring(0, 20 - contractAndSiteMatchCodeLength) + contractNumber + siteMatchCode;
		else
			retVal = siteAccountName + contractNumber + siteMatchCode;
		
		return retVal;
	}
	
	private Boolean userHasSiteProvisionAccess(string userId) {
		return [SELECT Site_Provision_Access__c FROM User WHERE Id = :userId LIMIT 1].Site_Provision_Access__c;
	}
	
	private void logProvisionToSiteInvocation() {
		Task tsk = new Task (
		                     WhatId = theOrder.Id,
		                     Subject = 'Provision To Site Clicked',
		                     Status = 'Completed',
		                     ActivityDate = Date.today(),
		                     Description = 'Provision to site invoked on ' + string.valueOf(Date.today()) + ' by ' + UserInfo.getFirstName() + ' ' + UserInfo.getLastName() + '.',
							 Type='Note'
		                     );
		insert tsk;
	}
	
}