public with sharing class AccountTypeJob implements database.batchable<sobject>, Schedulable, Database.Stateful {
    
    public static final string JOB_NAME = 'AccountTypeJob';
    public static final string ACTIVE_STATUS = 'Customer - Current';
    public static final string FORMER_STATUS = 'Customer - Former';
    public static final string ACTIVE = 'Active';

    public static final string PARTNER = 'Partner';
    public static final string RESELLER = 'Reseller';
    
    public Map<ID, String> invalidIDToMessageMap;
    
    public AccountTypeJob(){}
    
    public void execute(SchedulableContext ctx){
        AccountTypeJob b = new AccountTypeJob(); 
        database.executebatch(b);
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC){
        // only query those accounts that are not Partner or Reseller
    	invalidIDToMessageMap = new Map<ID, String>();
        return Database.getQueryLocator([Select Id, type, 
            (Select Id From Orders4__r limit 1),
            (Select Id From Orders2__r limit 1) 
            From Account 
            where Type != :PARTNER AND Type != :RESELLER
            order by LastModifiedDate desc]);
    }
    
    public void execute(Database.BatchableContext BC, List<account> accounts){
        List<Account> accountsToUpdate = new List<Account>();
        List<Account> accountsToQuery = new List<Account>();
        Integer size = accounts.size();
        for(Integer i = 0; i < size ; i++ ) {
            Account a = accounts.get(i);
            if(a.Orders4__r.isEmpty() && a.Orders2__r.isEmpty()) {
                if(a.type == ACTIVE_STATUS) {
                    a.type = FORMER_STATUS;
                    accountsToUpdate.add(a);
                }
            } else {
                accountsToQuery.add(a);
            }
        }
        
        List<Account> accountsWithActiveOrders = [Select Id, type,
                                    (Select Id From Orders4__r where Status__c =: ACTIVE limit 1), 
                                    (Select Id From Orders2__r where Status__c =: ACTIVE limit 1) 
                                 From Account where ID IN: accountsToQuery];
        
        for(Account a : accountsWithActiveOrders) {
            if(a.Orders4__r.isEmpty() && a.Orders2__r.isEmpty()) {
                if(a.type != FORMER_STATUS) {
                    a.type = FORMER_STATUS;
                    accountsToUpdate.add(a);
                }
            } else {
                if(a.type != ACTIVE_STATUS) {
                    a.type = ACTIVE_STATUS;
                    accountsToUpdate.add(a);
                }
            }
        }
        if(!accountsToUpdate.isEmpty()) {
	        List<Database.SaveResult> results = Database.update(accountsToUpdate, false);
	        for(Database.Saveresult res : results){
	            for(Database.error error : res.getErrors()){
	                invalidIDToMessageMap.put(res.getID(), error.getMessage());
	            }
	        }
        }
        
    }
    
    public void finish(Database.BatchableContext info){     
       if(!invalidIDToMessageMap.isEmpty()) {
          List<Messaging.Singleemailmessage> messages = buildErrorEmail(invalidIDToMessageMap);
          if(!messages.isEmpty()) {
              Messaging.sendEmail(messages);
          }
       }
       AccountTypeJob scheduledJob = new AccountTypeJob();
       system.scheduleBatch(scheduledJob, JOB_NAME + String.valueof(Datetime.now().getTime()), 60);
    }
    
    public static List<Messaging.SingleEmailmessage> buildErrorEmail(Map<Id, String> invalidIDToMessageMap ){

        List<Messaging.Singleemailmessage> messagesToReturn = new List<Messaging.Singleemailmessage>();
        
        Batch_Job_Parameters__c params =  Batch_Job_Parameters__c.getValues(JOB_NAME);
        
        if(params != null && params.Emails_To_Notify__c != null) {
            String emailBody = 'The ' + JOB_NAME +  ' batch job failed on the following cases:';
            for(ID caseID : invalidIDToMessageMap.keyset()){
                 emailBody += '/n ' + caseID + ' ' + invalidIDToMessageMap.get(caseID);  
            }
            List<String> toAddresses = (params != null) ? params.Emails_To_Notify__c.split(',') : new List<String>() ;
            Messaging.SingleEmailmessage errorEmail = new Messaging.Singleemailmessage();
            errorEmail.setToAddresses(toAddresses);
            errorEmail.setHtmlBody(emailBody);
            messagesToReturn.add(errorEmail);
        }
            
        return messagesToReturn;
        
    }
    
}