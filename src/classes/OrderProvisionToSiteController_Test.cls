@isTest
public class OrderProvisionToSiteController_Test {
	
	static testMethod void OrderProvisionToSiteController_Test() {
    	Id profileId = [SELECT Id FROM Profile WHERE Name='System Administrator' LIMIT 1].Id;
    	User testingUser = TestingUtils.createUser('Test', 'User', profileId, true);
    	
		Account account = TestingUtils.createAccount('TestAccount', true);
		Contact contact = TestingUtils.createContact('Jon', 'Doe', account.Id, true);
		Site__c site = TestingUtils.createSite(account);
		
		Order__c theOrder = new Order__c(
						   Site_Account__c=account.Id,
						   Site_Provision_Status__c='Active',
						   Site_Address_1__c='123 Street',
						   Site_City__c='Boston',
						   Site_State__c='MA',
						   Site_Postal_Code__c='02108',
						   Site_Country__c='USA',
						   Payment_Terms__c='Annual',
						   Contract_Number__c='44311',
						   Contract_Start_Date__c=date.today(),
						   Contract_End_Date__c=date.today(),
						   System_Administrator__c=contact.Id,
						   System_Administrator_Phone__c='413-555-1234',
						   System_Administrator_Email__c='jdoe@gmail.com',
						   Setup_As_Admin_User__c='Yes',
						   Email_Access_Pool_Rollover_for_Renewal__c=false,
						   Exclude_D_B_Worldbase_Companies__c='No'
		);
		insert theOrder;
		
		Product2 product = createProduct();
		Order_Product__c orderProduct = new Order_Product__c(
			Order__c=theOrder.Id, 
			Product__c=product.Id,
			//Site__c=site.Id,
			Users__c=2
		);
		insert orderProduct;
		
		Site_Configuration__c sc = new Site_Configuration__c(Order_Product__c=orderProduct.Id, New_Site_Match_code__c='1', Seats__c=2, Key__c='OrderProduct_SiteConfig');
		insert sc;
		
		
		/**********************/
		/*
		Order__c theOrder2 = new Order__c(
						   Site_Account__c=account.Id,
						   Site_Provision_Status__c='Active',
						   Site_Address_1__c='123 Street',
						   Site_City__c='Boston',
						   Site_State__c='MA',
						   Site_Postal_Code__c='02108',
						   Site_Country__c='USA',
						   Payment_Terms__c='Annual',
						   Contract_Number__c='2',
						   Contract_Start_Date__c=date.today(),
						   Contract_End_Date__c=date.today(),
						   System_Administrator__c=contact.Id,
						   System_Administrator_Phone__c='413-555-1234',
						   System_Administrator_Email__c='jdoe@gmail.com',
						   Setup_As_Admin_User__c='Yes',
						   Email_Access_Pool_Rollover_for_Renewal__c=false,
						   Exclude_D_B_Worldbase_Companies__c='No'
		);
		insert theOrder2;
		
		Order_Product__c orderProduct2 = new Order_Product__c(
			Order__c=theOrder.Id, 
			Product__c=product.Id,
			Users__c=2
		);
		insert orderProduct2;
		*/
		
		/* ORDER 1 */
    	System.runAs(testingUser) {
    		system.assert(testingUser.Site_Provision_Access__c);
	    	ApexPages.StandardController orderController = new ApexPages.StandardController(theOrder);
	    	OrderProvisionToSiteController extOrderProvisionToSite = new OrderProvisionToSiteController(orderController);
	    	extOrderProvisionToSite.doProvisionToSite();
    	}
    	
    	//orderProduct.Site__c = null;
    	//update orderProduct;
    	
    	/* ORDER 2 */
    	/*System.runAs(testingUser) {
    		system.assert(testingUser.Site_Provision_Access__c);
	    	ApexPages.StandardController orderController = new ApexPages.StandardController(theOrder2);
	    	OrderProvisionToSiteController extOrderProvisionToSite = new OrderProvisionToSiteController(orderController);
	    	extOrderProvisionToSite.doProvisionToSite();
    	}*/
    	
		/* TEST VALIDATE ORDER */
		/*theOrder.System_Administrator__c = null;
		update theOrder;
     	System.runAs(testingUser) {
    		system.assert(testingUser.Site_Provision_Access__c);
	    	ApexPages.StandardController orderController = new ApexPages.StandardController(theOrder);
	    	OrderProvisionToSiteController extOrderProvisionToSite = new OrderProvisionToSiteController(orderController);
	    	extOrderProvisionToSite.doProvisionToSite();
    	}
    	
    	theOrder.System_Administrator__c=contact.Id;
		theOrder.Site_Provision_Status__c = 'In process';
		update theOrder;
     	System.runAs(testingUser) {
    		system.assert(testingUser.Site_Provision_Access__c);
	    	ApexPages.StandardController orderController = new ApexPages.StandardController(theOrder);
	    	OrderProvisionToSiteController extOrderProvisionToSite = new OrderProvisionToSiteController(orderController);
	    	extOrderProvisionToSite.doProvisionToSite();
    	}
    	*/    	
    	   	
	}
	
	
	static testMethod Product2 createProduct() {
		Product2 product = new Product2(Name='AVENTION - North America', Family='Avention', ProductCode='100562', IsActive=true);
		insert product;
		
		Product_Schema__c productSchema = new Product_Schema__c(Schema_Name__c='OSNP', Schema_Display_Name__c='AVENTION', Product__c=product.Id);
		insert productSchema;
		
		return product; 
	}

}