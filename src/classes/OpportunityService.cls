/* Class:       OpportunityService
 * Created On:  10/26/2015
 * Created by:  OpFocus Team
 * Description: Provides web service methods for the Opportunity object. Right now, the only
 *  	operation supported is creation of a Quote
 */
global class OpportunityService {
	// Create a new Steelbrick Quote for the given Opportunity 
	WebService static Utils.ButtonResult createQuote(Id oppId, Id acctId) {
		try {
			// now make sure our Account's Country value is valid - if not, this will throw a validation
			// exception so we can return an error to user before creating quote.
			Account acct = new Account(Id = acctId);
			update acct; // don't actually update anything but if invalid data, it will complain.

			// if we got this far, then account country is valid - create our quote and return.
			// set the Expiration Date on Quote to be 1 month from today
			SBQQ__Quote__c quote = new SBQQ__Quote__c(SBQQ__Opportunity2__c = oppId,
				SBQQ__Account__c = acctId, SBQQ__ExpirationDate__c = Date.today().addMonths(1));
			insert quote;
			return new Utils.ButtonResult(true, '', quote.Id);
		} catch (Exception e) {
			String errorMsg = 'Error creating Quote for Opportunity ' + oppId + '. Error Details = ' + 
				e.getMessage();
			// if this is a validation error for Account's Invalid Country, return a nicer message
			if (e.getMessage().containsIgnoreCase('FIELD_CUSTOM_VALIDATION_EXCEPTION')) {
				if (e.getMessage().containsIgnoreCase('Invalid Country')) {
					errorMsg = 'Error creating Quote for Opportunity. The Opportunity\'s Account does not have a valid Country value. ' + 
						'Please correct it and retry.';
				} else if (e.getMessage().containsIgnoreCase('Invalid State')) {
					errorMsg = 'Error creating Quote for Opportunity. The Opportunity\'s Account does not have a valid State value. ' + 
						'Please correct it and retry.';
				}
			}
			System.debug('========> ' + errorMsg);
			return new Utils.ButtonResult(false, errorMsg);			
		}
	} 
}