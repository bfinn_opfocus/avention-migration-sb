@isTest
private class TestOpportunitySchedulesController {
	
	@isTest static void opportunityScheduleController_ut1() {
		
        Account acc = new Account(Name='Test Account');
        insert acc;

        Opportunity opp = new Opportunity();
        opp.Name = 'Test Opportunity ';
        opp.StageName = '10% - Prospect';
        opp.CloseDate = System.today();
        opp.AccountId = acc.Id;
        insert opp;

        List<Product2> products = TestingUtils.generateProducts(10, false);
        
        for (Product2 p : products) {
            p.Family += 'Subscription';
            p.Subscription_Term__c = '3 years';
            p.CanUseRevenueSchedule = true;
        }

        insert products;
        
    // Generate standard prices (note Test.getStandardPricebookId())
        Map<Id, Decimal> standardProductPrices = TestingUtils.generateProductPrices(products, 5000);
        List<PricebookEntry> standardEntries = TestingUtils.generatePricebookEntries(standardProductPrices, Test.getStandardPricebookId(), true);   

        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;

        Map<Id, Decimal> customProductPrices = TestingUtils.generateProductPrices(products, 15000);
        List<PricebookEntry> customEntries = TestingUtils.generatePricebookEntries(customProductPrices, customPB.Id, true);   
        OpportunityLineItem oppLineItem = new OpportunityLineItem();
        oppLineItem.Quantity = 1;
        oppLineItem.UnitPrice = 5000;
        oppLineItem.OpportunityId = opp.Id;
        oppLineItem.PricebookEntryId = customEntries[0].Id;
        insert oppLineItem;     

        List<OpportunityLineItemSchedule> schedules = [Select Id, ScheduleDate, Revenue, Type from OpportunityLineItemSchedule];
        System.debug('schedules ' + schedules);
        System.assertNotEquals(null,schedules);

        Test.startTest();

        	ApexPages.StandardController con = new ApexPages.StandardController(oppLineItem);
        	OpportunitySchedulesController testExt = new OpportunitySchedulesController(con);
        	System.assertNotEquals(null,testExt.schedules);
        	System.assertNotEquals(0,testExt.totalAmount);

        	testExt.doEdit();
        	System.assertEquals(true,testExt.isEdit);
        	testExt.doCancel();	
        	System.assertEquals(false,testExt.isEdit);

        	testExt.schedules[0].Revenue = 200;
        	testExt.recalculate();

        	testExt.doSave();

        Test.stopTest();

	}
	
}