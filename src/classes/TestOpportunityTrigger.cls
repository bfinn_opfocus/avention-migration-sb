/*
 * class:  TestOpportunityTrigger
 * Created by OpFocus on 04/23/2015
 * Description: Test Class for the OpportunityTrigger
 *              
 */
@isTest
private class TestOpportunityTrigger {
    
    
    // Standard Unit Test for testing the Opportunity Trigger that runs and builds the Opportunity Splits
    
    @isTest static void opportunityTrigger_ut1() {
		
		// Load and create test data
        Account acc = new Account(Name='Test Account');
        insert acc;
		
        Opportunity opp = new Opportunity();
        opp.Name = 'Test Opportunity ';
        opp.StageName = '10% - Prospect';
        opp.CloseDate = System.today();
        opp.AccountId = acc.Id;
        insert opp;
        
        // Insert new User for Manager
        User manager = TestingUtils.createUser('Manager','Test',UserInfo.getProfileId(),false);
        insert manager;
        
        //Update the ManagerId on the currentUser.	
        User currentUser = new User(Id=UserInfo.getUserId(),ManagerId=manager.Id);
        update currentUser;

		
		// Build products, pricebooks, pricebookentries
        List<Product2> products = TestingUtils.generateProducts(10, false);
        
        for (Product2 p : products) {
            p.Family += 'Subscription';
            p.Subscription_Term__c = '3 years';
            p.CanUseRevenueSchedule = true;
        }

        insert products;
        
    // Generate standard prices (note Test.getStandardPricebookId())
        Map<Id, Decimal> standardProductPrices = TestingUtils.generateProductPrices(products, 5000);
        List<PricebookEntry> standardEntries = TestingUtils.generatePricebookEntries(standardProductPrices, Test.getStandardPricebookId(), true);   

        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;

        Map<Id, Decimal> customProductPrices = TestingUtils.generateProductPrices(products, 15000);
        List<PricebookEntry> customEntries = TestingUtils.generatePricebookEntries(customProductPrices, customPB.Id, true);   

        OpportunityLineItem oppLineItem = new OpportunityLineItem();
        oppLineItem.Quantity = 1;
        oppLineItem.UnitPrice = 5000;
        oppLineItem.OpportunityId = opp.Id;
        oppLineItem.PricebookEntryId = customEntries[0].Id;
        insert oppLineItem;     

		List<OpportunityLineItemSchedule> schedules = [Select Id, ScheduleDate, Revenue, Type from OpportunityLineItemSchedule];
        System.debug('schedules ' + schedules);
        System.assertNotEquals(null,schedules);

		Test.startTest();
			opp = [Select Id, Name, Amount, StageName, Total_Yr_1_Amount__c from Opportunity where Id = :opp.Id];
			opp.StageName = '100% - Closed Won';
        	opp.Amount = 100000;
			update opp; // This will fire the trigger to create multiple Opportunity Split records
			
			List<OpportunitySplit> splits = Database.query('Select ' + SchemaUtils.getFieldCSV(OpportunitySplit.SObjectType) + ' from OpportunitySplit');
			
			System.assertNotEquals(null,splits);
	
        Test.stopTest();

        
    }

}