/* Class:       OrderHelper
 * Created On:  05/16/2015
 * Created by:  OpFocus Team
 * Description: Helper class for Order object. For use with Order triggers and Order controllers.
 */
public with sharing class OrderHelper {
    public static Boolean blnAlreadyInsideTrigger = false;
    
    public static final String SUBSCRIPTION   = 'Subscription';
    public static final String DATA_SOLUTIONS = 'Data Solutions';

    // When an Order is created from a Steelbrick Quote, we need to copy the QLIs over
    // BFS 11/20/2015 : Updated to add a conditional prior to copying a QLI to an Order Product.
    // If Product is part of a Bundle, then it should not be copied over. Here is how
    // we identify Bundled Products 
    //     1. Product.Configure Type = Required 
    //     2. Product.Configure Event = Always
    public static void copyQLIsToOrderProducts(List<Order__c> lstOrders) {
        blnAlreadyInsideTrigger = true;

        List<Id> lstQuoteIdsToQuery = new List<Id>();
        Map<Id, Order__c> mapOrderByQuoteId = new Map<Id, Order__c>();
        for (Order__c order : lstOrders) {
            lstQuoteIdsToQuery.add(order.Quote__c);
            mapOrderByQuoteId.put(order.Quote__c, order);
        }

        // now get the list of QLIs for this Order's Quote and walk through to 
        // see if we have any duplicate Products
        Map<String, List<SBQQ__QuoteLine__c>> mapQLIsByProductCode = new Map<String, List<SBQQ__QuoteLine__c>>();
        for (SBQQ__QuoteLine__c quoteLineItem : [select Id, SBQQ__Quote__c, 
                SBQQ__Quote__r.SBQQ__PaymentTerms__c, SBQQ__Product__c, SBQQ__NetPrice__c,
                SBQQ__ProductCode__c, SBQQ__Quantity__c, SBQQ__Number__c, 
                Future_Contractual_Obligation__c, Delivery_Method__c,
                Fair_Value__c, Order_Product_Name__c from SBQQ__QuoteLine__c 
                where SBQQ__Quote__c in:lstQuoteIdsToQuery and 
                SBQQ__Product__r.SBQQ__ConfigurationType__c != 'Required' and
                SBQQ__Product__r.SBQQ__ConfigurationEvent__c != 'Always']) {
            if (mapQLIsByProductCode.get(quoteLineItem.SBQQ__ProductCode__c) == null) {
                mapQLIsByProductCode.put(quoteLineItem.SBQQ__ProductCode__c, new List<SBQQ__QuoteLine__c>());
            }
            mapQLIsByProductCode.get(quoteLineItem.SBQQ__ProductCode__c).add(quoteLineItem);
        }

        List<Order_Product__c> lstProductsToCreate = new List<Order_Product__c>();
        // now lets copy and merge where needed QLIs over to Order Products
        for (String productCode : mapQLIsByProductCode.keyset())
        {
            List<SBQQ__QuoteLine__c> lstQlisForSameProduct = mapQLIsByProductCode.get(productCode);
            // For each duplicate QLI, we need to aggregate the Value together and create a single
            // Order Product for all of the QLIs.
            System.debug('======> There are '  + lstQlisForSameProduct.size() + 
                ' QLIs with same product code = '  + productCode);
            Decimal aggregatedPrice = 0.0;
            for (SBQQ__QuoteLine__c quoteLineItem : lstQlisForSameProduct) {
                System.debug('========> adding ' + quoteLineItem.SBQQ__NetPrice__c + ' to aggregatedPrice.');
                aggregatedPrice += quoteLineItem.SBQQ__NetPrice__c;
            }
            System.debug('========> Aggregated Price for productCode ' + productCode + ' = ' + 
                aggregatedPrice);
            // now copy all other values from a single QLI as we are only changing amount
            SBQQ__QuoteLine__c quoteLineItem = lstQlisForSameProduct.get(0);
            Order__c order = mapOrderByQuoteId.get(quoteLineItem.SBQQ__Quote__c);
            Order_Product__c orderProduct = new Order_Product__c(Order__c=order.Id,
                Sales_Price__c=aggregatedPrice, 
                PMD_Product_Code__c=quoteLineItem.SBQQ__ProductCode__c,
                Product__c=quoteLineItem.SBQQ__Product__c,
                Contract_Line_Number__c=quoteLineItem.SBQQ__Number__c,
                Fair_Value__c=quoteLineItem.Fair_Value__c,
                Future_Contractual_Obligation__c=quoteLineItem.Future_Contractual_Obligation__c,
                Delivery_Method__c=quoteLineItem.Delivery_Method__c,
                Payment_Terms__c=quoteLineItem.SBQQ__Quote__r.SBQQ__PaymentTerms__c,
                Contract_Line_Amount__c=aggregatedPrice,
                Users__c=quoteLineItem.SBQQ__Quantity__c,
                Name=quoteLineItem.Order_Product_Name__c);
            lstProductsToCreate.add(orderProduct);
        }
        if (!lstProductsToCreate.isEmpty()) {
            insert lstProductsToCreate;

            List<Order__c> lstOrdersToUpdate = new List<Order__c>();
            // now reset our copy flag for all those Orders that we performed copy & merge
            for (Order__c order : mapOrderByQuoteId.values()) {
                lstOrdersToUpdate.add(new Order__c(Id=order.Id, Copy_and_Merge_QLIs__c = false));
            }
            update lstOrdersToUpdate;
        }
    }

    // When an Order has been set ready for billing, it has been sent to Intacct and we need 
    // to mark its associated Opportunity as Closed/Won - Finance.
    // 1. If Order is created by Order Schedule, the Auto-Close process would work the same 
    // as it does now. (Orders created from an Order Schedule have a SFDC_520_Quote__c value)
    // 2. If the Order is created by a Quote (its Quote__c value will be non-null), 
    // then the Auto-Close process will work as follows
    //      a. update the Opportunity Stage as “Stage 6 Closed Won” 
    //      b. set the Close Date same as is true now
    //      c. DO NOT copy Order Products to Opportunity Line Items
    // If both Quote__c and SFDC_520_Quote__c are both defined, Quote__c rules (Rule #2) take precedence
  public static void handleOrdersForBilling( Map<Id, Order__c> mapOrdersForQuoteByOppId, 
        Map<Id, Order__c> mapOrdersForOSByOppId) {

        List<Id> lstOppIds = new List<Id>(mapOrdersForQuoteByOppId.keyset());
        lstOppIds.addAll(mapOrdersForOSByOppId.keyset());
        List<Opportunity> lstOpps = [select Id, StageName, Amount 
            from Opportunity where Id in: lstOppIds];

        // first set the close date, and subscription dates for all Opportunities
        for (Opportunity opp : lstOpps) {
            Order__c order = mapOrdersForOSByOppId.get(opp.Id);
            if (order == null) {
                order = mapOrdersForQuoteByOppId.get(opp.Id);
            }
            System.debug('======> Updating Opportunity Dates. Setting Subscription Start Date = ' + 
                order.Contract_Start_Date__c + ' and Subscription End Date = ' + 
                order.Contract_End_Date__c);
            opp.StageName = 'Stage 6 Closed Won';
            calculateCloseDate(opp, order);

            // Map Contract Start/End Date to Subscription Start/End Date
            opp.Subscription_Start_Date__c = order.Contract_Start_Date__c;
            opp.Subscription_End_Date__c = order.Contract_End_Date__c;
        }

        List<Id> lstOrderIds = new List<Id>();
        for (Order__c order : mapOrdersForOSByOppId.values()) {
            lstOrderIds.add(order.Id);
        }
        System.debug('==========> Getting Order Products.');
        List<Order_Product__c> lstOrderProducts = [select Sales_Price__c, Users__c, 
            Product__r.ProductCode, Product__r.Family, Order__r.Id, Id 
            from Order_Product__c where Order__r.Id in :mapOrdersForOSByOppId.values()];

/** IF AND WHEN REVENUE SCHEDULES GET REINSTATED, USE THIS QUERY INSTEAD
        List<Order_Product__c> lstOrderProducts = [select Sales_Price__c, Users__c, 
            Product__r.ProductCode, Product__r.Family, Product__r.Subscription_Term__c, Order__r.Id, Id 
            from Order_Product__c where Order__r.Id in :mapOrdersForOSByOppId.values()];
*/

        List<OpportunityLineItem> lstOlis = [select UnitPrice, TotalPrice, Quantity, 
            Opportunity.Id, Id, ProductCode, Discount, ListPrice
            from OpportunityLineItem where OpportunityId in :mapOrdersForOSByOppId.keyset()];
        Map<Id, List<Order_Product__c>> mapOrderProductsByOrderId = new Map<Id, List<Order_Product__c>>();

        System.debug('==========> There are ' + lstOrderProducts + ' for all Orders');
        for (Order_Product__c orderProduct : lstOrderProducts) {
            System.debug('==========> Searching for Order Products for Order ' + orderProduct.Order__r.Id);
            if (mapOrderProductsByOrderId.get(orderProduct.Order__r.Id) == null) {
                System.debug('==========> Putting List in map for Order ' + orderProduct.Order__r.Id);
                mapOrderProductsByOrderId.put(orderProduct.Order__r.Id, new List<Order_Product__c>());
            }
            System.debug('==========> Adding OrderProduct ' + orderProduct.Id + 
                ' into list of OrderProducts for Order ' + orderProduct.Order__r.Id);
            mapOrderProductsByOrderId.get(orderProduct.Order__r.Id).add(orderProduct);
        }   
        // get a map of OLIs keyed by Opportunity Id
        Map<Id, List<OpportunityLineItem>> mapOlisByOppId = new Map<Id, List<OpportunityLineItem>>();
        List<Id> lstOliIds = new List<Id>();
        System.debug('==========> there are ' + lstOlis.size() + ' OLIs.');
        for (OpportunityLineItem oli : lstOlis) {
            if (mapOlisByOppId.get(oli.Opportunity.Id) == null) {
                mapOlisByOppId.put(oli.Opportunity.Id, new List<OpportunityLineItem>());
            }
            mapOlisByOppId.get(oli.Opportunity.Id).add(oli);
            lstOliIds.add(oli.Id);
        }   

        // get OLI Revenue Schedules
        /** REINSTATE THIS CODE IF AND WHEN REVENUE SCHEDULES ARE RE-ADDED
        Map<Id, List<OpportunityLineItemSchedule>> mapOliSchedulesByOliId = new Map<Id, List<OpportunityLineItemSchedule>>();           
        for (OpportunityLineItemSchedule schedule : [SELECT Revenue, ScheduleDate, OpportunityLineItemId 
            from OpportunityLineItemSchedule 
            where OpportunityLineItemId in :lstOliIds order by OpportunityLineItemId, ScheduleDate ASC]) {
            System.debug('==========> Creating entry in mapOliSchedulesByOliId for OLI ' + schedule.OpportunityLineItemId);
            if (mapOliSchedulesByOliId.get(schedule.OpportunityLineItemId) == null) {
                mapOliSchedulesByOliId.put(schedule.OpportunityLineItemId, new List<OpportunityLineItemSchedule>());
            }
            mapOliSchedulesByOliId.get(schedule.OpportunityLineItemId).add(schedule);
        }
        **/

        List<OpportunityLineItem> lstOlisToUpdate = new List<OpportunityLineItem>();
        Map<Id, List<OpportunityLineItem>> mapOlisToAdjust = 
            new Map<Id, List<OpportunityLineItem>>();
        /** REINSTATE THIS CODE IF AND WHEN REVENUE SCHEDULES ARE RE-ADDED
        List<OpportunityLineItemSchedule> lstSchedulesToUpdate = new List<OpportunityLineItemSchedule>();
        **/
        for (Opportunity opp : lstOpps) {
            Order__c order = mapOrdersForOSByOppId.get(opp.Id);
            if (order == null) {
                continue; // this Opportunity is for an Order created by a Quote so do not copy products
            }
            
            // need to make sure all Order Products exist on Opportunity with same values for ListPrice.
            List<OpportunityLineItem> lstOlisForThisOpp = mapOlisByOppId.get(opp.Id);

            System.debug('==========> Getting List of Order Products for Order ' + 
                order.Id  + ' mapOrderProductsByOrderId = ' + mapOrderProductsByOrderId);
            if (mapOrderProductsByOrderId.get(order.Id) == null) {
                // there are no Products for this Order - move on to next Order.
                System.debug('==========> There are no Order Products for Order ' + 
                    order.Name + '. Not auto-closing Opportunity.');
                order.addError('There are no Order Products for this Order. Please create at least one and resubmit to Intacct.');              
                continue;
            }
            List<Order_Product__c> lstOrderProductsForOrder = mapOrderProductsByOrderId.get(order.Id);
            // this should not happen but if it does, we need to alert user to the error and 
            // abort the Auto-Close operation.
            System.debug('==========> lstOrderProductsForOrder = ' + 
                lstOrderProductsForOrder + ' and there are ' + 
                    (lstOrderProductsForOrder.size() == 0 ? '0' : lstOrderProductsForOrder.size() + 
                        ' order products.'));

            if (lstOrderProductsForOrder == null || lstOrderProductsForOrder.isEmpty()) {
                // there are no Products for this Order - move on to next Order.
                System.debug('==========> There are no Order Products for Order ' + 
                    order.Name + '. Not auto-closing Opportunity.');
                order.addError('There are no Order Products for this Order. Please create at least one and resubmit to Intacct.');
                return;
            }
            // make a map of our Olis keyed off Opp Id for use when adjusting amounts.
            // we want to inlude all Olis, not just those that match an Order Product
            for (OpportunityLineItem oli : lstOlisForThisOpp) {
                if (mapOlisToAdjust.get(opp.Id) == null) {
                    mapOlisToAdjust.put(opp.Id, new List<OpportunityLineItem>());
                }
                mapOlisToAdjust.get(opp.Id).add(oli);               
            }

            for (Order_Product__c orderProduct : lstOrderProductsForOrder) {
                Boolean blnFoundMatchingOli = false;
                System.debug('========> Searching for OrderProduct ' + orderProduct.Product__r.ProductCode);
                for (OpportunityLineItem oli : lstOlisForThisOpp) {
                    if (orderProduct.Product__r.ProductCode.equals(oli.ProductCode)) {
                        // now determine what to do based on ProductCode's SubscriptionTerm and
                        // ProductFamily.
                        /* If Opportunity Schedules are reinstated, this code should be reinstated.
                         ** OpFocus 8/6/2015
                        if (orderProduct.Product__r.Family.contains(SUBSCRIPTION)) {
                            System.debug('==========> Inside if clause for Subscription Product Family for Order Product ' +
                                orderProduct.Id);
                            // get list of schedules for this OLI.
                            System.debug('==========> Getting OliSchedules for OLI Id = ' + oli.Id);
                            List<OpportunityLineItemSchedule> lstSchedules = mapOliSchedulesByOliId.get(oli.Id);
                            // if this OLI does not have any schedules, just continue to next OLI                       
                            if (lstSchedules == null) {
                                continue;
                            } 
                            Integer numSchedules = lstSchedules.size();
                            if (numSchedules == 1) {
                                handle1YearSubscription(orderProduct, lstSchedules, lstSchedulesToUpdate);
                            } else if (numSchedules == 2) {
                                handle2YearSubscription(orderProduct, lstSchedules, lstSchedulesToUpdate);
                            } else {
                                handle3YearSubscription(orderProduct, lstSchedules, lstSchedulesToUpdate);
                            }
                        } else { */
                        // for all other product families
                        // found the Order Product in OLI list.
                        if (orderProduct.Sales_Price__c == 0.0) {
                            System.debug('=========> Skipping this order Product and moving on to next one.');
                            blnFoundMatchingOli = true;
                            break; // skip to next OLI as we cannot create an OLI with 0 salesprice
                        }
                        // only set TotalPrice to null if UnitPrice is changing
                        if (oli.UnitPrice != orderProduct.Sales_Price__c) {
                            System.debug('========> Setting TotalPrice to Null because changing Unit Price from ' +
                                oli.UnitPrice + ' to ' + orderProduct.Sales_Price__c + ' for OLI ' + oli.Id);
                            oli.TotalPrice = null; // must be nulled out when UnitPrice is set
                        }
                        oli.UnitPrice = orderProduct.Sales_Price__c;
                        oli.Discount = 0.0;
                        lstOlisToUpdate.add(oli);
                        blnFoundMatchingOli = true;
                        break; // break out of inner loop
                    }
                } 
                if (!blnFoundMatchingOli) {
                    String errorMsg = 'Failed to find a matching Opportunity Product for Order Product (ProductCode=' + 
                        orderProduct.Product__r.ProductCode + ') on Order ' + order.Name + 
                        '. Ignoring this Order Product.';
                    System.debug('==========> ERROR : ' + errorMsg);
                }
            }
        }
        
        // now update the opportunities in the database.
        if (!lstOpps.isEmpty()) {
            System.debug('======> Updating Opportunities. There are ' + lstOpps.size() + ' to update.');
            update lstOpps;
        }
        if (!lstOlisToUpdate.isEmpty()) {
            for (OpportunityLineItem oli : lstOlisToUpdate) {
                System.debug('========> updating oli = ' + oli);
            }
            update lstOlisToUpdate;
        }
        // now get the Opportunity with updated Amount. Need to do updates on OLIs as above
        // will commit changes to UnitPrice which will trigger update to Opp Amount.
        lstOpps = [select Id, Amount from Opportunity where Id in :lstOpps];
        // need to reconcile the Order Amount & Opportunity Amount
        reconcileAmounts(lstOpps, mapOlisToAdjust, mapOrdersForOSByOppId);

        /** REVENUE SCHEDULE LOGIC. PLEASE REINSTATE IF AND WHEN REVENUE SCHEDULES ARE USED
        if (!lstSchedulesToUpdate.isEmpty()) {
            update lstSchedulesToUpdate;
        }
        **/
    }

    // Need to reconcile the Amounts on Opportunity and Order. If there is a discrepancy, 
    // follow this logic
    //  For each Order Product that was copied over,
    // 1. Compare the Order Sales Price with the Opportunity Amount. 
    // 2. If there is a discrepancy, divide the difference evenly amongst
    //      the OLIs.  
    // This is ONLY done for OLIs that have a corresponding Order Product which has already
    //      benn handled before invoking this method.
    // OpFocus 8/25/2015
    private static void reconcileAmounts(List<Opportunity> lstOpps, 
            Map<Id, List<OpportunityLineItem>> mapOlisToAdjust,
            Map<Id, Order__c> mapOrdersByOppId) {
        List<Opportunity> lstOppsToUpdate = new List<Opportunity>();
        List<OpportunityLineItem> lstOlisToUpdate = new List<OpportunityLineItem>();

        for (Opportunity opp : lstOpps) {
            Order__c order = mapOrdersByOppId.get(opp.Id);
            if (order == null) {
                continue; // BFS 11/19 -  this means this Order was created by Quote so do nothing
            }
            List<OpportunityLineItem> lstOlis = mapOlisToAdjust.get(opp.Id);
            System.debug('==========> Order Amount = ' + order.Order_Product_Sales_Amount__c + ', Opp Amount = ' + 
                opp.Amount);
            if (order.Order_Product_Sales_Amount__c == opp.Amount) {
                System.debug('==========> Opportunity Amount matches Order Amount. Doing nothing.');
                continue; // nothing to do
            }
            Decimal amountDiff = opp.Amount - order.Order_Product_Sales_Amount__c;
            Integer numOlis = lstOlis.size();
            Decimal amountToAdd = amountDiff.divide(numOlis,2);
            System.debug('==========> There is a difference of ' + amountDiff + 
                ' between Order and Opportunity Amounts. Adding ' + amountToAdd + ' to each OLI.');

            Decimal newAmount = 0.0;
            for (OpportunityLineItem oli : lstOlis) {
                oli.UnitPrice -= amountToAdd;
                System.debug('==========> Setting unitPrice = ' + oli.UnitPrice);
                newAmount += oli.UnitPrice;
                lstOlisToUpdate.add(oli);
            }

            // if the amount did not divide evenly, reconcile it here
            if (newAmount != order.Order_Product_Sales_Amount__c) {
                // find the difference left after dividing evenly and apply to first Oli.
                Decimal diff = newAmount - order.Order_Product_Sales_Amount__c;
                OpportunitylineItem oli = lstOlisToUpdate.get(0);
                System.debug('==========> Subtracting ' + diff + ' from OLI unitPrice '  +
                    oli.UnitPrice);
                oli.UnitPrice -= diff;
                newAmount -= diff;              
            }
            System.debug('==========> New Opportunity Amount = ' + newAmount);
            opp.Amount = newAmount;
            lstOppsToUpdate.add(opp);
        }
        if (!lstOppsToUpdate.isEmpty()) {
            update lstOppsToUpdate;
        }
        if (!lstOlisToUpdate.isEmpty()) {
            update lstOlisToUpdate;
        }
    }

    private  static void calculateCloseDate(Opportunity opp, Order__c order) {
        // Set Close Date. 8/21/2015 : follow these
        // 1. If Override_Start_Date__c is not null, set Close Date to it.
        // 2. If Contract Start Date is in a prevous month, CloseDate is Intacct Order Created Date 
        // 3. If Contract Start Date is in current month, CloseDate should be ContractStartDate
        // 4. If Contract Start Date is in future month, CloseDate should be 1 day before ContractStartDate
        
        System.debug('==========> order.Override_Start_Date__c = ' + order.Override_Start_Date__c + 
            'order.Contract_Start_Date__c = ' + order.Contract_Start_Date__c + ' order.Order_Created_Date__c = ' + 
             order.Order_Created_Date__c);

        if (order.Override_Start_Date__c != null) { // (rule 1) 
            opp.CloseDate = order.Override_Start_Date__c;
        } else if (order.Contract_Start_Date__c != null) {
            // if Contract Start Date is in the past (rule 2) : either 
            // 1. previous year and hence month 
            // 2. current year & previous month 
            if (order.Contract_Start_Date__c.year() < Date.today().year() || 
                (order.Contract_Start_Date__c.year() == Date.today().year() && 
                    order.Contract_Start_Date__c.month() < Date.today().month())) {
                system.debug('==========> date is in the past: using Created_Date__c (or today if null): ' + 
                        (order.Order_Created_Date__c == null ? 
                        Date.today() : order.Order_Created_Date__c));
                    opp.CloseDate = (order.Order_Created_Date__c == null ? Date.today() : 
                        order.Order_Created_Date__c.date());
            } else if (order.Contract_Start_Date__c.month() == Date.today().month()) { // rule 3 
                system.debug('==========> date is in same month : using Contract_Start_Date : ' + 
                    order.Contract_Start_Date__c);
                opp.CloseDate = order.Contract_Start_Date__c;
            } else if (order.Contract_Start_Date__c.year() > Date.today().year() || 
                order.Contract_Start_Date__c.month() > Date.today().month()) { // rule 4
                system.debug('==========> date is in the future : using Contract_Start_Date-1 : ' + 
                    order.Contract_Start_Date__c.addDays(-1));
                    opp.CloseDate = order.Contract_Start_Date__c.addDays(-1);
            }
        } else {
            // if Contract_Start_Date is not set which should not happen, set CloseDate to today by default
            System.debug('==========> Contract_Start_Date is not set so using today ' +  Date.today());       
            opp.CloseDate = Date.today();
        }
    }

    /** REVENUE SCHEDULE LOGIC. PLEASE REINSTATE IF AND WHEN REVENUE SCHEDULES ARE USED
    private static void handle1YearSubscription(Order_Product__c orderProduct, 
        List<OpportunityLineItemSchedule> lstSchedules, List<OpportunityLineItemSchedule> lstSchedulesToUpdate ) {

        for (OpportunityLineItemSchedule schedule : lstSchedules) {
            // update scheduled amount listed against the product for which the 
            // code matches. Amount should be adjusted over the number of years
            // for subscription
            System.debug('==========> Initializing scheduleRevenue = ' + orderProduct.Sales_Price__c);
            schedule.Revenue = orderProduct.Sales_Price__c.setScale(3);
            lstSchedulesToUpdate.add(schedule);                     
        }
    }

    private static void handle2YearSubscription(Order_Product__c orderProduct, 
        List<OpportunityLineItemSchedule> lstSchedules, List<OpportunityLineItemSchedule> lstSchedulesToUpdate ) {

        Integer scheduleIdx = 1;
        for (OpportunityLineItemSchedule schedule : lstSchedules) {
            // update scheduled amount listed against the product for which the 
            // code matches. Amount should be adjusted over the number of years
            // for subscription
            // Default schedule revenue to Order Sales Price for 1 year Subscription
            System.debug('==========> Initializing scheduleRevenue = ' + orderProduct.Sales_Price__c);
            Decimal scheduleRevenue = orderProduct.Sales_Price__c;
            if (scheduleIdx == 1) {
                scheduleRevenue = (scheduleRevenue * 95/195).setScale(3);
            } else if (scheduleIdx == 2) {
                scheduleRevenue = (scheduleRevenue-(scheduleRevenue*95/195)).setScale(3);
            }       
            scheduleIdx++;
            schedule.Revenue = scheduleRevenue;
            lstSchedulesToUpdate.add(schedule);                     
        }
    }


    private static void handle3YearSubscription(Order_Product__c orderProduct, 
        List<OpportunityLineItemSchedule> lstSchedules, List<OpportunityLineItemSchedule> lstSchedulesToUpdate ) {
        Integer scheduleIdx = 1;
        Decimal firstYearSchedule = 0.0;
        Decimal secondYearSchedule = 0.0;
        for (OpportunityLineItemSchedule schedule : lstSchedules) {
            // update scheduled amount listed against the product for which the 
            // code matches. Amount should be adjusted over the number of years
            // for subscription
            // Default schedule revenue to Order Sales Price for 1 year Subscription
            System.debug('==========> Initializing scheduleRevenue = ' + orderProduct.Sales_Price__c);
            Decimal scheduleRevenue = orderProduct.Sales_Price__c;
            if (scheduleIdx == 1) {
                scheduleRevenue = (scheduleRevenue*90/288).setScale(3);
                firstYearSchedule = scheduleRevenue;
                System.debug('==========> Setting scheduleRevenue = ' + scheduleRevenue);
            } else if (scheduleIdx == 2) {
                scheduleRevenue = (scheduleRevenue*96/288).setScale(3);
                secondYearSchedule = scheduleRevenue;
            } else if (scheduleIdx == 3) {
                scheduleRevenue = (scheduleRevenue-firstYearSchedule-secondYearSchedule).setScale(3);
            }
            scheduleIdx++;  
            schedule.Revenue = scheduleRevenue;
            lstSchedulesToUpdate.add(schedule);                     
        }
    }
    **/
}