@isTest
private class AccountTypeJobTest {

    static testMethod void AccountTypeJob_Test() {
        List<Account> accounts =  TestingUtils.createAccounts(48, 'Accounts with Bill orders', false);
        UnitTest.addData(accounts).tag('Accounts').divide(4).
            part('Customer Active Accounts').part('Customer Former Accounts')
            .part('Partner Accounts').part('Reseller Accounts');
        UnitTest.get('Customer Active Accounts').property('Type').assignFrom(new List<String>{AccountTypeJob.Active_STATUS})
                                                .divide(4).part('Customer Active Account with Bill Orders')
                                                .part('Customer Active Account with Site Orders').part('Customer Active with Both Orders')
                                                .part('Customer Active Account without Orders');
                                                
        UnitTest.get('Customer Former Accounts').property('Type').assignFrom(new List<String>{AccountTypeJob.FORMER_STATUS}).
                                                divide(4).part('Customer Former Account with Bill Orders')
                                               .part('Customer Former Account with Site Orders').part('Customer Former Account with Both Orders')
                                               .part('Customer Former Account without Orders');

        UnitTest.get('Partner Accounts').property('Type').assignFrom(new List<String>{AccountTypeJob.PARTNER}).
                                                divide(4).part('Partner Account with Bill Orders')
                                               .part('Partner Account with Site Orders').part('Partner Account with Both Orders')
                                               .part('Partner Account without Orders');

        UnitTest.get('Reseller Accounts').property('Type').assignFrom(new List<String>{AccountTypeJob.RESELLER}).
                                                divide(4).part('Reseller Account with Bill Orders')
                                               .part('Reseller Account with Site Orders').part('Reseller Account with Both Orders')
                                               .part('Reseller Account without Orders');

        /*
        List<Account> accounts =  TestingUtils.createAccounts(32, 'Accounts with Bill orders', false);
        UnitTest.addData(accounts).tag('Accounts').divide(2).part('Customer Active Accounts').part('Customer Former Accounts');
        UnitTest.get('Customer Active Accounts').property('Type').assignFrom(new List<String>{AccountTypeJob.Active_STATUS})
                                                .divide(4).part('Customer Active Account with Bill Orders')
                                                .part('Customer Active Account with Site Orders').part('Customer Active with Both Orders')
                                                .part('Customer Active Account without Orders');
                                                
        UnitTest.get('Customer Former Accounts').property('Type').assignFrom(new List<String>{AccountTypeJob.FORMER_STATUS}).
                                                divide(4).part('Customer Former Account with Bill Orders')
                                               .part('Customer Former Account with Site Orders').part('Customer Former Account with Both Orders')
                                               .part('Customer Former Account without Orders');
        */

        UnitTest.get('Accounts').insertAll();
        
        UnitTest.get('Customer Active Account with Site Orders').tag('Accounts with Site Orders');
        UnitTest.get('Customer Former Account with Site Orders').tag('Accounts with Site Orders');
        UnitTest.get('Customer Active Account with Bill Orders').tag('Accounts with Bill Orders');
        UnitTest.get('Customer Former Account with Bill Orders').tag('Accounts with Bill Orders');
        UnitTest.get('Customer Active with Both Orders').tag('Accounts with Site Orders').tag('Accounts with Bill Orders');
        UnitTest.get('Customer Former Account with Both Orders').tag('Accounts with Site Orders').tag('Accounts with Bill Orders');
        
        UnitTest.get('Customer Former Account without Orders').tag('Expected Former Accounts');
        UnitTest.get('Customer Active Account without Orders').tag('Expected Former Accounts');

        UnitTest.get('Partner Account with Site Orders').tag('Expected Partner Accounts');
        UnitTest.get('Reseller Account with Site Orders').tag('Expected Reseller Accounts');
        UnitTest.get('Partner Account with Bill Orders').tag('Expected Partner Accounts');
        UnitTest.get('Reseller Account with Bill Orders').tag('Expected Partner Accounts');
        UnitTest.get('Partner Account with Both Orders').tag('Expected Partner Accounts');
        UnitTest.get('Reseller Account with Both Orders').tag('Expected Partner Accounts');

        UnitTest.get('Partner Account without Orders').tag('Expected Partner Accounts');
        UnitTest.get('Reseller Account without Orders').tag('Expected Reseller Accounts');

        UnitTest.get('Accounts').minus('Expected Partner Accounts').
            minus('Expected Reseller Accounts').minus('Expected Former Accounts').tag('Expected Current Accounts');
        
        List<Order__c> ordersToInsert = new List<Order__c>();
        
        for(Account a : (List<Account>) UnitTest.get('Accounts with Site Orders').getList() ){
            Order__c o = TestingUtils.createOrdersCustom(1, 'o' + a.id, false)[0];
            o.Site_Account__c = a.id;
            o.Order_Status__c = 'Approved';
            o.Subscription_End_Date__c = Date.today().addDays(1);
            ordersToInsert.add(o);
        }
        for(Account a : (List<Account>) UnitTest.get('Accounts with Bill Orders').getList()){
            Order__c o = TestingUtils.createOrdersCustom(1, 'o' + a.id, false)[0];
            o.Order_Status__c = 'Approved';
            o.Subscription_End_Date__c = Date.today().addDays(1);
            o.Bill_To_Account__c = a.id;
            ordersToInsert.add(o);
        }
        for(Account a : (List<Account>) UnitTest.get('Customer Former Account with Both Orders').getList()){
            Order__c o = TestingUtils.createOrdersCustom(1, 'o' + a.id, false)[0];
            o.Order_Status__c = 'Approved';
            o.Subscription_End_Date__c = Date.today().addDays(1);
            o.Bill_To_Account__c = a.id;
            o.Site_Account__c = a.id;
            ordersToInsert.add(o);
        }
        
        insert ordersToInsert;
        
        
        for ( CronTrigger ct : [select Id, CronExpression from CronTrigger WHERE NextFireTime != null]) {
            System.abortJob(ct.Id);
        }
        
        Test.startTest();
            AccountTypeJob job = new AccountTypeJob();
            Database.executeBatch(job); 
        Test.stopTest();
        
        Map<Id, Account> accountsMap = new Map<Id, Account>([Select Id, type from Account where ID IN : UnitTest.getIds('Accounts')]);
        
        System.AssertEquals(((List<Account>) UnitTest.get('Accounts').getList()).size(), accountsMap.keyset().size(), 'An incorrect number of Accounts have been returned');
        for(Account a : (List<Account>) UnitTest.get('Expected Current Accounts').getList()){
            System.AssertEquals(AccountTypeJob.ACTIVE_STATUS, accountsMap.get(a.id).type, 'This account has not been set to Active correctly');
        }
        for(Account a : (List<Account>) UnitTest.get('Expected Former Accounts').getList()){
            System.AssertEquals(AccountTypeJob.FORMER_STATUS, accountsMap.get(a.id).type, 'This account has not been set to Active correctly');
        }
        
        

    }
    
      static testmethod void buildErrorEmailTest(){
        
        String ERROR_MESSAGE = 'An Error Has Occurred';
        String EMAIL_ADDRESS = 'test@test.com,test2@test.com';
        
        Account acc = TestingUtils.createAccount('test', true);
        
        Map<ID, String> mapOfObjectIDsToErrorMessages = new Map<ID, String>();
        mapOfObjectIDsToErrorMessages.put(acc.id, ERROR_MESSAGE);
        
        Batch_Job_Parameters__c setting = new Batch_Job_Parameters__c(name = AccountTypeJob.JOB_NAME, Emails_To_Notify__c = EMAIL_ADDRESS); 
        insert setting;
        
        Test.startTest();
            List<Messaging.Singleemailmessage> errorEmails = AccountTypeJob.buildErrorEmail(mapOfObjectIDsToErrorMessages);
            Messaging.sendEmail(errorEmails);            
        Test.stopTest();
        
        System.AssertEquals(1, errorEmails.size());
        System.Assert(errorEmails[0].htmlbody.contains(ERROR_MESSAGE), 'The e-mail message does not contain the correct error message');
        System.AssertEquals(errorEmails[0].toAddresses, EMAIL_ADDRESS.split(','), 'The e-mail message does not contain the correct error message');
        
        
    }
}