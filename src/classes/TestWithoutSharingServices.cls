@isTest
private class TestWithoutSharingServices {
	
	static testMethod void testWithoutSharingHelperUpdate_userDoesNotHaveAccessToTheRecord(){
		final String NEW_NAME = 'new Acme';
		
		// We picked a profile that does not have "Modify All" access to Accounts
		Id profileID = [Select Id From Profile where UserType = 'Guest' limit 1].id;
		User testUser = TestingUtils.createUser('t3st', 'NA - Opportunity - Standard', profileID, false);
		
		System.runAs( TestingUtils.ADMIN_USER){
			insert testUser; 
		}

		Account testAccount = TestingUtils.createAccount('Test Account', true);

		System.runAs( testUser ){
			// Test user does not have edit access to the account 
			  testAccount.name = NEW_NAME;
			Test.startTest();
					 WithoutSharingServices.updateWithoutSharing ( new List<Account> { testAccount } );
			Test.stopTest();
		}

		account acc = [Select Name, ID from Account limit 1];
		System.assertEquals(NEW_NAME, acc.Name, 'The name should change even if the test user does not edit access ');

	 }
}