/**
 * Unit Test for SchemaUtils Apex Class
 * @see SchemaUtils
 */
@isTest
private class TestSchemaUtils {

    @isTest
    static void testgetSObjectType() {
        SObjectType testToken = SchemaUtils.getSObjectType('Account');
        System.assertEquals(Account.SObjectType,testToken);
    }

    @isTest
    static void testgetDescribe() {
        DescribeSObjectResult dsr = SchemaUtils.getDescribe(Account.SObjectType);
        System.assertEquals('Account',dsr.getName());
    }

    @isTest
    static void testgetFieldMap() {
        Map<String,SObjectField> testFieldMap = SchemaUtils.getFieldMap(Account.SObjectType);
        System.assert(testFieldMap.keySet().contains('name'));
    }

    @isTest
    static void testgetSObjectField() {
        SObjectField fieldToken = SchemaUtils.getSObjectField(Account.SObjectType,'Name');
        System.assertEquals(Account.Name,fieldToken);
    }

    @isTest
    static void testgetDescribeByStringArgs() {
        DescribeFieldResult dfr  = SchemaUtils.getDescribe('Account','Name');
        System.assertEquals(dfr,Account.Name.getDescribe());
    }

    @isTest
    static void testgetDescribeByTokenString() {
        DescribeFieldResult dfr  = SchemaUtils.getDescribe(Account.SObjectType,'Name');
        System.assertEquals(dfr,Account.Name.getDescribe());
    }

    @isTest
    static void testgetDescribeBySingleFieldToken() {
        DescribeFieldResult dfr = SchemaUtils.getDescribe(Account.Name);
        System.assertEquals(dfr,Account.Name.getDescribe());
    }

    @isTest
    static void testGetFieldList(){
        list <SObjectField> fieldList = SchemaUtils.getFieldList(Account.SObjectType);
        System.assert(fieldList.size() != 0);
    }

    @isTest
    static void testgetFieldsCSV(){
        String fieldCSV = SchemaUtils.getFieldCSV(Account.SObjectType);
        System.assert(fieldCSV != null);
    }

    @isTest
    static void testGetChildren(){
        List<ChildRelationship> children = SchemaUtils.getChildren(Account.SObjectType);
        System.assert(children != null);
    }

}