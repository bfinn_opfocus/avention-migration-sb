/******************************************************************************* 
Name              : SiteDomainTriggerController
Revision History  : TBD
					 
Created/Modified by   Created/Modified Date     Requested by          Related Task/Issue             
----------------------------------------------------------------------------------------
1. Mark Robinson      02/12/2014         	    Jim Horty             
*******************************************************************************/
public class SiteDomainTriggerController {
	
	public static void linkSiteDomainToSite (List<Site_Domain__c> sitedomains) {
		List<String> siteDomainParentIds = new List<String>();
		List<String> siteIdStrings = new List<String>();
		Map<String, String> siteDomainSiteID = new Map<String, String>();
		Map<String, String> siteIdBySiteIDString = new Map<String, String>();
		
		for (Site_Domain__c sitedomain : sitedomains) {
			 if (sitedomain.Site__c != null) siteDomainParentIds.add(sitedomain.Site__c);
			 if (sitedomain.Site_ID__c != null) siteIdStrings.add(sitedomain.Site_ID__c);
		}
		
		for (Site__c site : [SELECT Id, Site_ID__c From Site__c WHERE Site_ID__c IN :siteIdStrings]) {
			system.debug('++++ site.Site_ID__c : ' + site.Site_ID__c);
			siteIdBySiteIDString.put(site.Site_ID__c, site.Id); 
		}

		for (Site__c s : [SELECT Id, Site_ID__c FROM Site__c WHERE Id IN : siteDomainParentIds]) {
			system.debug('++++ siteDomainSiteID.put ' + s.Id);
			siteDomainSiteID.put(s.Id, s.Site_ID__c);			
		}
		
		
		
		// SET PARENT SITE
		for (Site_Domain__c sitedomain : sitedomains) {
			system.debug('++++ sitedomain.Site__c : ' + sitedomain.Site__c);
			if (sitedomain.Site__c == null) {
				if (sitedomain.Site_ID__c != null) {
					sitedomain.Site__c = siteIdBySiteIDString.get(sitedomain.Site_ID__c);
				}
			} else {
				sitedomain.Site_ID__c = siteDomainSiteID.get(sitedomain.Site__c);
			}
		}
	}
	
	public static void assignKeyValue (List<Site_Domain__c> sitedomains) {
		List<String> siteIds = new List<String>();
		Map<String, String> siteIdStringsBySiteId = new Map<String, String>();
		
		
		// ASSIGN SITE ID STRING
		
		
		// ASSIGN KEY STRING
		for (Site_Domain__c sitedomain : sitedomains) {
			if (sitedomain.Site__c != null) siteIds.add(sitedomain.Site__c);
		}
		for (Site__c site : [SELECT Id, Site_ID__c FROM Site__c WHERE Id IN :siteIds]) {
			siteIdStringsBySiteId.put(site.Id, site.Site_ID__c);
		}
		
		for (Site_Domain__c sitedomain : sitedomains) {
			if (sitedomain.Key__c == null) {
				if (sitedomain.Site__c != null) {
					sitedomain.Key__c = siteIdStringsBySiteId.get(sitedomain.Site__c) + '_' + sitedomain.Domain__c;
				}
			}
		}
	}
	
	public static testMethod void linkSiteDomainToSite_Test() {
		Account account = TestingUtils.createAccount('TestAccount', true);
		Site__c site = TestingUtils.createSite(account);
		Site_Domain__c sd = TestingUtils.createSiteDomain(site);
		
	}
	
}