/*
** Class:  SitePrivHelper
** Created by OpFocus on June 24, 2015
** Description: Helper methods related to the Site_Priv__c object 
**   
*/  
public without sharing class SitePrivHelper {
	
	public static List<Site_Priv__c> updateSchemaName(List<Site_Priv__c> lstPrivsToUpdate){
		List<Id> lstSchemaId = new List<Id>();
		for(Site_Priv__c priv : lstPrivsToUpdate){
			lstSchemaId.add(priv.Schema__c);
		}

		Map<Id, DbSchema__c> mapDbSchemas = new Map<Id,DbSchema__c>([Select Id, Name from DbSchema__c where Id in :lstSchemaId]);
		for(Site_Priv__c priv : lstPrivsToUpdate){
			priv.Schema_Name__c = mapDbSchemas.get(priv.Schema__c).Name;
		}

		//return results not needed if calling from a trigger, but helpful for debugging
		return lstPrivsToUpdate;
	}

}