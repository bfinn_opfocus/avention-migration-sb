/*
** Class:  SiteRequestController
** Created by OpFocus on 4/8/2015
** Description: 
**   Controller for VF page SiteRequest.
**   This page will allow a user to create a request the creatoin of a new Avention Site.
*/  
public without sharing class SiteRequestController {

	public ApexPages.StandardController std {get; private set;}
	public Boolean pageError {get; private set;}

	public Site_Request__c siteRequest {get; private set;}
	public Map<String, List<BundleHelper>> mapProductBundles {get; private set;}
	public List<IpWrapper> lstIpAddresses {get; private set;}

	public Site__c existingSite {get; private set;}
	public Boolean isExistingSite {get; private set;}
	public Map<String, Site_Priv__c> mapExistingSitePrivs {get; private set;}

	public enum SiteModes {NewSite, RequestExtension, EditSite}
	public SiteModes siteMode {get; private set;}
	public String siteModeForVF {get{return siteMode.name();}}

	private Map<Id, List<Site_Request_Bundle_Line_Item__c>> mapBundlesAndLineItems = new Map<Id, List<Site_Request_Bundle_Line_Item__c>>();
	private Opportunity opp;
	private Order__c order;

	public static List<String> lstCurrentUserGroups {
		get{
			if(lstCurrentUserGroups == null){
				lstCurrentUserGroups = new List<String>();
				List<GroupMember> lstGm = [SELECT Id, group.name, group.type FROM GroupMember where UserOrGroupId = :UserInfo.getUserId() AND group.type='Regular'];
				for (GroupMember gm : lstGm){
					lstCurrentUserGroups.add(gm.group.Name);
				}
			}
			return lstCurrentUserGroups;
		}
		set;
	}
        
	public SiteRequestController(ApexPages.StandardController std) {	
		siteRequest = new Site_Request__c();
		populateActiveBundles();
		lstIpAddresses = new List<IpWrapper>();
		siteMode = SiteModes.NewSite;
		String oppId; 
		String orderId;
		String siteId;

		if(ApexPages.currentPage().getParameters().containsKey('OppId')){
			oppId = ApexPages.currentPage().getParameters().get('OppId');
		}
		if(ApexPages.currentPage().getParameters().containsKey('OrderId')){
			orderId = ApexPages.currentPage().getParameters().get('OrderId');
		}
		if(ApexPages.currentPage().getParameters().containsKey('SiteId')){
			siteId = ApexPages.currentPage().getParameters().get('SiteId');
			siteMode = SiteModes.EditSite;

			if(ApexPages.currentPage().getParameters().containsKey('mode')){
				String modeString = ApexPages.currentPage().getParameters().get('mode');
				if(modeString.toLowerCase() == 'requestextension'){
					siteMode = SiteModes.RequestExtension;
				}
			}		
		}


		//If a OppId is passed in then let's try to load it in
		if(oppId != null){
	        try {
	            opp = [select Id, Name, AccountId, Account.OwnerId, Account.Name, Account.PMD_Ship_To_Customer_ID__c, OwnerId, (SELECT ContactId,IsPrimary FROM OpportunityContactRoles where IsPrimary=true limit 1) from Opportunity where id=:oppId];
	            siteRequest.Opportunity__c = opp.Id;
	            siteRequest.Sales_Rep__c = opp.OwnerId;
	            siteRequest.Team_Leader__c = opp.Account.OwnerId;
	            siteRequest.Display_Name__c = opp.Account.Name;
	            siteRequest.Account__c = opp.AccountId;
	        }catch (Exception ex) {
	            String err = 'Opportunity not found (' + oppId + ')';
	            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error, err);
	            ApexPages.addMessage(msg);
	            pageError = true;
	            return;
	        }
            //If there is not CustNo for the account, show an error message. For now this needs to be resolved manually
            if(opp.Account.PMD_Ship_To_Customer_ID__c == null || opp.Account.PMD_Ship_To_Customer_ID__c ==''){
	            String err = 'The parent Account for this Opportunity ('+ opp.Account.Name + ') does not have a PMD Customer Number.  ';
	            err += 'Please contact a member of Sales Opps for assistance.';
	            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error, err);
	            ApexPages.addMessage(msg);
	            pageError = true;
	            return;	            	
            }

            if(opp.OpportunityContactRoles != null && !opp.OpportunityContactRoles.isEmpty()){
            	siteRequest.Primary_Contact__c = opp.OpportunityContactRoles[0].ContactId;
            }
            setDefaultsForNewSiteRequest();
		}
		//If a OrderId is passed in then let's try to load it in
		else if(orderId != null){
			try{
				order = [Select Id, Name, Opportunity__c, System_Administrator__c from Order__c where Id =:orderId];
	            opp = [select Id, Name, AccountId, Account.OwnerId, Account.PMD_Ship_To_Customer_ID__c, Account.Name, OwnerId, (SELECT ContactId, IsPrimary FROM OpportunityContactRoles where IsPrimary=true limit 1) from Opportunity where id=:order.Opportunity__c];
	            siteRequest.Order__c = order.id;
	            siteRequest.Opportunity__c = opp.Id;
	            siteRequest.Sales_Rep__c = opp.OwnerId;
	            siteRequest.Team_Leader__c = opp.Account.OwnerId;
	            siteRequest.Account__c = opp.AccountId;

	            //If there is not PMD_Ship_To_Customer_ID__c for the account, show an error message. For now this needs to be resolved manually
	            if(opp.Account.PMD_Ship_To_Customer_ID__c == null || opp.Account.PMD_Ship_To_Customer_ID__c ==''){
		            String err = 'The parent Account for this Order ('+ opp.Account.Name + ') does not have a PMD Customer Number. ';
		            err += 'Please contact a member of Sales Opps for assistance.';
		            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error, err);
		            ApexPages.addMessage(msg);
		            pageError = true;
		            return;	            	
	            }

	            if(order.System_Administrator__c != null){
	            	siteRequest.Primary_Contact__c = order.System_Administrator__c;
	            }
	            setDefaultsForNewSiteRequest();
			}
			catch(Exception ex){
	            String err;
	            if(order == null){
	            	err= 'Problem loading Order Id (' + orderId + ')';
	            }
	            if (order != null && opp == null){
	            	err ='This Order (' + order.Name + ') does not have an Opportunity associated with it';
	            }
	            System.debug('==========> Problem loading Order Id (' + orderId + ') : ' + ex.getMessage());
	            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error, err);
	            ApexPages.addMessage(msg);
	            pageError = true;
	            return;
			}
		}
		//If a SiteId is passed in then let's try to load it in
		else if(SiteId != null){
			try{
				existingSite = [SELECT Account__c, CUSTNUM__c, CUSTTYPE__c, Check_Password__c, Contact__c, Display_Name__c, Password__c, Require_Password__c, Site_ID__c, Name, Site_Request__c, Start_Date__c, Requested_Extension_Date__c,  Status__c, Has_Trial_SitePriv__c, User_Level_Auth__c, Email_Address__c FROM Site__c where Id = :SiteId];
				LoadExistingSite();
			}
			catch(Exception ex){
				String siteName = SiteId;
				if(existingSite.Site_ID__c != null){
					siteid = existingSite.Site_ID__c;
				}
	            String err = 'Problem loading Site Id (' + siteName + ') : ' + ex.getMessage();
	            System.debug('==========> Error loading existing site: ' + ex);
	            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error, err);
	            ApexPages.addMessage(msg);
	            pageError = true;
	            return;				
			}
		}
	}

	private void setDefaultsForNewSiteRequest(){
		siteRequest.Start_Date__c = Date.today();
		siteRequest.End_Date__c = siteRequest.Start_Date__c.addDays(7);
		siteRequest.Require_Domain__c = 		false;
		siteRequest.Require_Password__c = 		true; 
		siteRequest.Check_Password__c = 		true;
		siteRequest.Domain_Registration__c = 	false;
		SiteRequest.Require_Project__c = 		false;
		siteRequest.Require_Workgroup__c = 		false;
		siteRequest.Site_Allows_Perm_PW__c = 	false;
		siteRequest.Silent_Registration__c = 	false;
		siteRequest.Custom_Registration__c = 	false;
		siteRequest.Custom_Registration_URL__c = '';
		siteRequest.Rolling_User_ID__c = 		false;
		siteRequest.User_Level_Auth__c = 		false;
		siteRequest.Requestor__c = userinfo.getUserId();
		SiteRequest.Request_Type__c = 'New Site ID';
		SiteRequest.Password__c = 'Onesource';
		if(opp.Account != null && opp.Account.PMD_Ship_To_Customer_ID__c != null){
			siteRequest.PMD_Custom_Number__c = opp.Account.PMD_Ship_To_Customer_ID__c;	
		}	
	}

	public void populateActiveBundles(){
		mapProductBundles = new Map<String, List<BundleHelper>>();
		List<BundleHelper> lstReturnValue = new List<BundleHelper>();
		List<Site_Request_Bundle__c> lstBundles = [select Id, Name, Group__c, Allowed_Groups__c, Is_Trial_Bundle__c, Mutually_Exclusive_Bundle__c, Email_Template_Name__c, (select Id, Name, Product_Name__c, Expiration_Date__c, Seats__c, Schema__c, Is_Trial__c, Call_Add_Invite_Stored_Proc__c, Call_Credit_Or_Pool_Update_Proc__c, Exclude_DB_Worldbase_Companies__c, Insert_A_Site_Priv_Record__c, Schema_Name__c from Site_Request_Bundle_Line_Items__r order by Name) from Site_Request_Bundle__c where Active__c = true order by group__c asc, Mutually_Exclusive_Bundle__c desc, Name asc];
		if(lstBundles != null){
			for(Site_Request_Bundle__c bundle : lstBundles){
				if(BundleValidForGroup(bundle)){
					if(!mapProductBundles.containsKey(bundle.group__c)){
						mapProductBundles.put(bundle.group__c, new List<BundleHelper>());
					}
					mapProductBundles.get(bundle.group__c).add(new BundleHelper(false, bundle));
					mapBundlesAndLineItems.put(bundle.Id, bundle.Site_Request_Bundle_Line_Items__r);
				}
			}
		}
	}

	public Boolean BundleValidForGroup(Site_Request_Bundle__c bundle){
		Set<String> bundleGroups = new Set<String>();

		if(bundle.Allowed_Groups__c != null){
			if(bundle.Allowed_Groups__c.contains(';')){
				bundleGroups = new Set<String>(bundle.Allowed_Groups__c.split(';\\s*'));
			}else{
				bundleGroups.add(bundle.Allowed_Groups__c);
			}
		}
		for(String userGroup : lstCurrentUserGroups){
			if(bundleGroups.contains(userGroup)){
				return true;
			}
		}
		return false;
	}

	public Boolean IsSiteNameUnique(){
		String soql = 'SELECT count() FROM Site__c WHERE Site_ID__c = \'' + siteRequest.Site_Id__c + '\' OR Name = \'' + siteRequest.Site_Id__c + '\'';
		Integer count = Database.countQuery(soql);
		return count == 0;
	}

	//Placeholder to loading anexisting site
	public void LoadExistingSite(){
		//load basic site info
		isExistingSite = true;
		//load up the placeholder siteRequest object for input fields;  just don't save it
		siteRequest.Site_Id__c = existingSite.Name;
		siteRequest.Password__c = existingSite.Password__c;
		if(existingSite.Start_Date__c != null){
			siteRequest.Start_Date__c = existingSite.Start_Date__c.date();
		}
		if(existingSite.Contact__c != null){
			siteRequest.Primary_Contact__c = existingSite.Contact__c;
		}

		Boolean endDatesMatch = true;
		Datetime allSitePrivEndDate; //only applicable if endDatesMatch = true;
		Datetime earliestPrivEndDate; //If all the Privs don't expire together, use the earliest

		mapExistingSitePrivs = new Map<String, Site_Priv__c>();
		//load existing SitePrivs
		List<Site_Priv__c> existingSitePrivs = [Select Id, Name, Schema__c, Schema_Display_Name__c, Password__c, Active_Till__c, Allowed_Users__c from Site_Priv__c where Site__c = :existingSite.Id ];
		for(Site_Priv__c sitePriv : existingSitePrivs){
			mapExistingSitePrivs.put(sitePriv.Schema_Display_Name__c, sitePriv);
			//See if all date's match
			if(allSitePrivEndDate != null && allSitePrivEndDate != sitePriv.Active_Till__c){
				endDatesMatch = false;
			}
			allSitePrivEndDate = sitePriv.Active_Till__c;

			if(earliestPrivEndDate == null || earliestPrivEndDate < sitePriv.Active_Till__c){
				earliestPrivEndDate = sitePriv.Active_Till__c;
			}
		}

		//If there was an existing SiteRequest then we can see what bundles they already selected and disable them
		if(existingSite.Site_Request__c != null){
			List<Site_Request_Line_Item__c> ListRequestLineitems = [Select Id, Name, Site_Request__c, Originating_Site_Request_Bundle__c, Schema_Name__c from Site_Request_Line_Item__c where Site_Request__c = :existingSite.Site_Request__c];
			Set<Id> setOriginalBundleIds = new Set<Id>();
			for(Site_Request_Line_Item__c lineItem : ListRequestLineitems ){
				setOriginalBundleIds.add(lineItem.Originating_Site_Request_Bundle__c);
			}

			//loop through all the bundles and see if they're in the existing list
			for(List<BundleHelper> lstHelpers : mapProductBundles.values()){
				for(BundleHelper helper : lstHelpers){
					//in existing map, so it was selected previously. Disable it
					if(setOriginalBundleIds.contains(helper.bundle.id)){
						helper.selected = true;
						helper.enabled = false;
					}
					//And now set the value of of the bundle line items from the matching existing SitePrivs
					for(Site_Request_Bundle_Line_Item__c bundleLineItem : helper.bundle.Site_Request_Bundle_Line_Items__r){
						if(mapExistingSitePrivs.containsKey(bundleLineItem.Schema_Name__c) && helper.selected){
							Site_Priv__c sitePriv = mapExistingSitePrivs.get(bundleLineItem.Schema_Name__c);
							bundleLineItem.Seats__c = sitePriv.Allowed_Users__c;
							if(sitePriv.Active_Till__c != null){
								bundleLineItem.Expiration_Date__c = sitePriv.Active_Till__c.Date();
							}else{
								throw new SiteRequestException('The SitePriv ' + sitePriv.Schema_Name__c + ' does not have a valid expiration date.');
							}
						}
					}
				}
			}
		}
		if(endDatesMatch && allSitePrivEndDate != null){
			siteRequest.End_Date__c = allSitePrivEndDate.Date();
		}

		if(siteMode == SiteModes.RequestExtension){
			if(allSitePrivEndDate != null){
				existingSite.Requested_Extension_Date__c = allSitePrivEndDate.Date();
				existingSite.Requested_Extension_Original_Date__c = allSitePrivEndDate.Date();
			}
			else{
				existingSite.Requested_Extension_Date__c = earliestPrivEndDate.Date();
				existingSite.Requested_Extension_Original_Date__c = earliestPrivEndDate.Date();
			}
		}
	}

	public PageReference cancel(){
		String returnUrl = ApexPages.currentPage().getParameters().get('retURL');
		if(returnUrl != null && returnUrl != ''){
			return New PageReference( +  returnUrl); 
		}
		String prefix = Site_Request__c.getSObjectType().getDescribe().getKeyPrefix();
		return New PageReference('/' + prefix + '/o'); 
	}

	public PageReference SubmitExtensionRequest(){
		existingSite.Requested_Extension_Status__c = 'Submitted';
		try{
			existingSite.Requested_Extension_Date__c = siteRequest.End_Date__c;
			update existingSite;
		}
		catch(Exception e){
			String err = 'There was a problem submitting the request to extend the expiration date: ' + e.getMessage() + ' \n ' + e.getStackTraceString();
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error, err);
			ApexPages.addMessage(msg);
			pageError = true;
			return null;
		}
		return new PageReference('/' + existingSite.id);
	}

	public PageReference SaveExistingSite(){
		//create a list of SitePrivs to upsert
		Map<String, Site_Priv__c> mapSitePrivsToUpsert = new Map<String, Site_Priv__c>();
		//load up exising site from changes in site request so we can save it
		if(siteRequest.Primary_Contact__c != existingSite.Contact__c){
			existingSite.Contact__c = siteRequest.Primary_Contact__c;
		}

		for(List<BundleHelper> budleGroup : mapProductBundles.values()){
			for(BundleHelper helperBundle : budleGroup){
				if(helperBundle.selected){
					List<Site_Request_Bundle_Line_Item__c> products = mapBundlesAndLineItems.get(helperBundle.bundle.Id);
					for(Site_Request_Bundle_Line_Item__c product : products){
						Site_Priv__c sitePriv;
						//If in map then update exsting
						if(mapExistingSitePrivs.containsKey(product.Schema_Name__c)){
							sitePriv = mapExistingSitePrivs.get(product.Schema_Name__c);
						}else{ //if not in map then add
							sitePriv = new Site_Priv__c();
							sitePriv.Schema__c = product.Schema__c;
							sitePriv.Site__c = existingSite.Id;
							sitePriv.Call_Add_Invite_Proc__c = product.Call_Add_Invite_Stored_Proc__c;
							sitePriv.Call_Credit_Pool_Update_Proc__c = product.Call_Credit_Or_Pool_Update_Proc__c;
							sitePriv.Exclude_D_B_Worldbase_Companies__c = product.Exclude_DB_Worldbase_Companies__c;
							sitePriv.Insert_Site_Priv_Record__c = product.Insert_A_Site_Priv_Record__c;
							sitePriv.Email__c = existingSite.Email_Address__c;
							sitePriv.AdminLevel__c = existingSite.User_Level_Auth__c == true ? 1 : 0;
							if(sitePriv.Call_Credit_Pool_Update_Proc__c ){
								sitePriv.Credit_Pool_Users__c =  product.Seats__c;
							}
						}
						if(product.Expiration_Date__c != null){
							sitePriv.Active_Till__c = product.Expiration_Date__c;
						}else{
							sitePriv.Active_Till__c = siteRequest.End_Date__c;			
						}
						sitePriv.Active_Till__c = sitePriv.Active_Till__c.addHours(-(Utils.getTimeZoneOffset()));
						if(product.Seats__c != null){
							sitePriv.Allowed_Users__c = product.Seats__c;
						}else{
							sitePriv.Allowed_Users__c = siteRequest.Default_Seats__c;
						}
						mapSitePrivsToUpsert.put(product.Schema_Name__c, sitePriv);
					}
				}
			}
		}
		Savepoint sp = Database.setSavepoint();
		try{
			//save Site
			update existingSite;
			//upsert of site privs
			upsert mapSitePrivsToUpsert.values();
		}catch(Exception e){
			Database.rollback(sp);
            String err = 'Problem saving Site ' + existingSite.Name + ' : ' + e.getMessage();
            System.debug('==========> Error saving existing site: ' + e);
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error, err);
            ApexPages.addMessage(msg);
            pageError = true;
            return null;		
		}
		
		return new PageReference('/' + existingSite.id);
	}

	public PageReference CreateSiteRequest(){
		//Validate that multiple mutually-exclusive bundles are not selected
		List<String> lstSelectedMutuallyExclusiveBundles = getSelectedMutuallyExclusiveBundles();
		if(lstSelectedMutuallyExclusiveBundles.size() != 1){
			String err = '';
			if(lstSelectedMutuallyExclusiveBundles.size() > 1){
				err = Label.Error_Site_Request_Mutually_Exclusive_Bundles;
				for(String bundleName : lstSelectedMutuallyExclusiveBundles){
					err = err + bundleName + ', ';
				}
				//trim off trailing space and comma
				err = err.substring(0, err.length()-2);
			}
			if(lstSelectedMutuallyExclusiveBundles.size() < 1){
				err = Label.Error_Site_Request_Mutually_Exclusive_Bundles;
			}

			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error, err);
			ApexPages.addMessage(msg);
			return null;
		}
		//Ned to make sure the contact belongs to the same account as the site
		Id contactId = [Select Account.Id, Id from Contact where Id = :siteRequest.Primary_Contact__c].Account.Id;
		if(siteRequest.Account__c <> contactId){
			String err = Label.Error_Site_Request_Contact_Must_Belong_to_Account;
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error, err);
			ApexPages.addMessage(msg);
			return null;
		}

		if(!IsSiteNameUnique()){
			String err = Label.Error_Site_Request_Site_ID_Already_in_Use.replace('{!SiteID}', siteRequest.Site_Id__c)  ;
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error, err);
			ApexPages.addMessage(msg);
			return null;
		}

		Savepoint sp = Database.setSavepoint();

		//Create Site Request record
		try{
			//populate Primary Contact fields
			if(siteRequest.Primary_Contact__c != null ){
				Contact PrimaryContact = [Select Id, Firstname, LastName, Salutation, Name, MailingStreet, MailingCity, MailingState, MailingCountry, MailingPostalCode, Phone, Fax, Email from Contact where Id = :siteRequest.Primary_Contact__c];
				siteRequest.Email_Address__c = PrimaryContact.Email;
				siteRequest.Contact_Title__c = PrimaryContact.Salutation;
				siteRequest.Contact_Name__c = PrimaryContact.Name;
				siteRequest.Address__c = PrimaryContact.MailingStreet;
				siteRequest.City__c = PrimaryContact.MailingCity;
				siteRequest.State_or_Province__c = PrimaryContact.MailingState;
				siteRequest.Country__c = PrimaryContact.MailingCountry;
				siteRequest.Postal_Code__c = PrimaryContact.MailingPostalCode;
				siteRequest.Phone_Number__c = PrimaryContact.Phone;
				siteRequest.Fax_Number__c = PrimaryContact.Fax;
				for(List<BundleHelper> budleGroup : mapProductBundles.values()){
					for(BundleHelper helperBundle : budleGroup){
						if(helperBundle.selected){
							//Set tamplate that we'll eventaully use for the welcome email (based off bundles)
							if(helperBundle.bundle.Mutually_Exclusive_Bundle__c && helperBundle.bundle.Email_Template_Name__c != null){
								siteRequest.Welcome_Email_Template_Name__c = helperBundle.bundle.Email_Template_Name__c;
							}
						}
					}
				}
			}
			insert siteRequest;

			//Create Line Items
			List<Site_Request_Line_Item__c> lstRequestLineItemsToAdd = new List<Site_Request_Line_Item__c>();
			for(List<BundleHelper> budleGroup : mapProductBundles.values()){
				for(BundleHelper helperBundle : budleGroup){
					if(helperBundle.selected){
						List<Site_Request_Bundle_Line_Item__c> products = mapBundlesAndLineItems.get(helperBundle.bundle.Id);

						for(Site_Request_Bundle_Line_Item__c product : products){
							Site_Request_Line_Item__c lineItem = new Site_Request_Line_Item__c();
							lineItem.Originating_Site_Request_Bundle__c = helperBundle.bundle.id;
							lineItem.Schema__c = product.Schema__c;
							lineItem.Name = product.Product_Name__c;
							if(product.Expiration_Date__c != null){
								//Business rule to add a day to whatever was picked
								lineItem.Expiration_Date__c = product.Expiration_Date__c.addDays(1);
							}else{
								//Business rule to add a day to whatever was picked
								lineItem.Expiration_Date__c = siteRequest.End_Date__c.addDays(1);
							}
							if(product.Seats__c != null){
								lineItem.Seats__c = product.Seats__c;
							}else{
								lineItem.Seats__c = siteRequest.Default_Seats__c;
							}
							lineItem.Site_Request__c = siteRequest.id;
							lineItem.Call_Add_Invite_Proc__c = product.Call_Add_Invite_Stored_Proc__c;
							lineItem.Call_Credit_Pool_Update_Proc__c = product.Call_Credit_Or_Pool_Update_Proc__c;
							lineItem.Exclude_D_B_Worldbase_Companies__c = product.Exclude_DB_Worldbase_Companies__c;
							lineItem.Insert_Site_Priv_Record__c = product.Insert_A_Site_Priv_Record__c; 
							lineItem.Email__c = siteRequest.Email_Address__c ;

							lstRequestLineItemsToAdd.add(lineItem);
						}
					}
				}
			}

			if(!lstRequestLineItemsToAdd.isEmpty()){
				insert lstRequestLineItemsToAdd;
			}

			//Create IP Addresses
			List<Site_Request_Domain__c> lstDomainsToAdd = new List<Site_Request_Domain__c>();
			for(IpWrapper ip : lstIpAddresses){
				if(ip.ipAddress !='0.0.0.0'){
					Site_Request_Domain__c requestDomain = new Site_Request_Domain__c();
					requestDomain.Name= ip.ipAddress;
					requestDomain.Domain__c= ip.ipAddress;
					requestDomain.Site_Request__c = siteRequest.id;
					lstDomainsToAdd.add(requestDomain);
				}
			}
			if(!lstDomainsToAdd.isEmpty()){
				insert lstDomainsToAdd;
			}
		}
		catch(Exception e){
			Database.rollback(sp);
	        String err = 'There was a problem creating the Site Request: ' + e.getMessage() + ' \n ' + e.getStackTraceString();
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error, err);
            ApexPages.addMessage(msg);
            pageError = true;
            return null;
		}

		return new PageReference('/' + siteRequest.id);
		//return returnValue;
	}

	//Loop through all selected bundles and return a list of ones that are flagged to be mutually exclusive
	private List<String> getSelectedMutuallyExclusiveBundles(){
		List<String> mutuallyExclusiveBundles = new List<String>();
		for(List<BundleHelper> budleGroup : mapProductBundles.values()){
			for(BundleHelper helperBundle : budleGroup){
				if(helperBundle.selected && helperBundle.bundle.Mutually_Exclusive_Bundle__c){
					mutuallyExclusiveBundles.add(helperBundle.bundle.Name);
				}
			}
		}
		return mutuallyExclusiveBundles;
	}

	public void AddIpRow(){
		lstIpAddresses.add(new IpWrapper('0.0.0.0'));
	}

	public class BundleHelper{
		public Boolean selected {get;set;}
		public Boolean enabled {get;set;}
		public Site_Request_Bundle__c bundle {get;set;}

		public BundleHelper(Boolean selected, Site_Request_Bundle__c bundle){
			this.selected = selected;
			this.bundle = bundle;
			this.enabled = true;
		}
		public BundleHelper(Boolean selected, Boolean enabled, Site_Request_Bundle__c bundle){
			this.selected = selected;
			this.bundle = bundle;
			this.enabled = enabled;
		}
	}
	public class IpWrapper{
		IpWrapper(String ipAddress){
			this.ipAddress = ipAddress;
		}
		public String ipAddress {get;set;}
	}

	public class SiteRequestException extends Exception {}
}