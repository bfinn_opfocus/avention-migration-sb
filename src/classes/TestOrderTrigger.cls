/* Class:       TestOrderTrigger
 * Created On:  11/19/2015
 * Created by:  OpFocus Team
 * Description: Unit test for Order trigger functionality
 */
@isTest
private class TestOrderTrigger
{
    private static final Boolean testOrderReadyForBilling = true;
    private static final Boolean testSubscriptionOrderReadyForBilling = true;
    private static final Boolean test2YearSubscriptionOrderReadyForBilling = true;
    private static final Boolean test3YearSubscriptionOrderReadyForBilling = true;
    private static final Boolean testSetCloseDate = true;
    private static final Boolean testSetFutureCloseDate = true;
    private static final Boolean testSetFutureYearCloseDate = true;
    private static final Boolean testSetCloseDateForEarlierDate = true;
    private static final Boolean testSetCloseDateForDateInPreviousYear = true;
    private static final Boolean testSetCloseDateWithOverride = true;
    private static final Boolean testMissingOLI = true;
    private static final Boolean testNoUnitPriceChange = true;
    private static final Boolean testAmountDifferences = true;
    private static final Boolean testAmountDifferencesForUneven = true;
    private static final Boolean testCreateOrderFromQuote = true;
    private static final Boolean testCopyQLIs = true;


/** REINSTATE
    @isTest
    static void testSubscriptionOrderReadyForBilling() {
        System.assert(testSubscriptionOrderReadyForBilling, 'Test disabled.');
        initData(OrderHelper.SUBSCRIPTION, 1);

        Test.startTest();
        testOrder.Intacct_Order_ID__c = '1234567890';
        update testOrder;
        Test.stopTest();

        // now verify that Schedules took on Order Product's Sales Price
        List<Id> lstOliIds = new List<Id>();
        for (OpportunityLineItem oli : [select Id 
            from OpportunityLineItem where OpportunityId=:testOpp.Id]) {
                lstOliIds.add(oli.Id);
        }
        /** IF AND WHEN REVENUE SCHEDULES GET REINSTATED, REINSTATE THIS CODE
        List<OpportunityLineItemSchedule> lstSchedules = [select Id, OpportunityLineItemId, Revenue
            from OpportunityLineItemSchedule where OpportunityLineItemId in:lstOliIds];

        for (OpportunityLineItemSchedule schedule : lstSchedules) {
            System.assert(schedule.Revenue == 1000 || schedule.Revenue == 2000, 
                'Revenue not set correctly on OLI Schedule.');
        }       
    }
**/

/** REINSTATE
    @isTest
    static void test2YearSubscriptionOrderReadyForBilling() {
        System.assert(test2YearSubscriptionOrderReadyForBilling, 'Test disabled.');
        initData(OrderHelper.SUBSCRIPTION, 2);

        Test.startTest();
        testOrder.Intacct_Order_ID__c = '1234567890';
        update testOrder;
        Test.stopTest();

        // now verify that Schedules took on Order Product's Sales Price
        List<Id> lstOliIds = new List<Id>();
        for (OpportunityLineItem oli : [select Id 
            from OpportunityLineItem where OpportunityId=:testOpp.Id]) {
                lstOliIds.add(oli.Id);
        }
        /** IF AND WHEN REVENUE SCHEDULES GET REINSTATED, REINSTATE THIS CODE

        List<OpportunityLineItemSchedule> lstSchedules = [select Id, OpportunityLineItemId, Revenue
            from OpportunityLineItemSchedule 
            where OpportunityLineItemId in:lstOliIds 
            order by ScheduleDate ASC];
        // If and when Schedules are put into place, this assert statement needs
        // to be changed to reflect the number of expected scheduled - should be 4
        // unless logic changes
        System.assertEquals(0, lstSchedules.size(), 'Incorrect number of Schedules created.');
    }
**/

/** REINSTATE
    @isTest
    static void test3YearSubscriptionOrderReadyForBilling() {
        System.assert(test3YearSubscriptionOrderReadyForBilling, 'Test disabled.');
        initData(OrderHelper.SUBSCRIPTION, 3);

        Test.startTest();
        testOrder.Intacct_Order_ID__c = '1234567890';
        update testOrder;
        Test.stopTest();

        // now verify that Schedules took on Order Product's Sales Price
        OpportunityLineItem oli = [select Id 
            from OpportunityLineItem where OpportunityId=:testOpp.Id and Quantity=10 limit 1];

        /** IF AND WHEN REVENUE SCHEDULES GET REINSTATED, REINSTATE THIS CODE

        System.debug('==========> Getting OLISchedules for our OLIs');
        List<OpportunityLineItemSchedule> lstSchedules = [select Id, OpportunityLineItemId, Revenue
            from OpportunityLineItemSchedule 
            where OpportunityLineItemId=:oli.Id
            order by ScheduleDate ASC];

        // If and when Schedules are put into place, this assert statement needs
        // to be changed to reflect the number of expected scheduled - should be 3
        // unless logic changes

        System.assertEquals(0, lstSchedules.size(), 'Incorrect number of Schedules created.');
        Integer scheduleIdx = 1;
        for (OpportunityLineItemSchedule schedule : lstSchedules) {
            if (scheduleIdx == 1) {
                System.assertEquals(312.5, schedule.revenue, 
                    '1st year revenue schedule not set correctly.');
            } else if (scheduleIdx == 2) {
                System.assertEquals(333.333, schedule.revenue, '2nd year revenue schedule not set correctly.');
            } else {
                System.assertEquals(354.167, schedule.revenue, '3rd year revenue schedule not set correctly.');
            }
            scheduleIdx++;
        }
    }
**/

    @isTest
    static void testOrderReadyForBilling() {
        System.assert(testOrderReadyForBilling, 'Test disabled.');
        initData(OrderHelper.DATA_SOLUTIONS, 0);

        Test.startTest();
        testOrder.Intacct_Order_ID__c = '1234567890';
        update testOrder;
        Test.stopTest();

        // now verify that Oli took on Order Product's attributes
        List<OpportunityLineItem> lstOlis = [select Id, UnitPrice from OpportunityLineItem 
            where OpportunityId=:testOpp.Id];
        System.assertEquals(2, lstOlis.size(), 'Found wrong number of OLis for our Opportunity.');
        System.assertEquals(1000, lstOlis.get(0).UnitPrice, 'UnitPrice not set correctly on OLI.');
        // verify that CloseDate is today when Contract_Start_Date is null
        System.assertEquals(Date.today(), [select CloseDate from Opportunity 
            where Id=:testOpp.Id limit 1].CloseDate, 'CloseDate not set on Opportunity as expected.');
    }

    @isTest
    static void testCreateOrderFromQuote() {
        System.assert(testCreateOrderFromQuote, 'Test disabled.');
        initData(OrderHelper.DATA_SOLUTIONS, 0, false);

        // create QLIs for the same Product to test as they will not be copied over
        SBQQ__QuoteLine__c qli = new SBQQ__QuoteLine__c(SBQQ__Quote__c=testQuote.Id, 
            SBQQ__NetPrice__c=10000,
            SBQQ__Product__c=testProduct.Id, 
            SBQQ__Number__c=100, 
            SBQQ__Quantity__c=1);
        insert qli;

        // Now create an Order from Quote and set copy QLI flag to false to fire trigger
        Order__c order = new Order__c(Name='Test Order', 
            Opportunity__c=testOpp.Id, Quote__c=testQuote.Id, Amount__c=3000);
        insert order;

        Test.startTest();
        order.Intacct_Order_ID__c = '12345';
            update order;
        Test.stopTest();

        // verify that QLI was not copied over
        System.assertEquals(0, [select Id from Order_Product__c  where Order__c=:order.Id].size(),
            'Order Products copied over when they should not have been.');
    }

    @isTest
    static void testNoUnitPriceChange() {
        System.assert(testNoUnitPriceChange, 'Test disabled.');
        initData(OrderHelper.DATA_SOLUTIONS, 0);

        Test.startTest();

        testOrder.Intacct_Order_ID__c = '1234567890';
        update testOrder;
        
        testOrder.Intacct_Order_ID__c = null;
        update testOrder;

        // now try again without making any change to OLI and it should work.
        // tests scenario where we try to update an OLI when we are not changing
        // any of it values but we null out TotalPrice - SF complains
        testOrder.Intacct_Order_ID__c = '9999';
        update testOrder;

        Test.stopTest();
    }

    @isTest
    static void testSetCloseDate() {
        System.assert(testSetCloseDate, 'Test disabled.');
        initData(OrderHelper.DATA_SOLUTIONS, 0);

        Test.startTest();
            testOrder.Contract_Start_Date__c = Date.today();
            testOrder.Intacct_Order_ID__c = '9876543210';
            update testOrder;
        Test.stopTest();

        // verify that CloseDate is the same as our Contract_Start_Date which we just set to today
        System.assertEquals(Date.today(), 
            [select CloseDate from Opportunity 
            where Id=:testOpp.Id limit 1].CloseDate, 'CloseDate not set on Opportunity as expected.');
    }


    @isTest
    static void testSetFutureCloseDate() {
        System.assert(testSetFutureCloseDate, 'Test disabled.');
        initData(OrderHelper.DATA_SOLUTIONS, 0);

        Test.startTest();
            testOrder.Contract_Start_Date__c = Date.today().addDays(30);
            testOrder.Intacct_Order_ID__c = '9876543210';
            update testOrder;
        Test.stopTest();

        // verify that CloseDate is 1 day before Contract_Start_Date
        System.assertEquals(testOrder.Contract_Start_Date__c.addDays(-1), 
            [select CloseDate from Opportunity 
            where Id=:testOpp.Id limit 1].CloseDate, 'CloseDate not set on Opportunity as expected.');
    }

    @isTest
    static void testSetFutureYearCloseDate() {
        System.assert(testSetFutureYearCloseDate, 'Test disabled.');
        initData(OrderHelper.DATA_SOLUTIONS, 0);

        Date thisMonth = Date.today();
        // If this is January, need to bump it to February
        if (thisMonth.month() == 1) {
            thisMonth = thisMonth.addMonths(2);
        }
        Date prevMonthLastYear = thisMonth.addYears(1).addMonths(-1);        

        Test.startTest();
            // set Contract Start Date to a date next year but in a previous month.
            testOrder.Contract_Start_Date__c = prevMonthLastYear;
            testOrder.Intacct_Order_ID__c = '9876543210';
            update testOrder;
        Test.stopTest();

        // verify that CloseDate is 1 day before Contract_Start_Date
        System.assertEquals(testOrder.Contract_Start_Date__c.addDays(-1), 
            [select CloseDate from Opportunity 
            where Id=:testOpp.Id limit 1].CloseDate, 'CloseDate not set on Opportunity as expected.');
    }

    @isTest
    static void testSetCloseDateForEarlierDate() {
        System.assert(testSetCloseDateForEarlierDate, 'Test disabled.');
        initData(OrderHelper.DATA_SOLUTIONS, 0);

        Date thisMonth = Date.today();
        // If this is January, need to bump it to February
        if (thisMonth.month() == 1) {
            thisMonth = thisMonth.addMonths(2);
        }
        Date prevMonth = thisMonth.addMonths(-1);        

        Test.startTest();
            testOrder.Contract_Start_Date__c = prevMonth;
            testOrder.Intacct_Order_ID__c = '9876543210';
            update testOrder;
        Test.stopTest();
        
        // verify that CloseDate is today
        System.assertEquals(Date.today(), 
            [select CloseDate from Opportunity 
            where Id=:testOpp.Id limit 1].CloseDate, 'CloseDate not set on Opportunity as expected.');
    }

    @isTest
    static void testSetCloseDateForDateInPreviousYear() {
        System.assert(testSetCloseDateForDateInPreviousYear, 'Test disabled.');
        initData(OrderHelper.DATA_SOLUTIONS, 0);

        Test.startTest();
            testOrder.Contract_Start_Date__c = Date.today().addDays(-365);
            testOrder.Order_Created_Date__c = Date.today().addDays(-30);
            testOrder.Intacct_Order_ID__c = '9876543210';
            update testOrder;
        Test.stopTest();
    }

    @isTest
    static void testSetCloseDateWithOverride() {
        System.assert(testSetCloseDateWithOverride, 'Test disabled.');
        initData(OrderHelper.DATA_SOLUTIONS, 0);

        Test.startTest();
            testOrder.Override_Start_Date__c = Date.today().addDays(-7);
            testOrder.Intacct_Order_ID__c = '9876543210';
            update testOrder;
        Test.stopTest();
        
        // verify that CloseDate today
        System.assertEquals(Date.today().addDays(-7), 
            [select CloseDate from Opportunity 
            where Id=:testOpp.Id limit 1].CloseDate, 'CloseDate not set on Opportunity as expected.');
    }


    @isTest
    static void testMissingOLI() {
        System.assert(testMissingOLI, 'Test disabled.');
        initData(OrderHelper.DATA_SOLUTIONS, 0);

        // add an OrderProduct with no matching OLI
        Product2 orphanProduct = new Product2(Name = 'Product Name 3',
            ProductCode = '6666666666', IsActive = true, Family = 'Product Family');
        insert orphanProduct;
        insert(new  Order_Product__c(Order__c=testOrder.Id,
            Sales_Price__c=8000, Users__c=1, Product__c=orphanProduct.Id));

        Test.startTest();
        testOrder.Intacct_Order_ID__c = '1234567890';
        update testOrder;
        Test.stopTest();
        // verify that other two Products were carried over and third order product was ignored
        List<OpportunityLineItem> lstOlis = [select Id, UnitPrice from OpportunityLineItem 
            where OpportunityId=:testOpp.Id];
        System.assertEquals(2, lstOlis.size(), 'Found wrong number of OLis for our Opportunity.');
    }


    @isTest
    static void testAmountDifferences() {
        System.assert(testAmountDifferences, 'Test disabled.');
        initData(OrderHelper.DATA_SOLUTIONS, 0);


        Test.startTest();
            testOrder.Amount__c = 2500;
            testOrder.Intacct_Order_ID__c = '1234567890';
            update testOrder;
        Test.stopTest();

        // now verify that Oli took on Order Product's attributes
        testOrder = [select Order_Product_Sales_Amount__c from Order__c where Id=:testOrder.Id limit 1];
        System.assertEquals(testOrder.Order_Product_Sales_Amount__c, [select Amount from Opportunity 
            where Id=:testOpp.Id limit 1].Amount, 'Amount not set on Opportunity as expected.');
    }

    @isTest
    static void testAmountDifferencesForUneven() {
        System.assert(testAmountDifferencesForUneven, 'Test disabled.');
        initData(OrderHelper.DATA_SOLUTIONS, 0);

        Id stdPBId = Test.getStandardPricebookId();

        Product2 product = new Product2(Name = 'Product Name 3',
            ProductCode = '55555', IsActive = true, Family = OrderHelper.DATA_SOLUTIONS);
        insert product;

        PricebookEntry pbe = new PricebookEntry(Pricebook2Id=stdPBId, 
            Product2Id=product.Id, UnitPrice=500, isActive=true);
        insert pbe;

        // first add a third OLI an Product to make amount not divide evenly
        Order_Product__c orderProduct = new  Order_Product__c(Order__c=testOrder.Id,
            Sales_Price__c=1000, Users__c=1,Product__c=product.Id);
        insert orderProduct;

        OpportunitylineItem oli = new OpportunityLineItem(OpportunityId = testOpp.Id,
            PricebookEntryId = pbe.Id, Quantity = 1,  
            TotalPrice = 1 * pbe.UnitPrice);
        insert oli;

        Test.startTest();
            testOrder.Amount__c = 5000;
            testOrder.Intacct_Order_ID__c = '1234567890';
            update testOrder;
        Test.stopTest();

        // now verify that Oli took on Order Product's attributes
        testOrder = [select Order_Product_Sales_Amount__c from Order__c where Id=:testOrder.Id limit 1];
        System.assertEquals(testOrder.Order_Product_Sales_Amount__c, [select Amount from Opportunity 
            where Id=:testOpp.Id limit 1].Amount, 'Amount not set on Opportunity as expected.');
    }
    
    @isTest
    static void testCopyQLIs() {
        System.assert(testCopyQLIs, 'Test disabled.');
        initData(OrderHelper.DATA_SOLUTIONS, 0, false); // false means do not create Order.

        // create a duplicate QLI for the same Product to test merge
        SBQQ__QuoteLine__c qli = new SBQQ__QuoteLine__c(SBQQ__Quote__c=testQuote.Id, 
            SBQQ__NetPrice__c=10000,
            SBQQ__Product__c=testProduct.Id, 
            SBQQ__Number__c=100, 
            SBQQ__Quantity__c=1);
        insert qli;

        // Now create an Order from Quote and set copy QLI flag to false to fire trigger
        Order__c order = new Order__c(Name='Test Order', 
            Opportunity__c=testOpp.Id, Quote__c=testQuote.Id, Amount__c=3000);
        insert order;

        Test.startTest();
            order.Copy_and_Merge_QLIs__c=true;
            update order;
        Test.stopTest();

        // verify that copy and merge field was unset
        System.assertEquals(false, [select Copy_and_Merge_QLIs__c from Order__c 
            where Id=:order.Id limit 1].Copy_and_Merge_QLIs__c,
            'Did not reset Copy_and_Merge_QLIs__c on Order to false as expected.');

        // and verify that QLIs were merged into a single Order Product
        System.assertEquals(2, [select Id from Order_Product__c where Order__c=:order.Id].size(),
            'Did not merge QLIs as expected.');

        // finally verify that the prices for all the years are aggregated
        // value should be sum of our two QLI's Net Price value which is 2*10000
        System.assertEquals(11000, [select Sales_Price__c from Order_Product__c 
            where Order__c=:order.Id and Product__c=:testProduct.Id limit 1].Sales_Price__c,
            'Failed to aggregate QLI Prices together');
    }

    private static void initData(String productFamily, Integer subTerm) {
        System.debug('==========> License = ' + userinfo.isCurrentUserLicensed('SBQQ'));
        initData(productFamily, subTerm, true);
    }

    private static void initData(String productFamily, Integer subTerm, Boolean blnCreateOrder) {

        Account testAcct = TestingUtils.createAccount('Test Account', false);
        testAcct.Site_Address_1__c = '100 some Street';
        testAcct.Site_City__c = 'Fairfax';
        testAcct.Site_State__c = 'VA';
        testAcct.Site_Postal_Code__c = '22033';
        testAcct.Site_Country__c = 'United States';
        insert testAcct;


        Id stdPBId = Test.getStandardPricebookId();

        List<Product2> lstProducts = new List<Product2>();
        testProduct = new Product2(Name = 'Product Name 1',
            ProductCode = '12345', IsActive = true, Family = productFamily, 
            Fair_Value__c='Product Fair Value', Future_Contractual_Obligation__c='Over-the-life',
            Delivery_Method__c='Email');
        if (subTerm != 0) {
            /** IF/WHEN REVENUE SCHEDULES ARE REINSTATED, RE-ADD THIS CODE
            product.Subscription_Term__c = subTerm + ' Year';
            product.CanUseRevenueSchedule = false;
            **/
        }
        lstProducts.add(testProduct);
        
        Product2 product1 = new Product2(Name = 'Product Name 2',
            ProductCode = '67890', IsActive = true, Family = productFamily, 
            Fair_Value__c='Product1 Fair Value', Future_Contractual_Obligation__c='One-time',
            Delivery_Method__c='USPS');
        if (subTerm != 0) {
            /** IF/WHEN REVENUE SCHEDULES ARE REINSTATED, RE-ADD THIS CODE
            product1.Subscription_Term__c = subTerm + ' Year';
            product1.CanUseRevenueSchedule = false;
            **/
        }
        lstProducts.add(product1);
        insert lstProducts;

        PricebookEntry pbe = new PricebookEntry(Pricebook2Id=stdPBId, 
            Product2Id=testProduct.Id, UnitPrice=1000, isActive=true);
        PricebookEntry pbe1 = new PricebookEntry(Pricebook2Id=stdPBId, 
            Product2Id=product1.Id, UnitPrice=500, isActive=true);
        insert new List<PricebookEntry> {pbe, pbe1};

        testOpp = new Opportunity(Name='Test Opp', StageName='10% - Prospect', 
            CloseDate=Date.today().addDays(30), Account=testAcct);
        insert testOpp;
        List<OpportunityLineItem> lstOlis = new List<OpportunityLineItem>();
        lstOlis.add(new OpportunityLineItem(OpportunityId = testOpp.Id,
            PricebookEntryId = pbe.Id, Quantity = 1,  
            TotalPrice = 1 * pbe.UnitPrice));
        lstOlis.add(new OpportunityLineItem(OpportunityId = testOpp.Id,
            PricebookEntryId = pbe1.Id, Quantity = 1,  
            TotalPrice = 1 * pbe1.UnitPrice));
        insert lstOlis;

        // create a Quote. The QLIs will be created from the OLIs
        // we created above so no need to create additional QLIs

        testQuote = new SBQQ__Quote__c();
        testQuote.SBQQ__Opportunity2__c=testOpp.Id;
//        testQuote.SBQQ__PaymentTerms__c='Credit Card';
        testQuote.SBQQ__Account__c = testAcct.Id;
        testQuote.SBQQ__ExpirationDate__c = Date.today().addMonths(1);
 //       testQuote.Terms_Request_Approval__c = 'Approved';
        insert testQuote;

        if (blnCreateOrder) {
            // if we are not creating a Quote then create an Order Schedule & Order
            SFDC_520_Quote__c sfdcQuote = new SFDC_520_Quote__c();
            insert sfdcQuote;
            testOrder = new Order__c(Name='Test Order', SFDC_520_Quote__c=sfdcQuote.Id,
                Opportunity__c=testOpp.Id, Amount__c=3000);
            insert testOrder;

            List<Order_Product__c> lstOrderProds = new List<Order_Product__c>();

            lstOrderProds.add(new Order_Product__c(Order__c=testOrder.Id,
                Sales_Price__c=1000, Users__c=1,Product__c=testProduct.Id));
            lstOrderProds.add(new Order_Product__c(Order__c=testOrder.Id,
                Sales_Price__c=2000, Users__c=1,Product__c=product1.Id));

            insert lstOrderProds;
        }
    }

    private static Product2 testProduct;
    private static SBQQ__Quote__c testQuote;
    private static Order__c testOrder;
    private static Opportunity testOpp;
}