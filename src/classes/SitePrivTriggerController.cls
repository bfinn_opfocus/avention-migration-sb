/******************************************************************************* 
Name              : SitePrivTriggerController
Revision History  : TBD
					 
Created/Modified by   Created/Modified Date     Requested by          Related Task/Issue             
----------------------------------------------------------------------------------------
1. Mark Robinson      02/12/2014         	    Jim Horty             
*******************************************************************************/
public class SitePrivTriggerController {

	public static void linkSitePrivToSite (List<Site_Priv__c> siteprivs) {
		List<String> sitePrivIds = new List<String>();
		List<String> siteIds = new List<String>();
		Map<String, String> sitePrivSiteID = new Map<String, String>();
		Map<String, String> siteIdBySiteIDString = new Map<String, String>();
		
		for (Site_Priv__c sitepriv : siteprivs) {
			 //if (sitepriv.Site__c != null) sitePrivIds.add(sitepriv.Site__c);
			 //if (sitepriv.Site_ID__c != null) siteIds.add(sitepriv.Site_ID__c);
			 if (sitepriv.Site__c != null) sitePrivIds.add(sitepriv.Site__c);
			 if (sitepriv.Site_ID__c != null) {
			 	system.debug('++++ ' + sitepriv.Site_ID__c + ' : ' + sitepriv.Site_ID__c.toUpperCase());
			 	siteIds.add(sitepriv.Site_ID__c.toUpperCase());
			 }
		}
		
		for (Site__c site : [SELECT Id, Site_ID__c From Site__c WHERE Site_ID__c IN :siteIds]) {
			//siteIdBySiteIDString.put(site.Site_ID__c, site.Id);
			siteIdBySiteIDString.put(site.Site_ID__c.toUpperCase(), site.Id);
		}

		for (Site__c s : [SELECT Id, Site_ID__c FROM Site__c WHERE Id IN : sitePrivIds]) {
			system.debug('++++ sitePrivSiteID.put ' + s.Id);
			sitePrivSiteID.put(s.Id, s.Site_ID__c);			
		}
		
		
		
		// SET PARENT SITE
		for (Site_Priv__c sitepriv : siteprivs) {
			system.debug('++++ sitepriv.Site__c : ' + sitepriv.Site__c);
			if (sitepriv.Site__c == null) {
				system.debug('++++ sitepriv.Site_ID__c : ' + sitepriv.Site_ID__c);
				if (sitepriv.Site_ID__c != null) {
					system.debug('++++ siteIdBySiteIDString.containsKey : ' + siteIdBySiteIDString.containsKey(sitepriv.Site_ID__c.toUpperCase()));
					List<String> lst = new List<String>();
					lst.addAll(siteIdBySiteIDString.values());
					system.debug('++++ siteIdBySiteIDString size : ' + lst.size() );
					//sitepriv.Site__c = siteIdBySiteIDString.get(sitepriv.Site_ID__c);
					if (siteIdBySiteIDString.containsKey(sitepriv.Site_ID__c.toUpperCase())) {
						sitepriv.Site__c = siteIdBySiteIDString.get(sitepriv.Site_ID__c.toUpperCase());
					}
				}
			} else {
				sitepriv.Site_ID__c = sitePrivSiteID.get(sitepriv.Site__c);
			}
		}
	}
	
	public static void incrementCreditPoolUsers(List<Site_Priv__c> newSitePrivs, Map<Id, Site_Priv__c> oldSitePrivs) {
		for (Site_Priv__c sitePriv : newSitePrivs) {
			if (sitePriv.Call_Credit_Pool_Update_Proc__c) {
				Site_Priv__c oldSitePriv = oldSitePrivs.get(sitePriv.Id);
				if (oldSitePriv.Credit_Pool_Users__c != sitePriv.Credit_Pool_Users__c && sitePriv.Credit_Pool_Users__c != 0) {
					if (oldSitePriv.Credit_Pool_Users__c != null) {
						if (sitePriv.Credit_Pool_Users__c != null) sitePriv.Credit_Pool_Users__c += oldSitePriv.Credit_Pool_Users__c;//sitePriv.Credit_Pool_Users__c += oldSitePriv.Credit_Pool_Users__c;
						else sitePriv.Credit_Pool_Users__c = oldSitePriv.Credit_Pool_Users__c;
					}
				}
			}
		}
	}
	
	public static testMethod void linkSitePrivToSite_Test() {
		Account account = TestingUtils.createAccount('TestAccount', true);
		Site__c site = TestingUtils.createSite(account);
		Site_Priv__c sp = TestingUtils.createSitePriv(site);
		update sp; // Test coverage for SitePrivIncrementCreditPoolUsers.trigger
	}
	
	
}