/*
 * class:  OpportunitySchedulesController
 * Created by OpFocus on 04/15/2015
 * Description: Controller class for OpportunitySchedules Visualforce page, to be dropped on Opportunity Products Detail Page
 *              
 */
public with sharing class OpportunitySchedulesController {

	private ApexPages.StandardController con;
	private Id lineId;

	public Decimal totalAmount {get;set;}
	public Boolean isEdit {get;set;}
	public List<OpportunityLineItemSchedule> schedules {get;set;}  

	// Constructor
	public OpportunitySchedulesController(ApexPages.StandardController con) {
		// inits	
		this.con = con;
		schedules = new List<OpportunityLineItemSchedule>();
		lineId = con.getId();
		isEdit = false;
		totalAmount = 0;

		// Get the schedules for the Opportunity Product
		schedules = Database.query('select ' + SchemaUtils.getFieldCSV(OpportunityLineItemSchedule.SobjectType) + ' from OpportunityLineItemSchedule where OpportunityLineItemId = :lineId');

		// Calculate the totals
		for (OpportunityLineItemSchedule olis : schedules) {
			totalAmount += olis.Revenue;
		}
	}

	// Show Edits
	public void doEdit() {
		isEdit = true;
	}

	// Save the schedules
	public void doSave() {

		try {
			update schedules;
			isEdit = false;
		} catch (Exception e) {
			// TODO: Do something here??	
		}

	}

	// Cancel the edits
	public void doCancel() {
		isEdit = false;
	}

	// Recalculate the totals
	public void reCalculate() {
		totalAmount = 0;
		for (OpportunityLineItemSchedule olis : schedules) {
			totalAmount += olis.Revenue;
		}	
	}

}