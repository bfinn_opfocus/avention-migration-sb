/*
** Class:  TestSite
** Created by OpFocus on June 8, 2015
** Description: Unit tests for the Site__c Object (Class and triggers)
**              
*/  
@isTest
private class TestSite {

    private static Boolean testConvertToSite = true;
    private static Boolean testUpdateHasTrialSitePrive = true;
    private static Boolean testUpdateContactFields = true;
    private static Boolean testExtendSitePrivDates = true;
    private static Boolean testCalculateSiteType = true;

    static testMethod void testConvertToSite() {
    	System.assert(testConvertToSite, 'Test disabled.');

    	Account a = createTestAccount();
    	Opportunity o = createTestOpp(a);
    	Contact c = createTestContact(a);
    	Site_Request__c sr = createTestSiteRequest(o, c, a);
    	Site_Request_Domain__c srd = createTestDomain(sr);
    	DbSchema__c schema = createTestSchema('OSNP', false, false);
    	Site_Request_Line_Item__c lineItem = createTestLineItem(sr, schema);

        //changing it to Approved will trigger the update
        sr.Status__c = 'Approved';
        update sr;
        sr = [Select Id, Site__c from Site_Request__c where Id = :sr.Id];
    	System.assertNotEquals(null, sr.Site__c);  	
    }

    static testMethod void testUpdateHasTrialSitePrive(){
        System.assert(testUpdateHasTrialSitePrive, 'Test disabled.');

        Account a = createTestAccount();
        Opportunity o = createTestOpp(a);
        Contact c = createTestContact(a);
        Site__c s = createTestSite(c, a);

        DbSchema__c schemaProd = createTestSchema('OSNP', false, false);
        DbSchema__c schemaTrial = createTestSchema('TEST_OSNP', true, false);

        //Has_Trial_SitePriv__c should be false because we only have a non-Trial priv on it
        Site_Priv__c privProd = createTestSitePriv(s, schemaProd);
        s = refreshSite(s.id);
        System.assertEquals(false, s.Has_Trial_SitePriv__c);

        //Now that we added a Trial priv Has_Trial_SitePriv__c should now be true
        Site_Priv__c privTrial = createTestSitePriv(s, schemaTrial);
        s = refreshSite(s.id);
        System.assertEquals(true, s.Has_Trial_SitePriv__c);
    }

    static testMethod void testUpdateContactFields(){
        System.assert(testUpdateContactFields, 'Test disabled.');

        Account a = createTestAccount();
        Opportunity o = createTestOpp(a);
        Contact c1 = new Contact(FirstName='John', LastName='Doe', AccountId=a.id, Email='jdoe@test.com');
        insert c1;
        Contact c2 = new Contact(FirstName='Jane', LastName='Smith', AccountId=a.id, Email='jsmith@test.com');
        insert c2;
        Site__c s = createTestSite(c1, a);

        s = refreshSite(s.id);
        System.assertEquals(c1.Email, s.Email_Address__c);

        s.Contact__c = c2.id;
        update s;
        s = refreshSite(s.id);
        System.assertEquals(c2.Email, s.Email_Address__c);
    }

    static testMethod void testExtendSitePrivDates(){
        System.assert(testExtendSitePrivDates, 'Test disabled.');

        Account a = createTestAccount();
        Opportunity o = createTestOpp(a);
        Contact c = createTestContact(a);
        Site__c s = createTestSite(c, a);

        DbSchema__c schemaProd = createTestSchema('OSNP', false, false);
        Site_Priv__c priv = createTestSitePriv(s, schemaProd);

        Date newDate = Date.Today().addDays(90);
        s.Requested_Extension_Status__c = 'Submitted';
        s.Requested_Extension_Date__c = newDate;
        update s;

        //changing it to Approved from Submitted will trigger the update
        s.Requested_Extension_Status__c = 'Approved';
        update s;

        priv = [Select id, Active_Till__c from Site_Priv__c where Id = :priv.Id];

        System.assertEquals(newDate, priv.Active_Till__c.date());
    }

    static testMethod void testCalculateSiteType(){
        System.assert(testCalculateSiteType, 'Test disabled.');

        Account a = createTestAccount();
        Opportunity o = createTestOpp(a);
        Contact c = createTestContact(a);
        Site__c s = createTestSite(c, a);

        DbSchema__c schemaProd = createTestSchema('OSNP', false, false);
        DbSchema__c schemaTrial = createTestSchema('TEST_OSNP', true, false);
        DbSchema__c schemaLegacy = createTestSchema('LEGACY_OSNP', false, true);

        //Has_Trial_SitePriv__c should be CLIENT because we only have a non-Trial priv on it
        Site_Priv__c privProd = createTestSitePriv(s, schemaProd);
        s = refreshSite(s.id);
        System.assertEquals('CLIENT', s.CUSTTYPE__c);

        //Now that we added a TRIAL priv Has_Trial_SitePriv__c should now be true
        Site_Priv__c privTrial = createTestSitePriv(s, schemaTrial);
        s = refreshSite(s.id);
        System.assertEquals(true, s.Has_Trial_SitePriv__c);
        System.assertEquals('TRIAL', s.CUSTTYPE__c);

        //Now that we added a Legacy priv Has_Legacy_SitePriv__c should now be true and we should be able to change Type
        Site_Priv__c privLegacy = createTestSitePriv(s, schemaLegacy);
        s = refreshSite(s.id);
        s.CUSTTYPE__c = 'ONESOURCE';  //legacy type
        update s;
        s = refreshSite(s.id);
        System.assertEquals(true, s.Has_Legacy_SitePriv__c);
        System.assertEquals('ONESOURCE', s.CUSTTYPE__c);
    }

    static Site__c refreshSite(Id siteId){
        return [select Id, Site_ID__c, Has_Trial_SitePriv__c, Account__c, Contact__c, Contact_Name__c, Email_Address__c, CUSTTYPE__c, Has_Legacy_SitePriv__c from Site__c where Id = :siteId];
    }

    static Site__c createTestSite(Contact c, Account a){
        String testSiteId = 'Test Site';
        Site__c s = new Site__c(Site_ID__c=testSiteId, Account__c=a.id, Contact__c=c.id);
        insert s;
        return s;
    }

    static Site_Priv__c createTestSitePriv(Site__c s, DbSchema__c schema){
        Site_Priv__c sp = new Site_Priv__c(Site__c=s.id, Schema__c=schema.id, Allocated_Users__c=1, Allowed_Users__c=1, Active_Till__c=Date.Today().addDays(30));
        insert sp;
        return sp;
    }

    static Account createTestAccount(){
        Account a = new Account(Name='Account 1');
        insert a;
        return a;
    }

    static Opportunity createTestOpp(Account a){
        Opportunity o = new Opportunity(Name='Opp1', Account=a, StageName='90% - Selected', CloseDate=Date.Today());
        insert o;
        return o;
    }

    Static Site_Request_Domain__c createTestDomain(Site_Request__c sr){
        Site_Request_Domain__c srd = new Site_Request_Domain__c(Domain__c='255.255.255.255', Site_Request__c=sr.Id);
        insert srd;
        return srd;        
    }

    static Contact createTestContact(Account a){
        Contact c = new Contact(FirstName='John', LastName='Doe', AccountId=a.id, email='jdoe@test.com');
        insert c;
        return c;        
    }

    static Site_Request__c createTestSiteRequest(Opportunity o, Contact c, Account a){
        Site_Request__c sr = new Site_Request__c(Site_Id__c = 'Test Site 1',Opportunity__c=o.id, Primary_Contact__c=c.id, Account__c=a.id, Start_Date__c=Date.Today(), End_Date__c=Date.Today().addDays(30));
        insert sr;
        return sr;        
    }

    static DbSchema__c createTestSchema(String schemaName, Boolean isTrial, Boolean isLegacy){
        DbSchema__c schema = new DbSchema__c(Is_Trial__c=isTrial, Name=schemaName, Is_Legacy_Product__c=isLegacy);
        insert schema;
        return schema;
    }

    static Site_Request_Line_Item__c createTestLineItem(Site_Request__c sr, DbSchema__c schema){
        Site_Request_Line_Item__c lineItem = new Site_Request_Line_Item__c(Site_Request__c=sr.Id, Schema__c=schema.id, Seats__c=1, Expiration_Date__c=sr.End_Date__c);
        insert lineItem;
        return lineItem;
    }
}