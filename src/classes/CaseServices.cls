public with sharing class CaseServices {

	public static Map<id, case> filterCasesToPopulateContact(List<case> cases){
		Map<id, case> casesWithUsers = new Map<id, case>();
		
		for(case ca : cases){
			if(ca.OS_User_ID__c != null && ca.ContactId == null )
				casesWithUsers.put(ca.OS_User_ID__c, ca);
		}
		return casesWithUsers;
	}  
	// This method takes a case, and its OS_USER_ID, and sets the case's Contact ID to the value found in the OS_USER_ID.  
	
	public static void populateCasesWithContact(Map<id, case> cases){

		MAP<id, OS_USER_ID__c> users = new Map<id, os_user_id__c>([Select id, contact__c from Os_User_Id__c where id in: cases.keyset()]);
		
		for(case ca : cases.values())
			ca.contactID = users.get(ca.OS_User_ID__c).contact__c;
	}
	
	
	// When Cases are Upsert/Deleted, this modifies a corresponding contact value's Most_recently_opened_created_Case__c fields
	public static void adjustRecentlyOpened(List<ID> contactIds){
		List<Contact> contacts = [SELECT Account.Name, Most_recently_opened_created_Case__c,
								 (SELECT CreatedDate FROM Cases ORDER BY CreatedDate DESC Limit 1 )
								 FROM Contact
								 WHERE ID IN: contactIds];
		List<ContactShare> contactShares = new List<ContactShare>();
		List<Contact> contactsToUpdate = new List<Contact>();
		
		for(Contact con: contacts) {
			if(con.Cases.isEmpty()){
				con.Most_recently_opened_created_Case__c = null;
				contactsToUpdate.add(con);
				
			} else if(con.Most_recently_opened_created_Case__c != con.cases[0].CreatedDate){
				DateTime dt = con.cases[0].CreatedDate;
				con.Most_recently_opened_created_Case__c = date.newinstance(dt.year(), dt.month(), dt.day());
				contactsToUpdate.add(con);
			}
		}
			if(!contactsToUpdate.isempty()){
				try{
					WithoutSharingServices.updateWithoutSharing (contactsToUpdate);
				} catch (System.DmlException e) {
					for ( Integer i=0; i < e.getNumDml(); i++){
						trigger.New[0].addError('Update of Contact Failed : "'  + e.getMessage());
					}
				}
			}	
	}  

	// When Cases are Upsert/Deleted, this modifies a corresponding contact value's Most_recently_opened_created_Case__c fields
	public static void adjustRecentlyModified(List<id> contactIds){
		List<Contact> contacts = [SELECT Account.Name, Most_recently_modified_case__c,
								  (SELECT LastModifiedDate FROM Cases ORDER BY LastModifiedDate Desc  Limit 1 )
								  FROM Contact WHERE ID IN: contactIds];
		List<ContactShare> contactShares = new List<ContactShare>();
		List<Contact> contactsToUpdate = new List<Contact>();
		
		for (Contact con: contacts) {
			
			if(con.Cases.isEmpty()){
				con.Most_recently_modified_Case__c = null;
				contactsToUpdate.add(con);
			
			} else if(con.Most_recently_modified_Case__c != con.Cases[0].LastModifiedDate){
				DateTime dt = con.cases[0].LastModifiedDate;
				con.Most_recently_modified_Case__c = date.newinstance(dt.year(), dt.month(), dt.day());
				contactsToUpdate.add(con);
			}
			
		}
			if(!contactsToUpdate.isempty()){
				try{
					WithoutSharingServices.updateWithoutSharing (contactsToUpdate);
				} catch (System.DmlException e) {
					for ( Integer i=0; i < e.getNumDml(); i++){
						trigger.New[0].addError('Update of Contact Failed : "'  + e.getMessage());
					}
				}
			}		
	}
}