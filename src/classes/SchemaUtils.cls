/*
 * class:  SchemaUtils
 * Created by OpFocus on 04/16/2015
 * Description: Utility class that has methods that can be used when 
 *              calling Various Apex Describe Related Info on SObject and SObject Fields
 */

global with sharing class SchemaUtils {
	 /**
     * Cache map of String to SObjectType from a GlobalDescribe
     */
    private static Map<String,SObjectType> GLOBAL_DESCRIBE_CACHE;

    /**
     * Cache map of SObjectType to DescribeSObjectResult
     */
    private static Map<SObjectType,DescribeSObjectResult> RESULT_CACHE;

    /**
     * Cache map of SObjectType to Map of String to SObjectField
     */
    private static Map<SObjectType,Map<String,SObjectField>> OBJECT_FIELD_CACHE;

    /**
     * Cache map of DescribeSObjectResult to Map of String,DescribeFieldResult
     */
    private static Map<DescribeSObjectResult,Map<String,DescribeFieldResult>> FIELD_RESULT_CACHE;

    /**
     * Cache map of SObjectField to DescribeFieldResult
     */
    private static Map<SObjectField,DescribeFieldResult> FIELD_TOKEN_RESULT_CACHE;

    /**
     * Cache map of SObjectType to List<ChildRelationship>
     */
    private static Map<SObjectType,List<ChildRelationship>> CHILD_RELATIONSHIP_CACHE;

    /**
     * Static block to initialize cache in
     * execution context.
     */
    static {
        GLOBAL_DESCRIBE_CACHE = Schema.getGlobalDescribe();
        RESULT_CACHE = new Map<SObjectType,DescribeSObjectResult>();
        OBJECT_FIELD_CACHE = new Map<SObjectType,Map<String,SObjectField>>();
        FIELD_RESULT_CACHE = new Map<DescribeSObjectResult,Map<String,DescribeFieldResult>>();
        FIELD_TOKEN_RESULT_CACHE = new Map<SObjectField,DescribeFieldResult>();
        CHILD_RELATIONSHIP_CACHE = new Map<SObjectType,List<ChildRelationship>>();
    }

    /**
     * Returns a token for the SObjectType found by the
     * string value of the SObject's api name.
     * @param objectName String value of the SObject API name
     * @throws SchemaServiceException when an invalid api name is passed for a given SObjectType
     * @return SObjectType
     */
    global static SObjectType getSObjectType(String objectName) {
        if (String.isEmpty(objectName))
            throw new SchemaUtilsException('Invalid Argument');
        else if (GLOBAL_DESCRIBE_CACHE.get(objectName) == null)
            throw new SchemaUtilsException('Invalid Argument');
        return GLOBAL_DESCRIBE_CACHE.get(objectName);
    }

    /**
     * Returns a DescribeSObjectResult object for a given SObjectType
     * token.
     * @param  objectToken  The SObjectType to return the
     *                      describe result for
     * @return DescribeSObjectResult
     */
    global static DescribeSObjectResult getDescribe(SObjectType objectToken) {
        if (RESULT_CACHE.get(objectToken) == null)
            RESULT_CACHE.put(objectToken,objectToken.getDescribe());
        return RESULT_CACHE.get(objectToken);
    }

    /**
     * Returns a map with key of the field api name (without namespace if same as pkg)
     * and value of the SObjectField token.
     * @param  token  SObjectType token to return the field map for.
     * @return  Map<String,SObjectField>
     */
    global static Map<String,SObjectField> getFieldMap(SObjectType token) {
        if (OBJECT_FIELD_CACHE.get(token) == null) {
            DescribeSObjectResult objectDescribe = getDescribe(token);
            Map<String,SObjectField> fieldMap = objectDescribe.fields.getMap();
            OBJECT_FIELD_CACHE.put(token,fieldMap);
        }
        return OBJECT_FIELD_CACHE.get(token);
    }

    /**
     * Returns the token for a SObject field from the SObjectType 'token' and
     * a String 'fieldName'.
     * @param token
     * @param fieldName
     * @throws SchemaServiceException when no field is found by the fieldName argument
     * @return SObjectField
     */
    global static SObjectField getSObjectField(SObjectType token, String fieldName) {
        Map<String,SObjectField> fieldMap = getFieldMap(token);
        if (fieldMap.get(fieldName) == null)
            throw new SchemaUtilsException('Invalid Argument');
        return fieldMap.get(fieldName);
    }

    /**
     * Returns the DescribeFieldResult object for a given
     * SObjectField
     * @param field SObjectField to describe
     * @return DescribeFieldResult
     */
    global static DescribeFieldResult getDescribe(SObjectField field) {
        if (FIELD_TOKEN_RESULT_CACHE.get(field) == null)
            FIELD_TOKEN_RESULT_CACHE.put(field,field.getDescribe());
        return FIELD_TOKEN_RESULT_CACHE.get(field);
    }

    /**
     * Return a field describe for a given object and field by string value for
     * each argument (objectName and fieldName)
     * @param objectName The api name for the SObject type that contains the field to describe
     * @param fieldName The api name for the field to describe
     * @return DescribeFieldResult
     */
    global static DescribeFieldResult getDescribe(String objectName, String fieldName) {
        SObjectType token = getSObjectType(objectName);
        return getDescribe(token,fieldName);
    }

    /**
     * Returns a DescribeFieldResult object for a given SObjectType and field
     * name (String value)
     * @param  objectToken  SObjectType for the SObject that holds the
     *                      field to return the describe result for.
     * @param  fieldName    String value of the api name for the field to return
     *                      the describe result for.
     * @return  DescribeFieldResult
     */
    global static DescribeFieldResult getDescribe(SObjectType objectToken, String fieldName) {
        // this map helps us not lose existing field describe result cache
        Map<String,DescribeFieldResult> cloneMap = new Map<String,DescribeFieldResult>();
        // Do we have describe field results for this sobject?
        // fieldName = fieldName.removeStart(nsp);
        if (FIELD_RESULT_CACHE.get(getDescribe(objectToken)) == null) {
            cloneMap.put(fieldName,getSObjectField(objectToken,fieldName).getDescribe());
            FIELD_RESULT_CACHE.put(getDescribe(objectToken),cloneMap);
        }
        else if (FIELD_RESULT_CACHE.get(getDescribe(objectToken)).get(fieldName) == null) {
            // our field results do not include the right field result
            // lets make sure we get a clone of the other fields before we kill the map and
            // lose the cache
            cloneMap = FIELD_RESULT_CACHE.get(getDescribe(objectToken)).clone();
            // lets add the new field describe result to the clones map
            cloneMap.put(fieldName,getSObjectField(objectToken,fieldName).getDescribe());
            // lets update the existing field result cache value for the key
            // of this objectTokens describe with the new string/describe field cache
            FIELD_RESULT_CACHE.put(getDescribe(objectToken),cloneMap);
        }
        return FIELD_RESULT_CACHE.get(getDescribe(objectToken)).get(fieldName);
    }

    /**
     * Returns a list of SObject field tokens for a given SObjectType
     * Checks accessiblity of the field for the running user (only includes
     * fields that the running user has access too)
     * @param token SObjectType
     * @return List<SObjectField>
     */
    public static List<SObjectField> getFieldList(SObjectType token) {
        List<SObjectField> results = new List<SObjectField>();
        for (SObjectField field : getFieldMap(token).values()) {
            DescribeFieldResult dfr = getDescribe(field);
            if (dfr.isAccessible()) results.add(field);
        }
        return results;
    }


    /**
     * Returns a CSV String of SObject field tokens for a given SObjectType
     * Checks accessiblity of the field for the running user (only includes
     * fields that the running user has access too)
     * @param token SObjectType
     * @return CSV String
     */
    public static String getFieldCSV(SObjectType token) {
    	FieldListConstruct listBuilder = new FieldListConstruct(getFieldList(token));
        return listBuilder.getStringValue();
    }

    /**
     * Returns a list of child relationships for a given SObjectType
     * @param token SObjectType to return child relationships for
     * @return List<ChildRelationship>
     */
    public static List<ChildRelationship> getChildren(SObjectType token) {
        if (CHILD_RELATIONSHIP_CACHE.get(token) == null) {
            DescribeSObjectResult parent = getDescribe(token);
            List<ChildRelationship> results = new List<ChildRelationship>();
            for (ChildRelationship cr : parent.getChildRelationships())
                results.add(cr);
            CHILD_RELATIONSHIP_CACHE.put(token,results);
        }
        return CHILD_RELATIONSHIP_CACHE.get(token);
    }


    // ------------------------------------------------------------------- //
    // ** StringConstruct and Related StringConstruct Helper/Utils Sub-classes //
    // ------------------------------------------------------------------- //

    /**
     * Provides stubs for building lists of fields to use for
     * SOQL queries executed by Queries
     */
    public virtual class StringConstruct {
        protected String stringValue;
        public StringConstruct() { /* constructor */ }
        public StringConstruct(List<String> values) {
            add(values);
        }
        public virtual void add(List<String> values) {
            for(String value : values)
                add(value);
        }
        public virtual void add(String value) {
            stringValue = ( stringValue == null ? value : stringValue + value );
        }
        public virtual String getStringValue() {
            return stringValue;
        }
    }

    /**
     * Extends StringConstruct to provide a utility to produce
     * a comma delimited list in a single string
     * from a list of strings
     * @type CommaDelimitedListConstruct
     */
    public virtual class CommaDelimitedListConstruct extends StringConstruct {
        public CommaDelimitedListConstruct() { /* constructor */ }
        public CommaDelimitedListConstruct(List<String> values) {
            super(values);
        }
        public virtual override void add(String value) {
            stringValue = ( stringValue == null ? '{0}' + value : stringValue + ',{0}' + value );
        }
        public override String getStringValue() {
            return getStringValue('');
        }
        public String getStringValue(String itemPrefix) {
            if (stringValue == null)
                return null;
            return String.format(stringValue, new List<String> { itemPrefix });
        }
    }

    /**
     * Extends CommadDelimitedListBuilder to provide a factory for creating
     * a comma delimited list of fields in a single string for the Queries
     * class to use to build soql queries
     * @type FieldListConstruct
     */
    public virtual class FieldListConstruct extends CommaDelimitedListConstruct {
        public FieldListConstruct(List<SObjectField> values) {
            this(values, null);
        }
        public FieldListConstruct(List<SObjectField> values, List<Fieldset> fieldSets) {
            Set<String> selectFields = new Set<String>();
            for (Schema.SObjectField value : values)
                selectFields.add(SchemaUtils.getDescribe(value).getName());
            if (fieldSets != null)
                for(Schema.Fieldset fieldSet : fieldSets)
                    for(Schema.FieldSetMember fieldSetMember : fieldSet.getFields())
                    selectFields.add(fieldSetMember.getFieldPath());
            add(new List<String>(selectFields));
        }
    }


    public class SchemaUtilsException extends Exception {}



}