/*
** Class:  SiteRequestBundleHelper
** Created by OpFocus on May 2015
** Description: Class containing methods to the Site_Request_Bundle__c custom object
*/  
public without sharing class SiteRequestBundleHelper {

	public static List<Site_Request_Bundle__c> UpdateIsTrialStatus(List<Site_Request_Bundle__c> bundlesToUpdate){
		//Set of Ids for doing SOQL
		Set<Id> setBundleIds = new Set<Id>();

		//Map to match up the line items with the bundle
		Map<Id, List<Site_Request_Bundle_Line_Item__c>> mapBundleToLineItems = new Map<Id, List<Site_Request_Bundle_Line_Item__c>>();

		for(Site_Request_Bundle__c bundle : bundlesToUpdate){
			setBundleIds.add(bundle.Id);
			mapBundleToLineItems.put(bundle.Id, new List<Site_Request_Bundle_Line_Item__c>());
		}

		//now get line times
		List<Site_Request_Bundle_Line_Item__c> lstLineItems = [Select Id, Name, Is_Trial__c, Site_Request_Bundle__c from Site_Request_Bundle_Line_Item__c where Site_Request_Bundle__c in :setBundleIds];
		
		//Populate Map
		for(Site_Request_Bundle_Line_Item__c lineItem : lstLineItems){
			mapBundleToLineItems.get(lineItem.Site_Request_Bundle__c).add(lineItem);
		}

		//Now update the bundles trial stage by looking at each of thier line items.
		//If ANY line item is a trail, then the whole bundle is a trial.
		for(Site_Request_Bundle__c bundle : bundlesToUpdate){
			bundle.Is_Trial_Bundle__c = false;
			for(Site_Request_Bundle_Line_Item__c lineItem : mapBundleToLineItems.get(bundle.id)){
				if(lineItem.Is_Trial__c == true){
					bundle.Is_Trial_Bundle__c = true;
				}
			}
			//Un-check checkbox
			bundle.Do_Update_IsTrial_Checkbox__c = false;
		}

		//Not needed if being called from a trigger, but handy for debugging/unit-testing
		return bundlesToUpdate;
	}
}