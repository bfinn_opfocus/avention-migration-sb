/******************************************************************************* 
Name              : BusinessContactEmailTriggerController
Revision History  : TBD
					 
Created/Modified by   Created/Modified Date     Requested by          Related Task/Issue             
----------------------------------------------------------------------------------------
1. Mark Robinson      02/12/2014         	    Jim Horty             
*******************************************************************************/
public class BusinessContactEmailTriggerController {

	public static void linkBusinessContactEmailToSite (List<Business_Contact_Email__c> businessContactEmails) {
		List<String> siteIds = new List<String>();
		Map<String, String> siteIdBySiteIDString = new Map<String, String>();
		
		for (Business_Contact_Email__c businessContactEmail : businessContactEmails) {
			 if (businessContactEmail.Site_ID__c != null) siteIds.add(businessContactEmail.Site_ID__c);
		}
		
		for (Site__c site : [SELECT Id, Site_ID__c From Site__c WHERE Site_ID__c IN :siteIds]) {
			siteIdBySiteIDString.put(site.Site_ID__c, site.Id);
		}
		
		// SET PARENT SITE
		for (Business_Contact_Email__c businessContactEmail : businessContactEmails) {
			if (businessContactEmail.Site__c == null) businessContactEmail.Site__c = siteIdBySiteIDString.get(businessContactEmail.Site_ID__c);
			
		}
	}
	
	public static testMethod void linkBusinessContactEmailToSite_Test() {
		Account account = TestingUtils.createAccount('TestAccount', true);
		Site__c site = TestingUtils.createSite(account);
		Business_Contact_Email__c bce = TestingUtils.createBusinessContactEmail(site);
	}
	
}