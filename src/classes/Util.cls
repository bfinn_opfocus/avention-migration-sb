global with sharing class Util {

	public class MyException extends Exception {}
	
	 global class MethodResult
	{
		webservice boolean Success;
		webservice string Message;	
		
		MethodResult(boolean succeeded,string errorMessage)
		{
			Success = succeeded;
			Message = errorMessage;
		}	
	}
	
	webservice  static MethodResult MarkOrderAsDelivered(Id orderId)
	{
		List<Order_Product__c> lstOrderProducts = new List<Order_Product__c>([Select Id,Delivered__c, Delivery_Date__c from Order_Product__c where Order__c=:orderId]);
		
		
		Savepoint sp = Database.setSavepoint();
	 	try
	 	{
			for(integer i=0;i<lstOrderProducts.size();i++)
	 		{
	 			Order_Product__c oOrderProduct = lstOrderProducts[i];
	 			oOrderProduct.Delivered__c='Yes';
	 		 	oOrderProduct.Delivery_Date__c = Date.Today();
	 		}
	 		
	 		Database.Saveresult[] aosr= Database.update(lstOrderProducts);
 			for(integer i=0; i<aosr.size();i++)
			{
			 	if(!aosr[i].Success)
				 	throw new MyException('Unable to mark the order produts as delivered:' + aosr[i].errors[0].Message);
			}
	 	}catch(Exception ex)
	 	{
	 		
	 		Database.rollback(sp);			
			return new MethodResult(false,ex.getMessage());
	 	}
	 	
		return  new MethodResult(true,'');
	}
	
	webservice  static MethodResult RollUpPendingPaymentsToCurrentOrder(Id orderId, Id quoteId)
	{
		
		List<Order_Product__c> lstInitialOrderProducts = new List<Order_Product__c>([Select Id,Sales_Price__c,Contract_Line_Amount__c from Order_Product__c where Order__c=:orderId]);
		List<SFDC_520_Quote__c>  lstQuote = new List<SFDC_520_Quote__c>([Select Id,Quote_Amount__c from SFDC_520_Quote__c where Id =:quoteId]);
		Map<Id,Order__c> mapRefreshOrders = new Map<Id,Order__c>([Select Id,Non_Invoice_Order__c  from Order__c where SFDC_520_Quote__c =:quoteId  and Initial_Order__c=false]);
		Set<Id> setRefreshOrders =mapRefreshOrders.keySet();
		
		List<Order_Product__c> lstRefreshOrderProducts = new List<Order_Product__c>([Select Id,Fair_Value__c from Order_Product__c where Order__c in :setRefreshOrders]);
		
		List<Order__c> lstRefreshOrders = mapRefreshOrders.values();
		List<Order__c> lstInitialOrder = new List<Order__c>([Select Id,Amount__c from Order__c where Id=:orderId]);
		//Clear up all the line items on the Initial Order
		// Roll up the amount by replicating quote line items.			
		
	
		lstInitialOrder[0].Amount__c =lstQuote[0].Quote_Amount__c; 
		
		List<Order_Product__c> lstOrderProductsToInsert = new List<Order_Product__c>();	
		
		/*
		for(integer i=0;i<lstQuoteLine.size();i++)
		{
			SFDC_520_QuoteLine__c quoteLine = lstQuoteLine[i];
			Order_Product__c oOrderProduct = new Order_Product__c();
	 		oOrderProduct.CurrencyIsoCode=quoteLine.CurrencyIsoCode;
	 		oOrderProduct.Name = quoteLine.Name;
	 		oOrderProduct.PMD_Product_Code__c = quoteLine.Product_Code__c;
	 		oOrderProduct.Product__c=quoteLine.Product2__c;
	 		oOrderProduct.Sales_Price__c=quoteLine.Sales_Price__c;
	 		oOrderProduct.Contract_Line_Amount__c = quoteLine.Sales_Price__c;
	 		
	 		oOrderProduct.SFDC_520_QuoteLine__c=quoteLine.Id;
	 		oOrderProduct.Users__c=quoteLine.Users__c;
	 		//Default the values from Product_ref
	 		oOrderProduct.Fair_Value__c='No';
			oOrderProduct.Future_Contractual_Obligation__c=quoteLine.Product2__r.Future_Contractual_Obligation__c;
	 		oOrderProduct.Delivery_Method__c=quoteLine.Product2__r.Delivery_Method__c;
	 		oOrderProduct.Order__c=OrderId;
	 		lstOrderProductsToInsert.Add(oOrderProduct);	
	 	} */ 
	 	
	 	//Copy the Contract_Line_Amount to the Sales_Price on the inital order, so that it will invoice for the contract amount
	 	
	 	for(integer i=0;i<lstInitialOrderProducts.size();i++)
	 	{
	 		Order_Product__c oOrderProduct = lstInitialOrderProducts[i];
	 		oOrderProduct.Sales_Price__c = oOrderProduct.Contract_Line_Amount__c;
	 	}
	 	
	 	// Mark Refresh orders as Non Invoice Orders
	 	for(integer i=0;i<lstRefreshOrders.size();i++)
	 	{
	 		 lstRefreshOrders[i].Non_Invoice_Order__c = true;
	 	}
	 	//Mark Products on the Refresh Order as Fair Value to No
	 	for(integer i=0;i<lstRefreshOrderProducts.size();i++)
	 	{
	 		 lstRefreshOrderProducts[i].Fair_Value__c = 'No';
	 	}
	 	
	 	
	 	Savepoint sp = Database.setSavepoint();
	 	
	 	try
	 	{
	 		//Begin CRUD
	 		
	 		//Delete exisiting products in the Initial Order 	
	 		if(lstInitialOrderProducts.size()>0)
			{
				Database.Saveresult[] aopur= Database.update(lstInitialOrderProducts);
				for(integer i=0; i<aopur.size();i++)
				{
					 if(!aopur[i].Success)
						 throw new MyException('One of the inital order products can not be updated:' + aopur[i].errors[0].Message);
				}
			}
			//Add Products from the Quote to the Initial Order
	 		if(lstOrderProductsToInsert.size()>0)
	 		{
	 			Database.Saveresult[] aopsr= Database.insert(lstOrderProductsToInsert);
	 			for(integer i=0; i<aopsr.size();i++)
				{
				 	if(!aopsr[i].Success)
					 	throw new MyException('One of the order products can not be added from the quote:' + aopsr[i].errors[0].Message);
				}
	 		}
	 		//Update Amount on the Initial Order
	 		
	 		
	 		Database.Saveresult[] aosr= Database.update(lstInitialOrder);
	 			for(integer i=0; i<aosr.size();i++)
				{
				 	if(!aosr[i].Success)
					 	throw new MyException('Unable to update quote amount to the Initial Order:' + aosr[i].errors[0].Message);
				}
	 		
	 		//Update refresh Orders as Non-Invoice Orders
	 		
	 		if(lstRefreshOrders.size()>0)
	 		{
	 			
	 			 aosr= Database.update(lstRefreshOrders);
	 			for(integer i=0; i<aosr.size();i++)
				{
				 	if(!aosr[i].Success)
					 	throw new MyException('One of the refresh orders can not be updated as Non-Invoice order:' + aosr[i].errors[0].Message);
				}
	 		}
	 		
	 		//Reset Fair value to 'No' on refresh order products
	 		
	 		if(lstRefreshOrderProducts.size()>0)
	 		{
	 			Database.Saveresult[] aopsr= Database.update(lstRefreshOrderProducts);
	 			for(integer i=0; i<aopsr.size();i++)
				{
				 	if(!aopsr[i].Success)
					 	throw new MyException('On one of the refresh order products, can not update Fair Value :' + aopsr[i].errors[0].Message);
				}
	 		}
	 		
	 	}catch(Exception ex)
	 	{
	 		
	 		Database.rollback(sp);			
			return new MethodResult(false,ex.getMessage());
	 	}
	 	
		return  new MethodResult(true,'');
	}
	
	Webservice static MethodResult CreditApproveAllOrders(Id quoteId,string userName)
	{
		List<Order__c> lstUnApprovedOrdersForQuote = new List<Order__c>([Select Id,Credit_Status__c ,Credit_Updated_Date__c,Credit_Updated_By__c,Executive_Status__c from Order__c where Executive_Status__c='Pending' and  SFDC_520_Quote__c =:quoteId]);
		for(integer i=0;i<lstUnApprovedOrdersForQuote.size();i++)
		{
		 	Order__c order = lstUnApprovedOrdersForQuote[i];
			order.Credit_Status__c = 'Approved'; 
			order.Credit_Updated_Date__c = DateTime.now(); 
			order.Credit_Updated_By__c = userName; 
	    }
	    
	 	Savepoint sp = Database.setSavepoint();
	    try
	    {
	    	if(lstUnApprovedOrdersForQuote.size()>0)
	 		{
	 			Database.Saveresult[] aopsr= Database.update(lstUnApprovedOrdersForQuote);
	 			for(integer i=0; i<aopsr.size();i++)
				{
				 	if(!aopsr[i].Success)
					 	throw new MyException('Can not approve one of the orders for the quote. Message:' + aopsr[i].errors[0].Message);
				}
	 		}
	    }catch(Exception ex)
	    {
	    	Database.rollback(sp);			
			return new MethodResult(false,ex.getMessage());
	    }
	    return  new MethodResult(true,'');
	}
	
	TestMethod static void testMarkOrderAsDelivered()
    {
    		Order__c order = new Order__c(name='test order');
    		insert order;
    		
    		Order_Product__c orderProduct = new Order_Product__c(Order__c=order.Id);
    		insert orderProduct;
    		
    		Order__c[] lstOrders =[Select Id from Order__c Limit 1];
    		for(integer i=0;i<lstOrders.size();i++)
    		{
    			MethodResult mr=	Util.MarkOrderAsDelivered(lstOrders[i].Id);		
    		}
    }
    
    TestMethod static void testRollUpPendingPaymentsToCurrentOrder()
    {
    		SFDC_520_Quote__c quote =[Select Id from SFDC_520_Quote__c Limit 1];
    		
    		Order__c initialorder = new Order__c(name='test order',SFDC_520_Quote__c=quote.Id,initial_Order__c=true);
    		insert initialorder;
    		
    		Order__c refreshorder = new Order__c(name='test order',SFDC_520_Quote__c=quote.Id,initial_Order__c=false);
    		insert refreshorder;
    	
    		MethodResult mr=	Util.RollUpPendingPaymentsToCurrentOrder(initialorder.Id,quote.Id );
    }
    
     TestMethod static void testCreditApproveAllOrders()
    {
    		SFDC_520_Quote__c[] quotes =[Select Id from SFDC_520_Quote__c Limit 1];
    		
    		Order__c order = new Order__c(name='test order', SFDC_520_Quote__c=quotes[0].Id);    	
    	
    		insert order;
    		
    		for(integer i=0;i<quotes.size();i++)
    		{
    			MethodResult mr=	Util.CreditApproveAllOrders(quotes[i].Id,'SomeOne' );
    		}
    }
    
    TestMethod static void testAdditional()
    {
    	 integer i=0;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    	 i++;
    
    }
    
}