@isTest
private class TestTaskServices {
	
	static final string TESTACTIVITY_SUBJECT = 'Test Subject';
	static final string TESTCONTACT_LASTNAME = 'testington';
	static final string TESTCONTACT_LASTNAME2 ='Smith';
	
	static Contact testContact;
	static Contact testContact2;
	static Task testActivity;
	static Task testActivity2;
	static Task testActivity3;
	static List<id> activityIds;
	static list<id> contactIds;
	
	static void setup(){
		testContact = TestingUtils.createContact(TESTCONTACT_LASTNAME, TESTCONTACT_LASTNAME, null, false);
		testContact2 = TestingUtils.createContact(TESTCONTACT_LASTNAME2, TESTCONTACT_LASTNAME2, null, false);
		insert new List<Contact>{testContact, testContact2};
		testActivity = new Task(WhoId = testContact.ID);
		testActivity2 = new Task(WhoId = testContact2.ID);
		testActivity3 = new Task(WhoId = testContact2.ID);
		insert new list<task>{testActivity, testActivity2, testActivity3};
		
		contactIds = new list<id>{testContact.id, testContact2.id};
		activityIds = new list<id>{testActivity.id, testActivity2.id, testActivity3.id};
	}
	
	static testMethod void testRecentlyOpened() {
		setup();
		testContact.Most_recently_created_activity__c = date.newinstance(1960, 2, 17);
		update testContact;
		
		Test.startTest();
		   TaskServices.adjustRecentlyOpened(new list<id>{testContact.id, testContact2.id});	  	
		Test.stopTest();
		
		list<Contact> adjustedContact = [Select Most_recently_created_activity__c from contact where id IN : contactIds];
		System.AssertEquals(System.Today(),adjustedContact[0].Most_recently_created_Activity__c,  'The first contact should have the same opened date as the case');
		System.AssertEquals(System.Today(),adjustedContact[1].Most_recently_created_Activity__c, 'The second contact should have the same opened date as the case');
	}


	static testMethod void testRecentlyModified() {
		setup();
		
		testActivity.subject = TESTACTIVITY_SUBJECT;
		testActivity2.subject = TESTACTIVITY_SUBJECT; 

		update (new list<task>{testActivity, testActivity2});
		
		//The Test Contact will automatically be set to the correct date by the trigger, so we need to force it to be incorrect to test this.
		testContact.Most_recently_modified_activity__c = date.newinstance(1960, 2, 17);
		update testContact;
		
		Test.startTest();
		  TaskServices.adjustRecentlyModified(new list<id>{testContact.id, testContact2.id});	  	
		Test.stopTest();
		
		
		list<contact> adjustedContact = [Select Most_recently_modified_activity__c from contact where id IN : contactIds ];
		System.AssertEquals(System.Today(), adjustedContact[0].Most_recently_modified_activity__c, 'The first contact should have the same modified date as the case'); 
		System.AssertEquals(System.Today(), adjustedContact[1].Most_recently_modified_activity__c, 'The second contact should have the same modified date as the case');
	
	}
	
}