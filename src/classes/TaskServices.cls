public with sharing class TaskServices {
	
	public static void adjustRecentlyOpened(List<ID> contactIds){
		List<Contact> contacts = [SELECT Account.Name, Most_recently_created_activity__c,
								  (SELECT CreatedDate FROM tasks ORDER BY CreatedDate DESC Limit 1 )
								  FROM Contact
								  WHERE ID IN: contactIds];
								   
		List<Contact> contactsToUpdate = new List<Contact>();
		for(Contact con: contacts){		 
			
			if (con.Tasks.isEmpty()){
				con.Most_recently_created_activity__c = null;
				contactsToUpdate.add(con);
			} else if (con.Most_recently_created_activity__c != con.tasks[0].CreatedDate){
				DateTime dt = con.tasks[0].CreatedDate;
				con.Most_recently_created_activity__c = date.newinstance(dt.year(), dt.month(), dt.day());
			  	contactsToUpdate.add(con);
			}
			if(!contactsToUpdate.isempty()){
				try{
					WithoutSharingServices.updateWithoutSharing (contactsToUpdate);
				} catch (System.DmlException e) {
					for ( Integer i=0; i < e.getNumDml(); i++){
						trigger.New[0].addError('Update of Contact Failed : "'  + e.getMessage());
					}
				}
			}			
		}
	}  
	
	public static void adjustRecentlyModified(List<ID> contactIds){
		List<Contact> contacts = [SELECT Account.Name, Most_recently_modified_activity__c,
								  (SELECT LastModifiedDate FROM tasks ORDER BY LastModifiedDate Desc  Limit 1 )
								  FROM Contact WHERE ID IN: contactIds];
		List<Contact> contactsToUpdate = new List<Contact>();
		for(Contact con: contacts){
			
			if (con.Tasks.isEmpty()){
			  con.Most_recently_modified_activity__c = null;
			  contactsToUpdate.add(con);
			} else if(con.Most_recently_modified_activity__c != con.tasks[0].LastModifiedDate){
				DateTime dt = con.tasks[0].LastModifiedDate;
				con.Most_recently_modified_activity__c = date.newinstance(dt.year(), dt.month(), dt.day());
			  	contactsToUpdate.add(con);
			}
			
			if(!contactsToUpdate.isempty()){
				try{
					WithoutSharingServices.updateWithoutSharing (contactsToUpdate);
				} catch (System.DmlException e) {
					for ( Integer i=0; i < e.getNumDml(); i++){
						trigger.New[0].addError('Update of Contact Failed : "'  + e.getMessage());
					}
				}
			}			 
		}
	}
}