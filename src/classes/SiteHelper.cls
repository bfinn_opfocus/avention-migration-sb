/*
** Class:  SiteHelper
** Created by OpFocus on May 2015
** Description: Functions to support the Site__c custom object
**              +Utility to create uniuqe 10-charater site ID
*/  
public without sharing class SiteHelper {

	public static List<Site__c> ConvertToSite(List<Site_Request__c> siteRequests){
		String defaultWelcomeTemplateDeveloperName = 'Welcome_Avention_Admin_Site_Request';
		List<Site__c> lstSitesToCreate = new List<Site__c>();
		Map<String, Site_Priv__c> mapSiteLineItemsToCreate = new Map<String, Site_Priv__c>();
		List<Site_Domain__c> lstSiteDomainsToCreate = new List<Site_Domain__c>();
		List<CRM_Credit__c> lstCreditPool = new List<CRM_Credit__c>();
		List<Email_Pool__c> lstEmailPool = new List<Email_Pool__c>();
		List<Business_Contact_Email__c> lstEmailDownloadPool =  new List<Business_Contact_Email__c>();
		
		//Used to querey Site Request Line Items
		List<Id> siteRequestIds = new List<Id>();

		//used to look up Site Requests by ID so that we can update them with the Site ID's later
		Map<Id, Site_Request__c> mapSiteRequests = new Map<Id, Site_Request__c>();

		
		for(Site_Request__c siteRequest : siteRequests){
			siteRequestIds.add(siteRequest.Id);
			mapSiteRequests.put(siteRequest.Id, siteRequest);
		}

		//Get all Site Request Line Items for each Site Request
		Map<Id, List<Site_Request_Line_Item__c>> mapSiteRequestToLineItems = new Map<Id, List<Site_Request_Line_Item__c>>();
		for(Site_Request_Line_Item__c lineItem : [Select Id, Name, AppLink__c, Expiration_Date__c, Password__c, Require_Password__c, Schema__c, Seats__c, Site_Request__c, Call_Add_Invite_Proc__c, Call_Credit_Pool_Update_Proc__c, Exclude_D_B_Worldbase_Companies__c, Insert_Site_Priv_Record__c, Email__c from Site_Request_Line_Item__c where Site_Request__c in :siteRequestIds]){
			if(!mapSiteRequestToLineItems.containsKey(lineItem.Site_Request__c)){
				mapSiteRequestToLineItems.put(lineItem.Site_Request__c, new List<Site_Request_Line_Item__c>());
			}
			mapSiteRequestToLineItems.get(lineItem.Site_Request__c).add(lineItem);
		}

		//Get all Site Request IP Address Items for each Site Request
		Map<Id, List<Site_Request_Domain__c>> mapSiteRequestToDomains = new Map<Id, List<Site_Request_Domain__c>>();
		for(Site_Request_Domain__c domain : [Select Id, Domain__c, Site_Request__c from Site_Request_Domain__c where Site_Request__c in :siteRequestIds]){
			if(!mapSiteRequestToDomains.containsKey(domain.Site_Request__c)){
				mapSiteRequestToDomains.put(domain.Site_Request__c, new List<Site_Request_Domain__c>());
			}
			mapSiteRequestToDomains.get(domain.Site_Request__c).add(domain);
		}
		
		for(Site_Request__c siteRequest : siteRequests){
			Site__c newSite = new Site__c();

			//Map fields from Site Request to Site
			newSite.Site_ID__c = siteRequest.Site_Id__c;
			newsite.Name = siteRequest.Site_Id__c;
			newSite.Account__c = siteRequest.Account__c;
			newSite.Display_Name__c = siteRequest.Display_Name__c;
			newSite.Custom_Registration__c = siteRequest.Custom_Registration__c;
			newSite.Custom_Registration_URL__c = siteRequest.Custom_Registration_URL__c;
			newsite.Password__c = siteRequest.Password__c;
			newSite.Contact__c = siteRequest.Primary_Contact__c;
			newSite.Require_Domain__c = siteRequest.Require_Domain__c;
			newSite.Require_Password__c = siteRequest.Require_Password__c;
			newSite.Check_Password__c = siteRequest.Check_Password__c;	
			newSite.Require_Project__c = siteRequest.Require_Project__c == true ? 1 : 0;
			newSite.Require_Workgroup__c = siteRequest.Require_Workgroup__c == true ? 1 : 0;
			newSite.Site_Allows_Perm_PW__c = siteRequest.Site_Allows_Perm_PW__c == true ? 1 : 0;
			newSite.Rolling_User_ID__c = siteRequest.Rolling_User_ID__c;
			newSite.Silent_Registration__c = siteRequest.Silent_Registration__c;
			newSite.User_Level_Auth__c = siteRequest.User_Level_Auth__c;
			newSite.Domain_Registration__c = siteRequest.Domain_Registration__c == true ? 1 : 0;
			newSite.Start_Date__c = siteRequest.Start_Date__c;
			newSite.Site_Request__c = siteRequest.Id;
			newsite.Email_Address__c = siteRequest.Email_Address__c;
			newsite.CUSTNUM__c = siteRequest.PMD_Custom_Number__c;
			lstSitesToCreate.add(newSite);
		}

		//insert the Site objects now so we can get Site Id's. Will make creating the rest of the objects easier
		Savepoint sp = Database.setSavepoint();
		try{
			if(!lstSitesToCreate.isEmpty()){
				insert lstSitesToCreate;
			}

			//now update the Site Requests with the Site Id's
			for(Site__c site : lstSitesToCreate){
				mapSiteRequests.get(site.Site_Request__c).Site__c = site.Id;
			}

			//Now that we have actual Site Id's for each request let's finish creating thier related objects
			for(Site_Request__c siteRequest : siteRequests){
				//Copy over line itmes
				if(mapSiteRequestToLineItems.containsKey(siteRequest.Id)){
					for(Site_Request_Line_Item__c requestLineItem : mapSiteRequestToLineItems.get(siteRequest.Id))
					{
						Site_Priv__c newSitePriv = new Site_Priv__c();
						newSitePriv.Schema__c = requestLineItem.Schema__c;
						newSitePriv.Active_Till__c = requestLineItem.Expiration_Date__c;
						newSitePriv.Active_Till__c = newSitePriv.Active_Till__c.addHours(-(Utils.getTimeZoneOffset()));
						newSitePriv.Allocated_Users__c = 0;
						newSitePriv.Allowed_Users__c = requestLineItem.Seats__c;
						newSitePriv.Password__c = requestLineItem.Password__c;
						newSitePriv.Require_Password__c = requestLineItem.Require_Password__c;
						newSitePriv.Site_Request_Line_Item__c = requestLineItem.Id;
						newSitePriv.Site__c = mapSiteRequests.get(requestLineItem.Site_Request__c).Site__c;
						newSitePriv.Call_Add_Invite_Proc__c = requestLineItem.Call_Add_Invite_Proc__c;
						newSitePriv.Call_Credit_Pool_Update_Proc__c = requestLineItem.Call_Credit_Pool_Update_Proc__c;
						newSitePriv.Exclude_D_B_Worldbase_Companies__c = requestLineItem.Exclude_D_B_Worldbase_Companies__c;
						newSitePriv.Insert_Site_Priv_Record__c = requestLineItem.Insert_Site_Priv_Record__c;
						newSitePriv.Email__c = requestLineItem.Email__c;
						newSitePriv.AdminLevel__c = siteRequest.User_Level_Auth__c == true ? 1 : 0;
						if(newSitePriv.Call_Credit_Pool_Update_Proc__c ){
							newSitePriv.Credit_Pool_Users__c =  requestLineItem.Seats__c;
						}
						mapSiteLineItemsToCreate.put(newSitePriv.Schema__c,newSitePriv);
					}
				}

				//Copy Over IP Address items
				if( mapSiteRequestToDomains.containsKey(siteRequest.Id)){
					for(Site_Request_Domain__c domain : mapSiteRequestToDomains.get(siteRequest.Id))
					{
						Site_Domain__c newDomain = new Site_Domain__c();
						newDomain.Domain__c = domain.Domain__c;
						newDomain.Site__c = mapSiteRequests.get(domain.Site_Request__c).Site__c;
						lstSiteDomainsToCreate.add(newDomain);
					}
				}

				//Update site request object: flag as converted, Set Converted date, Site ID
				siteRequest.Is_Converted__c = true;
				siteRequest.Converted_Date__c = Datetime.now();

				try{
					//Send welcome email if needed
					if(siteRequest.Send_Welcome_Email__c && siteRequest.Email_Address__c != null){
						String welcomeTemplateDeveloperName = defaultWelcomeTemplateDeveloperName;
						if(siteRequest.Welcome_Email_Template_Name__c != null){
							welcomeTemplateDeveloperName = siteRequest.Welcome_Email_Template_Name__c;
						}

						System.debug('Sending out email to ' + siteRequest.Email_Address__c + ' using template ' + welcomeTemplateDeveloperName);
						Utils.sendEmail(siteRequest.Email_Address__c, siteRequest.Id, welcomeTemplateDeveloperName);
					}
				}catch(Exception e){

					//TODO Error handling for when welcome email sends
					Id userId = UserInfo.getUserId();
					User activeUser = [Select Email From User where Id = :userId limit 1];
					List<Messaging.SingleEmailMessage> lstMsgs = new List<Messaging.SingleEmailMessage>();
					String url = System.URL.getSalesforceBaseUrl().getHost();

					Messaging.SingleEmailMessage msg = new Messaging.SingleEmailMessage();
					msg.setToAddresses(new List<String>{activeUser.Email});
					msg.setSubject('Could not send welcome email');
					msg.plainTextBody =  'There was a problem sending out a new site welcome email to ' + siteRequest.Email_Address__c + ' for site https://' + url + '/'+ siteRequest.Site__c;
					msg.plainTextBody =  msg.plainTextBody + '\nError: ' + e.getMessage(); 

					lstMsgs.add(msg);

					Messaging.sendEmail(lstMsgs);
				}
			}

			if(!mapSiteLineItemsToCreate.values().isEmpty()){
				insert mapSiteLineItemsToCreate.values();
			}
			if(!lstSiteDomainsToCreate.isEmpty()){
				insert lstSiteDomainsToCreate;
			}
			if(!lstCreditPool.isEmpty()){
				insert lstCreditPool;
			}
			if(!lstEmailPool.isEmpty()){
				insert lstEmailPool;
			}
			if(!lstEmailDownloadPool.isEmpty()){
				insert lstEmailDownloadPool;
			}
		}catch(Exception e){
			Database.rollback(sp);
		}

		return lstSitesToCreate;
	}


	public static List<Site__c> UpdateHasTrialSitePriv(List<Site__c> lstSitesToUpdate){
		//Set of Ids for doing SOQL
		Set<Id> setSiteIds = new Set<Id>();
		//Map to match all the SitePrivs to Sites
		Map<Id, List<Site_Priv__c>> mapSiteIdtoSitePrivList = new Map<Id, List<Site_Priv__c>>();

		for(Site__c site : lstSitesToUpdate){
			setSiteIds.add(site.Id);
			mapSiteIdtoSitePrivList.put(site.Id, new List<Site_Priv__c>());
		}

		//now get all the SitePrivs where the active Till date is still in the future (we don't care about expired ones)
		List<Site_Priv__c> lstSitePrivs = [Select Id, Name, Is_Trial__c, Site__c from Site_Priv__c where Active_Till__c > TODAY and Site__c in :setSiteIds];
		
		//Populate Map
		for(Site_Priv__c sitePriv : lstSitePrivs){
			mapSiteIdtoSitePrivList.get(sitePriv.Site__c).add(sitePriv);
		}

		//now let's go through each site to update and look at it's list of SitePrivs to see if there's a trial
		for(Site__c site : lstSitesToUpdate){
			site.Has_Trial_SitePriv__c = false;
			for(Site_Priv__c sitePriv : mapSiteIdtoSitePrivList.get(site.Id)){
				if(sitePriv.is_Trial__c){
					site.Has_Trial_SitePriv__c = true;
				}
			}
		}		
		//Return is not needed if calling from a trigger, but handy for debugging & unit tests
		return lstSitesToUpdate;
	}

	public static List<Site__c> UpdateLatestTrialSitePrivDate(List<Site__c> lstSitesToUpdate){
		//Set of Ids for doing SOQL
		Set<Id> setSiteIds = new Set<Id>();
		//Map to match all the SitePrivs to Sites
		Map<Id, List<Site_Priv__c>> mapSiteIdtoSitePrivList = new Map<Id, List<Site_Priv__c>>();

		for(Site__c site : lstSitesToUpdate){
			setSiteIds.add(site.Id);
			mapSiteIdtoSitePrivList.put(site.Id, new List<Site_Priv__c>());
		}

		//now get all the SitePrivs where the trial site Prives
		List<Site_Priv__c> lstSitePrivs = [Select Id, Name, Is_Trial__c, Site__c, Active_Till__c from Site_Priv__c where Is_Trial__c = true AND Site__c in :setSiteIds];
		//Populate Map
		for(Site_Priv__c sitePriv : lstSitePrivs){
			mapSiteIdtoSitePrivList.get(sitePriv.Site__c).add(sitePriv);
		}

		//now let's go through each priv and determine the latest trial's expiration date
		for(Site__c site : lstSitesToUpdate){
			DateTime latestTrialExpiration;
			for(Site_Priv__c sitePriv : mapSiteIdtoSitePrivList.get(site.Id)){
				if(sitePriv.Active_Till__c != null){
					if(latestTrialExpiration == null || sitePriv.Active_Till__c > latestTrialExpiration){
						latestTrialExpiration = sitePriv.Active_Till__c;
					}
				}
			}
			site.Latest_Trial_Expiration_Date__c = latestTrialExpiration;
		}		
		//Return is not needed if calling from a trigger, but handy for debugging & unit tests
		return lstSitesToUpdate;
	}


	public static List<Site__c> UpdateHasLegacySitePriv(List<Site__c> lstSitesToUpdate){
		//Set of Ids for doing SOQL
		Set<Id> setSiteIds = new Set<Id>();
		//Map to match all the SitePrivs to Sites
		Map<Id, List<Site_Priv__c>> mapSiteIdtoSitePrivList = new Map<Id, List<Site_Priv__c>>();

		for(Site__c site : lstSitesToUpdate){
			setSiteIds.add(site.Id);
			mapSiteIdtoSitePrivList.put(site.Id, new List<Site_Priv__c>());
		}

		//now get all the SitePrivs 
		List<Site_Priv__c> lstSitePrivs = [Select Id, Name, Is_Legacy_Product__c, Site__c from Site_Priv__c where Site__c in :setSiteIds];
		
		//Populate Map
		for(Site_Priv__c sitePriv : lstSitePrivs){
			mapSiteIdtoSitePrivList.get(sitePriv.Site__c).add(sitePriv);
		}

		//now let's go through each site to update and look at it's list of SitePrivs to see if there's a legacy
		for(Site__c site : lstSitesToUpdate){
			site.Has_Legacy_SitePriv__c = false;
			for(Site_Priv__c sitePriv : mapSiteIdtoSitePrivList.get(site.Id)){
				if(sitePriv.Is_Legacy_Product__c){
					site.Has_Legacy_SitePriv__c = true;
				}
			}
		}		
		//Return is not needed if calling from a trigger, but handy for debugging & unit tests
		return lstSitesToUpdate;
	}


	public static List<Site__c> UpdateContactFields(List<Site__c> lstSitesToUpdate){
		List<Id> lstContactIds = new List<Id>();
		for(Site__c site :lstSitesToUpdate){
			lstContactIds.add(site.Contact__c);
		}

		Map<Id, Contact> mapContacts = new Map<Id, Contact>([Select Id, Name, Site_Address_1__c, Site_City__c, Site_Postal_Code__c, Site_State__c, Site_Country__c, Title, Email, Phone, Fax from Contact where Id in :lstContactIds]);
		for(Site__c site :lstSitesToUpdate){
			Contact c = mapContacts.containsKey(site.Contact__c) ? mapContacts.get(site.Contact__c) : new Contact();
			site.Contact_Name__c = c.Name != null? c.Name : '';
			site.Contact_Title__c = c.Title != null? c.Title : '';
			site.Address__c = c.Site_Address_1__c != null? c.Site_Address_1__c : '';
			site.City__c = c.Site_City__c != null? c.Site_City__c : '';
			site.State_or_Province__c = c.Site_State__c != null? c.Site_State__c : '';
			site.Country__c = c.Site_Country__c != null? c.Site_Country__c : '';
			site.Postal_Code__c = c.Site_Postal_Code__c != null? c.Site_Postal_Code__c : '';
			site.Email_Address__c = c.Email != null? c.Email : '';
			site.Phone_Number__c = c.Phone != null? c.Phone : '';
			site.Fax_Number__c = c.Name != null? c.Fax : '';
			site.Contact_Name__c = c.Name != null? c.Name : '';
		}
		//Return is not needed if calling from a trigger, but handy for debugging & unit tests
		return lstSitesToUpdate;
	}

	public static List<Site__c> ExtendSitePrivDates(List<Site__c> lstSitesToExtendPrivs){
		List<Id> lstSiteIds = new List<Id>();
		Map<Id, Site__c> mapSites = new Map<Id, Site__c>();
		
		//build collections
		for(Site__c site : lstSitesToExtendPrivs){
			lstSiteIds.add(site.Id);
			mapSites.put(site.Id, site);
		}
		//Get all privs that we'll need to update 
		List<Site_Priv__c> lstSitePrivs = [Select Id, Extension_Request_Date__c, Site__c from Site_Priv__c where Site__c in :lstSiteIds];
		for(Site_Priv__c sitePriv : lstSitePrivs){
			sitePriv.Extension_Request_Date__c = mapSites.get(sitePriv.Site__c).Requested_Extension_Date__c;
		}

		//update the SitePrivs
		update lstSitePrivs;

		//now clear out Requested Date fields because we've already processed it
		for(Site__c site : lstSitesToExtendPrivs){
			site.Requested_Extension_Date__c = null;
			site.Requested_Extension_Status__c = null;
		}		

		//Return is not needed if calling from a trigger, but handy for debugging & unit tests
		return lstSitesToExtendPrivs;
	}

	public static List<Site__c> CalculateSiteType(List<Site__c> lstSitesToUpdate){
		for(Site__c site :lstSitesToUpdate){
			if(!site.Has_Legacy_SitePriv__c){
				if(site.Has_Trial_SitePriv__c){
					site.CUSTTYPE__c = 'TRIAL';
				}else{
					site.CUSTTYPE__c = 'CLIENT';
				}
			}
		}
		//Return is not needed if calling from a trigger, but handy for debugging & unit tests
		return lstSitesToUpdate;	
	}

	public static Set<Id> sitesThatExtendedEndDates {
		get{
			if(sitesThatExtendedEndDates == null) {
				sitesThatExtendedEndDates = new Set<Id>();
			}
			return sitesThatExtendedEndDates;
		}
		set;
	}

}