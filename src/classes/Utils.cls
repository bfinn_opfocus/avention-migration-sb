/*
** Class:  Utils
** Created by OpFocus on May 2015
** Description: This class contains utility functions to use from various classes
*/  
global without sharing class Utils {
	
	public static void sendEmail(String toAddress, String bccAddress, Id sObjectId, String templateDeveloperName){

		Contact c = [select id, Email from Contact where email = :toAddress limit 1];

		// Construct the list of emails we want to send
		List<Messaging.SingleEmailMessage> lstMsgs = new List<Messaging.SingleEmailMessage>();

		Messaging.SingleEmailMessage msg = new Messaging.SingleEmailMessage();
		msg.setSaveAsActivity(true);
		if(templateDeveloperName != null && templateDeveloperName != ''){
			msg.setTemplateId( [select id from EmailTemplate where DeveloperName=:templateDeveloperName].id );
		}
		msg.setWhatId(sObjectId);
		msg.setTargetObjectId(c.id);
		List<String> toAddresses  = new List<String>{toAddress};
		if(bccAddress != null){
			msg.setBccAddresses(new List<String>{bccAddress});
		}

		msg.setToAddresses(toAddresses);

		lstMsgs.add(msg);

		Messaging.sendEmail(lstMsgs);


	}

	public static Integer getTimeZoneOffset(){
		string strOffset = System.now().format('Z');
		string strOffsetHours = strOffset.substring(0,3);

		if(strOffsetHours.startsWith('+'))
		{
			strOffsetHours = strOffsetHours.substring(1);
		}

		integer iMinutes = 100 * integer.valueOf(strOffset.substring(3));
		double dOffset = double.valueOf(strOffsetHours + '.' + ((iMinutes) / 60));	
		return Integer.valueOf(dOffset);	
	}

    global class ButtonResult {
	    WebService Boolean success {get; private set;}
	    WebService String msg {get; private set;}
	    WebService String resultDetails {get; set;}
	   
	    global ButtonResult() {
	          this.success = true;
	          this.msg = '';
	          this.resultDetails = '';
	    }
	    global ButtonResult(Boolean success, String msg) {
	          this.success = success;
	          this.msg = msg;
	          this.resultDetails = '';
	    }   
	    global ButtonResult(Boolean success, String msg, String resultDetails) {
	          this.success = success;
	          this.msg = msg;
	          this.resultDetails = resultDetails;
	    }   
	}
}