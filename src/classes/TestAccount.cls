/*
** Class:  TestAccount
** Created by OpFocus on June 18, 2015
** Description: Unit tests for the Account object and trigger
**              
*/  
@isTest
private class TestAccount {

    private static Boolean TestAccountIdPopulation         = true;
    private static Boolean TestGeneratePmdIdForExistingAccoun         = true;

    static testMethod void TestAccountIdPopulation() {
    	System.assert(TestAccountIdPopulation, 'Test disabled.');
    	
    	//Verify that once we insert an Account record the PMD Id is set from the AutoValue
    	Account a1 = new Account(Name='Test Account');
    	System.assert(a1.PMD_Ship_To_Customer_ID_Auto_number__c == null);
    	insert a1;
    	System.assertEquals(a1.PMD_Ship_To_Customer_ID_Auto_number__c, a1.PMD_Ship_To_Customer_ID__c);

    	//Verify that if already have a PMD Id it is not over-written
    	String TestId = 'ExistingId';
    	Account a2 = new Account(Name='Test Account 2', PMD_Ship_To_Customer_ID__c = TestId);
    	insert a2;
    	System.assertEquals(TestId, a2.PMD_Ship_To_Customer_ID__c);	
    }

    static testMethod void TestGeneratePmdIdForExistingAccoun() {
    	System.assert(TestGeneratePmdIdForExistingAccoun, 'Test disabled.');

    	//Create an account and then clear out the field since the trigger will populate it
    	Account a1 = new Account(Name='Test Account');
    	insert a1;
    	a1.PMD_Ship_To_Customer_ID__c = null;
    	update a1;
    	a1 = [Select Id, PMD_Ship_To_Customer_ID__c from Account where Id = :a1.Id ];
    	System.assert(a1.PMD_Ship_To_Customer_ID__c == null);
    	Utils.ButtonResult newPmdId = AccountService.GeneratePmdIdForExistingAccountWeb(a1.Id);
    	a1 = [Select Id, PMD_Ship_To_Customer_ID__c from Account where Id = :a1.Id ];
    	System.assertEquals(newPmdId.msg, a1.PMD_Ship_To_Customer_ID__c);	
    }
}