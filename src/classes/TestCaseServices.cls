@isTest
private class TestCaseServices {
	static final string TESTCASE_SUBJECT ='Test Subject';
	static final string TESTCONTACT_LASTNAME= 'testington';
	static Contact testContact;
	static Case testCase;
	static Case testCase2;
	static Case testCase3;
	static Account testAccount;
	static OS_User_ID__c userID;
	static List<id> caseIds;


	static void setup(){
		testAccount = TestingUtils.createAccount(TESTCONTACT_LASTNAME, true);
		testContact = TestingUtils.createContact(TESTCONTACT_LASTNAME, TESTCONTACT_LASTNAME, testAccount.id, true);
		userID = TestingUtils.createOSUserID(TESTCONTACT_LASTNAME, testContact.ID, true);
		testCase = TestingUtils.createCase('OS_User_ID__c', userID.id, false);
		testCase2 = TestingUtils.createCase('OS_User_ID__c', userID.id, false);
		testCase3 = TestingUtils.createCase('OS_User_ID__c', userID.id, false);
		insert new list<case>{testCase, testCase2, testCase3};
		caseIds = new list<id>{testCase.id, testCase2.id, testCase3.id};

	}
    
	static testMethod void testRecentlyOpened() {
		setup();
		
		//The Test Contact will automatically be set to the correct date by the trigger, so we need to force it to be incorrect to test this.
		testContact.Most_recently_opened_created_case__c = date.newinstance(1960, 2, 17);
		update testContact;
		Test.startTest();
		  CaseServices.adjustRecentlyOpened(new list<id>{testContact.id});      	
		Test.stopTest();
		
		list<contact> adjustedContacts = [Select Most_recently_opened_created_Case__c from Contact where id =: testContact.id];
	//	System.AssertEquals(System.Today(), adjustedContacts[0].Most_recently_opened_created_Case__c,  'The first case should have the same opened date as the contact');
	}


	static testMethod void testRecentlyModified() {
		setup();
		
		testCase.subject = TESTCASE_SUBJECT;
		testCase2.subject = TESTCASE_SUBJECT;
		update new list<case>{testCase, testCase2};
		//The Test Contact will automatically be set to the correct date by the trigger, so we need to force it to be incorrect to test this.
		testContact.Most_recently_modified_Case__c = date.newinstance(1960, 2, 17);
		update testContact;
		
		Test.startTest();
		  CaseServices.adjustRecentlyModified(new list<id>{testContact.id});      	
		Test.stopTest();
		
		list<contact> adjustedContacts = [Select Most_recently_modified_Case__c from Contact where id =: testContact.id];
	//	System.AssertEquals(System.Today(), adjustedContacts[0].Most_recently_modified_Case__c, 'The first case should have the same modified date as the contact');

	}

	static testMethod void testFilterCases() {
		setup();
		//We want to make sure two testcases get filtered out.
		testCase2.contact = null;
		testCase3.OS_User_ID__c = null;
		update new List<case>{testCase2, testCase3};

		Test.startTest();
		  Map<ID, case> filteredCases = CaseServices.filterCasesToPopulateContact(new list<case>{testCase, testCase2, testCase3});      	
		Test.stopTest();

	//	System.AssertEquals(1, filteredCases.size(), 'Only one case should be returned when they are filtered');
	//	System.AssertEquals(testCase2.id, filteredCases.values()[0].id, 'An incorrect case is being returned during the filter.');
			
	}
	static testMethod void testPopulateCases() {
		setup();
		Map<id, case> caseWithUsers = new Map<id, Case>();
		caseWithUsers.put(userID.id, testCase);
		caseWithUsers.put(userID.id, testCase3);

		Test.startTest();
		  CaseServices.populateCasesWithContact(caseWithUsers);     	
		Test.stopTest();
		
	//	System.AssertEquals(testContact.id, userID.contact__c,  'The OS_User_Id should be populated with the contact id');
			
	}

}