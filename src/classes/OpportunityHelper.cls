/*
 * class:  OpportunityHelper
 * Created by OpFocus on 04/16/2015
 * Description: Helper Class for Opportunity Trigger,
 *              
 */
public with sharing class OpportunityHelper {
    
    // Create Opportunity Splits when Opportunity is closed won

    public class OpportunitySplits implements TriggerDispatcher.Dispatcher {
        
        public void dispatch() {
            /*
            // Get the Opportunities that are modified and has the Stage as Closed Won
            Set<Id> setOpportunityIds = new Set<Id>();
            
            for (Opportunity opp : (List<Opportunity>)Trigger.new) {
                if (opp.StageName != ((Opportunity)Trigger.oldMap.get(opp.Id)).StageName && opp.StageName.contains('Closed Won'))
                    setOpportunityIds.add(opp.Id);
            }

            List<Opportunity> closedWonOpps = new List<Opportunity>();
            List<OpportunitySplit> splits = new List<OpportunitySplit>();

            // Query to get the existing OpportunitySplits.
            String soql = 'select ' + SchemaUtils.getFieldCSV(Opportunity.SObjectType) + ', Owner.ManagerId ' + 
                            ', (Select ' + SchemaUtils.getFieldCSV(OpportunitySplit.SObjectType) + ' from OpportunitySplits)' + 
                            ' from Opportunity where Id in :setOpportunityIds';

            closedWonOpps = Database.query(soql);               

            // Iterate over opportunities and change the existing splits and add new one if needed.
            for (Opportunity opp : closedWonOpps) {
                if (opp.Amount - opp.Total_Yr_1_Amount__c > 0) {
                    if (opp.OpportunitySplits != null && opp.OpportunitySplits.size() > 0) {
                        OpportunitySplit existingSplit = opp.OpportunitySplits[0];
                        existingSplit.SplitPercentage = (opp.Total_Yr_1_Amount__c/opp.Amount)*100;
                        splits.add(existingSplit);
                        // Create a new Split record with owner's manager id
                        OpportunitySplit newSplit = new OpportunitySplit();
                        newSplit.OpportunityId = opp.Id;
                        newSplit.SplitPercentage = ((opp.Amount - opp.Total_Yr_1_Amount__c)/(opp.Amount))*100;
                        newSplit.SplitOwnerId = opp.Owner.ManagerId;
                        splits.add(newSplit);
                    }   
                }
            }

            try {
                upsert splits; // Upsert the splits 
            } catch (Exception e) {
                // TODO: do something specifically 
                System.debug('Error occured in OpportunityHelper.OpportunitySplits ' + e.getMessage());
                System.debug('***** Stack Trace *****');
                System.debug(e.getStackTraceString());
            }
            */
            
        }
    }

}