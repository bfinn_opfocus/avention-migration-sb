/*
 * class:  TriggerDispatcher
 * Created by OpFocus on 04/16/2015
 * Description: Dispatcher class for Trigger Helper classes, use this to control execution orders and 
 *              provide structure for triggers
 */
public class TriggerDispatcher {
   
   
    /**
        New Static method to verify if the Object Triggers are disabled, if settings not found returns true;
    */
   
    public static boolean isEnabled(String objectName){
        
        if(TriggerSettings__c.getInstance(objectName) != null){
            return TriggerSettings__c.getInstance(objectName).isEnabled__c; 
        }
        
        return true;
        
    }

    /* New method to get the Exclude list of usernames*/

    public static String getExcludeUsers(String objectName){

        if(TriggerSettings__c.getInstance(objectName) != null){
            if(TriggerSettings__c.getInstance(objectName).ExcludeUsers__c != null)
                return TriggerSettings__c.getInstance(objectName).ExcludeUsers__c;     
        }

        return 'NOTHING DEFINED'; // this cannot be username. 

    }
   
    public static boolean hasAlreadyRanTrigger = false; 
   
    /**
        Enum representing each of before/after CRUD events on Sobjects
    */
    
    public enum Evt {
        afterdelete, afterinsert, afterundelete,
        afterupdate, beforedelete, beforeinsert, beforeupdate   
    }

    /**
        Simplistic Dispatcher to implement on any of the event. It doesn't requires or enforces any patter except the
        method name to be "dispatch()", a developer is free to use any Trigger context variable or reuse any other
        apex class here.
    */
    public interface Dispatcher {
        void dispatch();          
    } 

    // Internal mapping of Dispatchers
    Map<String, List<Dispatcher>> eventDispatcherMapping = new Map<String, List<Dispatcher>>();

    /**
        Core API to bind Dispatchers with events
    */
    public TriggerDispatcher bind(Evt event, Dispatcher eh) {
        List<Dispatcher> Dispatchers = eventDispatcherMapping.get(event.name());
        if (Dispatchers == null) {
            Dispatchers = new List<Dispatcher>();
            eventDispatcherMapping.put(event.name(), Dispatchers);
        }
        Dispatchers.add(eh);
        return this;
    }

    /**
        Invokes correct Dispatchers as per the context of trigger and available registered Dispatchers
    */
    public void manage() {
        Evt ev = null;
         if(Trigger.isInsert && Trigger.isAfter){
            ev = Evt.afterinsert;
        } else if(Trigger.isUpdate && Trigger.isAfter){
            ev = Evt.afterupdate;
        } else if(Trigger.isUpdate && Trigger.isBefore){
            ev = Evt.beforeupdate;
        } else if(Trigger.isInsert && Trigger.isBefore){
            ev = Evt.beforeinsert;
        }  else if(Trigger.isDelete && Trigger.isBefore){
            ev = Evt.beforedelete;
        } else if(Trigger.isDelete && Trigger.isAfter){
            ev = Evt.afterdelete;
        } else if(Trigger.isundelete){
            ev = Evt.afterundelete;            
        }
        List<Dispatcher> dispatchers = eventDispatcherMapping.get(ev.name());
        if (dispatchers != null && !dispatchers.isEmpty()) {
            for (Dispatcher h : dispatchers) {
                h.dispatch();
            }
        }
    }

}