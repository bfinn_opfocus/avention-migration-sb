/*
** Class:  AccountHelper
** Created by OpFocus on June 17, 2015
** Description: 
**   Static method to get the next available "auto-generate" number for PMD_Ship_To_Customer_ID__c if it's blank
*/  
public without sharing class AccountHelper {
	
	//For some time after the PMD roll-out we may have Accounts that don't have PMD_Ship_To_Customer_ID__c values
	//This function will be for a button to call to populate that value
	public static String GeneratePmdIdForExistingAccount(String AccountId){
		String newPmdId;
		Savepoint sp = Database.setSavepoint();
		try{
			Account accountToUpdate = [Select Id, PMD_Ship_To_Customer_ID__c from Account where Id = :AccountId];
			Account dummyAccount = New Account(Name='Dummy Account');
			insert dummyAccount;
			dummyAccount = [Select Id, PMD_Ship_To_Customer_ID_Auto_number__c from Account where Id = :dummyAccount.Id];
			newPmdId = dummyAccount.PMD_Ship_To_Customer_ID_Auto_number__c;
			accountToUpdate.PMD_Ship_To_Customer_ID__c = newPmdId;
			delete dummyAccount;
			update accountToUpdate;
		}catch(Exception e){
			Database.rollback(sp);
	        String err = 'There was a problem generating a new Site ID : ' + e.getMessage();
	        System.debug('==========> ' + err);
            throw new AccountException(err);
		}
		return newPmdId;
	}

	public class AccountException extends Exception {}
}