@isTest
private class OrderScheduleTriggerTest {

	private class DocuSignFieldsSetupData
	{
		final Integer NUM_OF_ORDER_SCHEDULES = 10;
		final Integer NUM_OF_PRODUCTS = 3; //Max of 15 products can be added to a Order Schedule
		List<Product2> testProducts;
		List<SFDC_520_Quote__c> testOrderSchedules;
		List<SFDC_520_QuoteLine__c> allOrderScheduleLineItems;
		Map<Id, List<SFDC_520_QuoteLine__c>> orderScheduleIdtoLineItems;
		
		private void setupProductsOrderSchedulesAndLineItems()
		{
			testProducts = TestingUtils.createProducts( NUM_OF_PRODUCTS );
			testOrderSchedules = TestingUtils.createOrderSchedules( NUM_OF_ORDER_SCHEDULES );

			Set<Id> allProductIds = Pluck.ids( testProducts );
			allOrderScheduleLineItems = new List<SFDC_520_QuoteLine__c>();
			orderScheduleIdtoLineItems = new Map<Id, List<SFDC_520_QuoteLine__c>>();
			for( SFDC_520_Quote__c orderSchedule : testOrderSchedules )
			{
				List<SFDC_520_QuoteLine__c> lineItems = TestingUtils.buildOrderScheduleLineItems( allProductIds, orderSchedule.Id );
				allOrderScheduleLineItems.addAll( lineItems );
				orderScheduleIdtoLineItems.put( orderSchedule.Id, lineItems );
			}
		}
	}
	
	static String buildQueryString()
	{
		String queryString = 'SELECT';
		for( Integer i = 1; i <= 15; i++ )
		{
			queryString += ' Product_' + i + '_Users__c, ' + ' Product_' + i + '_Code__c,' + ' Product_' + i + '_Name__c,';
		}
		queryString += ' Products_Updated__c, (SELECT Product2__r.Name, Users__c, Product_Code__c FROM QuoteLineItem__r ORDER BY Product2__r.Name ASC ) FROM SFDC_520_Quote__c';
		return queryString;
	}
		
	static testMethod void testBeforeUpdate_setDocuSignLineItemFields_AddInitialProducts()
	{
		DocuSignFieldsSetupData testData = new DocuSignFieldsSetupData();
		testData.setupProductsOrderSchedulesAndLineItems();
		Test.startTest();
			insert testData.allOrderScheduleLineItems;
		Test.stopTest();
		
		List<SFDC_520_Quote__c> updatedOrderSchedules = Database.query( buildQueryString() );
		System.assertEquals( testData.NUM_OF_ORDER_SCHEDULES, updatedOrderSchedules.size(), 'All of the order schedules should have been returned' );
		for( SFDC_520_Quote__c orderSchedule : updatedOrderSchedules )
		{
			System.assertEquals( testData.NUM_OF_PRODUCTS, orderSchedule.QuoteLineItem__r.size(), 'All of the line items should have been returned' );
			Integer productCounter = 1;
			System.assertEquals( false, orderSchedule.Products_Updated__c, 'The order schedule should no longer be marked that products were updated' );
			for( SFDC_520_QuoteLine__c lineItem : orderSchedule.QuoteLineItem__r )
			{
				String docusignLineItemUsersField = 'Product_' + productCounter + '_Users__c';
				String docusignLineItemCodeField = 'Product_' + productCounter + '_Code__c';
				String docusignLineItemNameField = 'Product_' + productCounter + '_Name__c';
				System.assertEquals( String.valueOf(lineItem.Users__c), orderSchedule.get( docusignLineItemUsersField ), 'The docusign users field should be set correctly' );
				System.assertEquals( String.valueOf(lineItem.Product_Code__c), orderSchedule.get( docusignLineItemCodeField ), 'The docusign product code field should be set correctly' );
				System.assertEquals( String.valueOf(lineItem.Product2__r.Name), orderSchedule.get( docusignLineItemNameField ), 'The docusign product name field should be set correctly' );
				productCounter++;
			}
		}
	}

	static testMethod void testBeforeUpdate_setDocuSignLineItemFields_DeleteProducts()
	{
		DocuSignFieldsSetupData testData = new DocuSignFieldsSetupData();
		testData.setupProductsOrderSchedulesAndLineItems();
		insert testData.allOrderScheduleLineItems;
		
		List<SFDC_520_Quote__c> updatedOrderSchedules = [ SELECT Id, (SELECT Product2__r.Name, Users__c, Product_Code__c FROM QuoteLineItem__r ORDER BY Product2__r.Name ASC ) FROM SFDC_520_Quote__c ];
		List<SFDC_520_QuoteLine__c> lineItemsToDelete = new List<SFDC_520_QuoteLine__c>();
		for( SFDC_520_Quote__c updatedOrderSchedule : updatedOrderSchedules )
		{
			lineItemsToDelete.add( updatedOrderSchedule.QuoteLineItem__r[0] );
		}
		
		Test.startTest();
			delete lineItemsToDelete;
		Test.stopTest();
		
		updatedOrderSchedules = (List<SFDC_520_Quote__c>)Database.query( buildQueryString() );
		System.assertEquals( testData.NUM_OF_ORDER_SCHEDULES, updatedOrderSchedules.size(), 'All of the order schedules should have been returned' );
		for( SFDC_520_Quote__c orderSchedule : updatedOrderSchedules )
		{
			System.assertEquals( testData.NUM_OF_PRODUCTS - 1, orderSchedule.QuoteLineItem__r.size(), 'All of the remaining line items should have been returned' );
			Integer productCounter = 1;
			String docusignLineItemUsersField;
			String docusignLineItemCodeField;
			String docusignLineItemNameField;
			
			for( SFDC_520_QuoteLine__c lineItem : orderSchedule.QuoteLineItem__r )
			{
				docusignLineItemUsersField = 'Product_' + productCounter + '_Users__c';
				docusignLineItemCodeField = 'Product_' + productCounter + '_Code__c';
				docusignLineItemNameField = 'Product_' + productCounter + '_Name__c';
				
				System.assertEquals( String.valueOf(lineItem.Users__c), orderSchedule.get( docusignLineItemUsersField ), 'The docusign users field should be set correctly' );
				System.assertEquals( String.valueOf(lineItem.Product_Code__c), orderSchedule.get( docusignLineItemCodeField ), 'The docusign product code field should be set correctly' );
				System.assertEquals( String.valueOf(lineItem.Product2__r.Name), orderSchedule.get( docusignLineItemNameField ), 'The docusign product name field should be set correctly' );
				productCounter++;
			}
			
			docusignLineItemUsersField = 'Product_' + testData.NUM_OF_PRODUCTS + '_Users__c';
			docusignLineItemCodeField = 'Product_' + testData.NUM_OF_PRODUCTS + '_Code__c';
			docusignLineItemNameField = 'Product_' + testData.NUM_OF_PRODUCTS + '_Name__c';
			
			System.assertEquals( NULL, orderSchedule.get( docusignLineItemUsersField ), 'The last docusign users field should be null since there is one less product' );
			System.assertEquals( NULL, orderSchedule.get( docusignLineItemCodeField ), 'The last docusign product code field should be null since there is one less product' );
			System.assertEquals( NULL, orderSchedule.get( docusignLineItemNameField ), 'The last docusign product name field should be null since there is one less product' );
		}
	}

	static testMethod void testBeforeUpdate_setDocuSignLineItemFields_EditProducts()
	{
		final String UPDATE_STRING = 'Update';
		DocuSignFieldsSetupData testData = new DocuSignFieldsSetupData();
		testData.setupProductsOrderSchedulesAndLineItems();
		insert testData.allOrderScheduleLineItems;
		List<SFDC_520_Quote__c> oldOrderSchedules = (List<SFDC_520_Quote__c>)Database.query( buildQueryString() );
		
		for( SFDC_520_QuoteLine__c lineItem : testData.allOrderScheduleLineItems )
		{
			lineItem.Product_Code__c += UPDATE_STRING;
		}
		Test.startTest();
			update testData.allOrderScheduleLineItems;
		Test.stopTest();
		
		Map<Id,SFDC_520_Quote__c> idToUpdatedOrderSchedule = new Map<Id, SFDC_520_Quote__c> ( (List<SFDC_520_Quote__c>)Database.query( buildQueryString() ) );
		for( SFDC_520_Quote__c oldOrderSchedule : oldOrderSchedules )
		{
			SFDC_520_Quote__c newOrderSchedule = idToUpdatedOrderSchedule.get( oldOrderSchedule.Id );
			Integer productCounter = 1;
			System.assertEquals( false, newOrderSchedule.Products_Updated__c, 'The order schedule should no longer be marked that products were updated' );
			for( SFDC_520_QuoteLine__c lineItem : newOrderSchedule.QuoteLineItem__r )
			{
				String docusignLineItemCodeField = 'Product_' + productCounter + '_Code__c';
				System.assertEquals( oldOrderSchedule.get( docusignLineItemCodeField ) + UPDATE_STRING, newOrderSchedule.get( docusignLineItemCodeField ), 'The docusign product code field should have been updated' );
				productCounter++;
			}
		}
	}
}