public with sharing class CreditMemoServices {
    
    public static final string DROP = 'Drop';
    public static final string BAD_DEBT = 'Bad Debt';
    
    public static void updateMemosWithoutDroppedOrders (List <Credit_Memo__c> memos ){ 
        List<Order__c > ordersToUpdate = new List<Order__c>();
        Map<ID, Credit_Memo__c> orderIdsToMemo = new Map<ID, Credit_Memo__c>();
        for(Credit_Memo__c memo : memos) {
            if( memo.Order__c != null ) {
                orderIdsToMemo.put(memo.Order__c, memo);
            }
        }
        
        for( Order__c o : [Select ID, Dropped__c, 
                                            (Select Id From Credit_Memos__r where type__c =: DROP OR type__c =: BAD_DEBT )
                                            from Order__c 
                                            where ID IN : Pluck.ids('Order__c', memos) 
                                                  and Dropped__c = true]) {
            if(o.Credit_Memos__r.isEmpty()){
                o.Dropped__c = false;
                ordersToUpdate.add(o);
            }
        }
        if(!ordersToUpdate.isEmpty()) {
            updateOrders(ordersToUpdate, orderIdsToMemo);
        }
        
    }
    
    
    public static List<Credit_Memo__c> filterCreditMemo (List <Credit_Memo__c> memos ){
        List<Credit_Memo__c> memosToReturn = new List<Credit_Memo__c>();
        for(Credit_Memo__c memo : memos) {
            if(memo.type__c == DROP || memo.type__c == BAD_DEBT){
                memosToReturn.add(memo);
            }
        }
        return memosToReturn;
    }
    
    public static void memosDroppingOrder(List<Credit_Memo__c> memos) {
        Map<Id, Credit_Memo__c> orderIdsToMemo = new Map<Id, Credit_Memo__c>();
        Set<Order__c> orders = new Set<Order__c>();
        
        for(Credit_Memo__c memo : memos) {
             orderIdsToMemo.put(memo.order__c, memo); 
        }
        
        for (Order__c o : [Select ID, Dropped__c from Order__c where ID IN : orderIdsToMemo.keyset() and Dropped__c != true] ) {
            o.Dropped__c = true;
            orders.add(o);
        } 
        
        if(!orders.isEmpty() && (!orderIdsToMemo.isEmpty()) ){
            updateOrders(new List<Order__C>(orders), orderIdsToMemo);    
        }
    }
    
    public static void updateOrders(List<Order__c> orders, Map<Id, Credit_Memo__c> orderIdsToMemo) {
        
        try{
            update orders;
        }catch (dmlexception e){
            for (Integer i = 0; i < e.getNumDml(); i++) {
                orderIdsToMemo.get(e.getDmlID(i)).addError(e);
            }
        }
        
    }
    
    
}