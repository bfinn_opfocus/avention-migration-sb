/******************************************************************************* 
Name              : EmailPoolTriggerController
Revision History  : TBD
					 
Created/Modified by   Created/Modified Date     Requested by          Related Task/Issue             
----------------------------------------------------------------------------------------
1. Mark Robinson      02/12/2014         	    Jim Horty             
*******************************************************************************/
public class EmailPoolTriggerController {

	public static void linkEmailPoolCreditToSite (List<Email_Pool__c> emailPools) {
		List<String> siteIds = new List<String>();
		Map<String, String> siteIdBySiteIDString = new Map<String, String>();
		
		for (Email_Pool__c emailPool : emailPools) {
			 if (emailPool.Site_ID__c != null) siteIds.add(emailPool.Site_ID__c);
		}
		
		for (Site__c site : [SELECT Id, Site_ID__c From Site__c WHERE Site_ID__c IN :siteIds]) {
			siteIdBySiteIDString.put(site.Site_ID__c, site.Id);
		}
		
		// SET PARENT SITE
		for (Email_Pool__c emailPool : emailPools) {
			if (emailPool.Site__c == null) emailPool.Site__c = siteIdBySiteIDString.get(emailPool.Site_ID__c);
			
		}				
	}
	
	public static testMethod void linkEmailPoolCreditToSite_Test() {
		Account account = TestingUtils.createAccount('TestAccount', true);
		Site__c site = TestingUtils.createSite(account);
		Email_Pool__c ep = TestingUtils.createEmailPool(site);
		
	}
	
}