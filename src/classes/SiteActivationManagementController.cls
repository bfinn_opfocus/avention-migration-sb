public class SiteActivationManagementController {
	
	String activationAction;
	Site__c theSite;

	public SiteActivationManagementController(ApexPages.StandardController acon) {
		activationAction = ApexPages.currentPage().getParameters().get('activationAction');  //activationAction = 'SUSPEND';  // SUSPEND;ACTIVATE
		theSite = [SELECT Id, Status__c, (SELECT Id, Active_Till__c, Active_Till_Temp_From_Suspension__c FROM Site_Privs__r) FROM Site__c WHERE Id = :acon.getId() LIMIT 1];
	}
	
	public PageReference doSiteManagement() {
		if (activationAction == 'SUSPEND') doSuspension();
		else if (activationAction == 'ACTIVATE') doActivation();
		
		return (new ApexPages.StandardController(theSite)).view();
	}
	
	private void doSuspension() {
		system.debug('++++ doSuspension');
		
		List<Site_Priv__c> sitePrivsToSuspend = new List<Site_Priv__c>();
		
		for (Site_Priv__c sitePriv : theSite.Site_Privs__r) {
			sitePriv.Active_Till_Temp_From_Suspension__c = sitePriv.Active_Till__c;
			sitePriv.Active_Till__c = datetime.now().addDays(-1);
			sitePrivsToSuspend.add(sitePriv);
		}
		
		update sitePrivsToSuspend;
		
		theSite.Status__c = 'Suspended';
		
		update theSite;
	}
	
	private void doActivation() {
		system.debug('++++ doActivation');
		
		List<Site_Priv__c> sitePrivsToActivate = new List<Site_Priv__c>();
		
		for (Site_Priv__c sitePriv : theSite.Site_Privs__r) {
			if (sitePriv.Active_Till_Temp_From_Suspension__c != null) {
				sitePriv.Active_Till__c = sitePriv.Active_Till_Temp_From_Suspension__c;
				sitePriv.Active_Till_Temp_From_Suspension__c = null;
				sitePrivsToActivate.add(sitePriv);				
			}
		}
		
		update sitePrivsToActivate;
		
		theSite.Status__c = 'Activated';
		
		update theSite;
	}
	
}