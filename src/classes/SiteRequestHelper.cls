/*
** Class:  SiteRequestHelper
** Created by OpFocus on May 1, 2015
** Description: This class conains helper methods related to the Site Request object
*/  

public without sharing class SiteRequestHelper {

	public static List<Site_Request__c> UpdateIsTrialStatus(List<Site_Request__c> siteRequestsToUpdate){
/*
		//Set of Ids for doing SOQL
		Set<Id> setSiteRequestIds = new Set<Id>();

		//Map to match up the line items with the bundle
		Map<Id, List<Site_Request__c>> mapSiteRequestToBundles = new Map<Id, List<Site_Request_Bundle__c>>();

		for(Site_Request__c siteRequest : siteRequestsToUpdate){
			setSiteRequestIds.add(siteRequest.Id);
			mapSiteRequestToBundles.put(siteRequest.Id, new List<Site_Request_Bundle__c>());
		}

		//now get all the bundles
		List<Site_Request_Bundle__c> lstBundles = [Select Id, Name, Is_Trial_Bundle__c, Site_Request__c from Site_Request_Bundle__c where id in :setBundleIds];
		
		//Populate Map
		for(Site_Request_Bundle__c lineItem : lstLineItems){
			mapSiteRequestToBundles.get(lineItem.Site_Request__c).add(lineItem);
		}

		//Now update the bundles trial stage by looking at each of thier line items.
		//If ANY bundle is a trail, then the whole site request is a trial.
		for(Site_Request__c siteRequest : siteRequestsToUpdate){
			siteRequest.Is_Trial__c = false;
			for(Site_Request_Bundle__c lineItem : mapSiteRequestToBundles.get(siteRequest.id)){
				if(lineItem.Is_Trial_Item__c == true){
					siteRequest.Is_Trial_Bundle__c = true;
				}
			}
			//Un-check checkbox
			siteRequest.Do_Update_IsTrial_Checkbox__c = false;
		}
*/
		//Not needed if being called from a trigger, but handy for debugging/unit-testing
		return siteRequestsToUpdate;
	}
	
}