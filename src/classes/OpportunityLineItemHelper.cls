/*
 * class:  OpportunityLineItemHelper
 * Created by OpFocus on 04/10/2015
 * Description: Helper Class for Opportunity Line Item, called from Triggers
 *              
 */
public with sharing class OpportunityLineItemHelper {

    public static boolean alreadyRanSchedules = false;
    
    public class OpportunitySchedules implements TriggerDispatcher.Dispatcher {

        public void dispatch() {
            if (alreadyRanSchedules)
                return;

            List<OpportunityLineItem> lineItems = (List<OpportunityLineItem>)Trigger.new;

            // Get the details about the Parent records, Opportunity and Products,

            String soql =  'Select ' + SchemaUtils.getFieldCSV(OpportunityLineItem.SObjectType) + ', Opportunity.CloseDate, Product2.Subscription_Term__c, Product2.Tier__c, Product2.Family from ' + 
                       ' OpportunityLineItem where Id in : lineItems';

            lineItems = Database.query(soql); // Query the Line Items to get details about parents as well.            

            List<OpportunityLineItemSchedule> lstLineItemSchedules = new List<OpportunityLineItemSchedule>(); 

            for (OpportunityLineItem oli : lineItems) {
                OpportunityLineItem oldOli;
                if (Trigger.isUpdate)
                    oldOli = (OpportunityLineItem)Trigger.oldMap.get(oli.Id);
                
                if (Trigger.isInsert || (( oli.Calculate_Revenue_Schedules__c && (oli.Calculate_Revenue_Schedules__c != oldOli.Calculate_Revenue_Schedules__c)))) {
                    OpportunityLineItemSchedule lineItemSchedule;
                    Date oppCloseDate = oli.Opportunity.CloseDate;
                    if (oli.Suggested_Year_1_Schedule_Amount__c != null && oli.Product2.Subscription_Term__c != null && oli.Product2.Family != null && oli.Product2.Family.contains('Subscription')) { // The Subscription_Term__c is always populated for Subscriptions
                        
                        lineItemSchedule = new OpportunityLineItemSchedule(ScheduleDate=oppCloseDate,Revenue=oli.Suggested_Year_1_Schedule_Amount__c,Type='Revenue',OpportunityLineItemId=oli.Id);
                        lstLineItemSchedules.add(lineItemSchedule);
                        
                        // Assumption that Subscription_Term__c Picklist value always contains a number either 1 or 2 or 3  
                        if (oli.Product2.Subscription_Term__c.contains('2') || oli.Product2.Subscription_Term__c.contains('3')) {
                            lineItemSchedule = new OpportunityLineItemSchedule(ScheduleDate=oppCloseDate.addYears(+1),Revenue=oli.Suggested_Year_2_Schedule_Amount__c,Type='Revenue',OpportunityLineItemId=oli.Id);
                            lstLineItemSchedules.add(lineItemSchedule);
                        }
    
                        if (oli.Product2.Subscription_Term__c.contains('3')) {
                            lineItemSchedule = new OpportunityLineItemSchedule(ScheduleDate=oppCloseDate.addYears(+2),Revenue=oli.Suggested_Year_3_Schedule_Amount__c,Type='Revenue',OpportunityLineItemId=oli.Id);
                            lstLineItemSchedules.add(lineItemSchedule);
                        }
                    }
              }
            }

            try {
                
                insert lstLineItemSchedules; // Insert the schedules.
                alreadyRanSchedules = true; // Set this to true
        
            } catch (Exception e) {
                // TODO: do something here?
                System.debug('Error occured in OpportunityLineItemHelper.OpportunitySchedules ' + e.getMessage());
                System.debug('***** Stack Trace *****');
                System.debug(e.getStackTraceString());
            }
            }
    }

    // Update the Actual Totals (custom fields) from the Schedule Amounts defined. If none defined, used the Sale Price on Opportunity Line Item
    // Note: this method is called from a before trigger and hence pass by reference is not modified and a new map is created.

    public class OpportunityProductActualAmounts implements TriggerDispatcher.Dispatcher {

        private boolean needsUpdate;

        public OpportunityProductActualAmounts(boolean needsUpdate) {
            this.needsUpdate = needsUpdate;
        }

        public void dispatch() {
            List<OpportunityLineItem> lineItems = (List<OpportunityLineItem>)Trigger.new;   

            // Get the OpportunityLineItemSchedule children for the OpportunityLineItems,

            String soql =  'Select ' + SchemaUtils.getFieldCSV(OpportunityLineItem.SObjectType) + ', Opportunity.CloseDate, Product2.Subscription_Term__c, Product2.Tier__c ' + 
                           ',(Select ' + SchemaUtils.getFieldCSV(OpportunityLineItemSchedule.SobjectType) +  
                           ' from OpportunityLineItemSchedules order by ScheduleDate) from OpportunityLineItem where Id in :lineItems';
            
            // Map to load the OpportunityLineItems
            Map<Id,OpportunityLineItem> mapOpportunityLineItems = new Map<Id,OpportunityLineItem>((List<OpportunityLineItem>)Database.query(soql));            

            for (OpportunityLineItem oli : (List<OpportunityLineItem>)Trigger.new) {
                System.debug('oli ' + oli);
                System.debug('oli.OpportunityLineItemSchedules ' + oli.OpportunityLineItemSchedules );
                OpportunityLineItem litem = mapOpportunityLineItems.get(oli.Id);
                if (litem.OpportunityLineItemSchedules == null || litem.OpportunityLineItemSchedules.size() == 0) 
                    oli.Actual_Year_1_Amount__c = litem.TotalPrice;
                else if (litem.OpportunityLineItemSchedules.size() >= 3) {
                // Assumption that schedules are set for 3 years only we are setting them based on number of records populated.
                    oli.Actual_Year_1_Amount__c = litem.OpportunityLineItemSchedules[0].Revenue;
                    oli.Actual_Year_2_Amount__c = litem.OpportunityLineItemSchedules[1].Revenue;
                    oli.Actual_Year_3_Amount__c = litem.OpportunityLineItemSchedules[2].Revenue;
                } else if (litem.OpportunityLineItemSchedules.size() == 2) {
                    oli.Actual_Year_1_Amount__c = litem.OpportunityLineItemSchedules[0].Revenue;
                    oli.Actual_Year_2_Amount__c = litem.OpportunityLineItemSchedules[1].Revenue;
                } else if (litem.OpportunityLineItemSchedules.size() == 1) {
                    oli.Actual_Year_1_Amount__c = litem.OpportunityLineItemSchedules[0].Revenue;
                }
            }

            if (needsUpdate) {
                try {
                    lineItems = (List<OpportunityLineItem>)Trigger.new;
                    update lineItems;
                } catch (Exception e) {
                    System.debug('Error occured in OpportunityLineItemHelper.OpportunityProductActualAmounts ' + e.getMessage());
                    System.debug('***** Stack Trace *****');
                    System.debug(e.getStackTraceString());
                }
            } 
        }
    }
}