@isTest
private class CreditMemoServicesTest {

    static TestMethod void updateMemosWithoutDroppedOrdersTest (){
        
        
        List<Order__c> orders = TestingUtils.createOrdersCustom(10, 'Test Order', false);
        for(Order__c o : orders) {
            o.Dropped__c = true;
        }
        insert orders;
        
        List<Credit_Memo__c> memos =  TestingUtils.createCreditMemos(orders, false);        
        TestingUtils.dummyInsert(memos);
        
        Test.startTest();
            CreditMemoServices.updateMemosWithoutDroppedOrders(memos);
        Test.stopTest();
        List<Order__c> queriedOrders = [Select Id, Dropped__c from Order__c where ID IN : orders];
        System.AssertEquals(orders.size(), queriedOrders.size(), 'An incorrect number of orders returned');
        for(Order__c o : queriedOrders ){
            System.Assert(!o.Dropped__c, 'The order should not be dropped');
        }
        
        System.AssertEquals(0, ApexPages.getMessages().size(), 'No page messages should be returned');
    }
    
    static TestMethod void updateMemosWithoutDroppedOrdersTriggerTest (){
        
        List<Order__c> orders = TestingUtils.createOrdersCustom(10, 'Test Order', true);
        
        List<Credit_Memo__c> memos =  TestingUtils.createCreditMemos(orders, false);
        for(Credit_Memo__c memo : memos) {
            memo.type__c = CreditMemoServices.DROP;
        }
        insert memos;
        
        Test.startTest();
            delete memos;
        Test.stopTest();
        
        List<Order__c> queriedOrders = [Select Id, Dropped__c from Order__c where ID IN : orders];
        System.AssertEquals(orders.size(), queriedOrders.size(), 'An incorrect number of orders returned');
        for(Order__c o : queriedOrders ){
            System.Assert(!o.Dropped__c, 'The order should not be dropped');
        }
        
        System.AssertEquals(0, ApexPages.getMessages().size(), 'No page messages should be returned');
        
    }

    static TestMethod void filterCreditMemoTest (){
        
        
        List<Order__c> orders = TestingUtils.createOrdersCustom(30, 'test order', true);
        
        List<Credit_Memo__c>  allMemos = TestingUtils.createCreditMemos(orders, false);
        
        UnitTest.addData(allMemos).tag('All Memos').divide(3).part('Valid Memos').part('Dropped Memos').part('Debted Memos');
        UnitTest.get('Valid Memos').property('Type__c').assignFrom(new List<String>{'Valid'});
        UnitTest.get('Dropped Memos').property('Type__c').assignFrom(new List<String>{CreditMemoServices.DROP});
        UnitTest.get('Debted Memos').property('Type__c').assignFrom(new List<String>{CreditMemoServices.BAD_DEBT});
        
        List<Credit_Memo__c> actualMemos = new List<Credit_Memo__c>();
        
        Test.StartTest();
            actualMemos = CreditMemoServices.filterCreditMemo(allMemos);
        Test.StopTest();
        
        List<Credit_Memo__c> expectedMemos = UnitTest.get('All Memos').minus('Valid Memos').getList();
        
        System.AssertEquals(expectedMemos.size(), actualMemos.size(), 'An incorrect number of memos returned');
        for(Credit_Memo__c memo : actualMemos) {
            System.AssertNotEquals('Valid', memo.type__c);
        }
        
    }
    
    static TestMethod void memosDroppingOrderTest() {
        
        
        List<Order__c> orders = TestingUtils.createOrdersCustom(10, 'testorder', true);
        UnitTest.addData(orders).tag('All Orders').divide(2).part('Orders to Drop').part('Qualified Orders');
        List<Credit_Memo__c>  allMemos = TestingUtils.createCreditMemos(UnitTest.get('Orders to Drop').getList(), true);
    	
    	Test.StartTest();
    	   CreditMemoServices.memosDroppingOrder(allMemos);
    	Test.StopTest();
    	
    	Set<ID> droppedIDs = UnitTest.getIds('Orders to Drop');
    	List<Order__C> updatedOrders = [Select Id, Dropped__c from Order__C where ID IN : orders];
    	System.assertEquals(orders.size(), updatedOrders.size(), 'An incorrect number of orders have been returned');
    	
    	for(Order__c o : updatedOrders ) {
    	    if(droppedIDs.contains(o.id)){
    	        System.Assert(o.Dropped__c, 'The opportunity should be dropped');
    	    } else {
    	        System.Assert(!o.Dropped__c, 'The opportunity should not be dropped');
    	    }
    	}
    }
    
    static TestMethod void updateOrdersTest() {
    	
    	Map<Id, Credit_Memo__c> orderIDToCreditMemo = new Map<Id, Credit_Memo__c>();
    	List<Order__c> orders = TestingUtils.createOrdersCustom(10, 'Test Order', false);
    	insert orders;
    	Credit_Memo__c memo = new Credit_Memo__c();
    	
    	for(Order__c o : orders){
    	    orderIDToCreditMemo.put(o.id, memo);
    	}
    	CreditMemoServices.updateOrders(orders, orderIDToCreditMemo);
        delete orders;
    	System.AssertEquals(0, ApexPages.getMessages().size(), 'No page messages should be returned');
    	
    	Test.StartTest();
    	    CreditMemoServices.updateOrders(orders, orderIDToCreditMemo);
    	Test.StopTest();
    	
    	System.AssertEquals(1, ApexPages.getMessages().size(), 'A page message should be returned');
    	
    }
    
    static testMethod void creditMemoTriggerTest(){
        List<Order__c> orders = TestingUtils.createOrdersCustom(30, 'test order', true);
        
        List<Credit_Memo__c>  allMemos = TestingUtils.createCreditMemos(orders, false);
                
        UnitTest.addData(allMemos).tag('All Memos').divide(3).part('Valid Memos').part('Dropped Memos').part('Debted Memos');
        UnitTest.get('Valid Memos').property('Type__c').assignFrom(new List<String>{'Valid'});
        UnitTest.get('Dropped Memos').property('Type__c').assignFrom(new List<String>{CreditMemoServices.DROP});
        UnitTest.get('Debted Memos').property('Type__c').assignFrom(new List<String>{CreditMemoServices.BAD_DEBT});
        
        Test.startTest();
            insert allMemos;
        Test.stopTest();
        
        Set<ID> validOrders = Pluck.ids('Order__c', UnitTest.get('Valid Memos').getList() );
        
        List<Order__c> updatedOrders = [Select ID, Dropped__c from Order__c where ID IN:  orders];
        
        for(Order__c o : updatedOrders ){
            if(validOrders.contains(o.id)){
                System.Assert(!o.Dropped__c, 'The opportunity should not be dropped');
            } else {
                System.Assert(o.Dropped__c, 'The opportunity should be dropped');
            }
        }
        
    }
    
    
}