@isTest
private class TestCaseTrigger {

	static final string TESTCASE_SUBJECT ='Test Subject';
	static final string TESTCONTACT_LASTNAME= 'testington';
	static final string TESTCONTACT_LASTNAME2 ='Smith';
	static Contact testContact;
	static Contact testContact2;
	static Case testCase;
	static Case testCase2;
	static Case testCase3;
	static Account testAccount;
	static OS_User_ID__c userID;
	
	static void setup(){
		testAccount = TestingUtils.createAccount(TESTCONTACT_LASTNAME, true); 
		testContact = TestingUtils.createContact(TESTCONTACT_LASTNAME, TESTCONTACT_LASTNAME, testAccount.id, false);
		testContact2 = new Contact(lastname= TESTCONTACT_LASTNAME2);
		insert new List<Contact>{testContact, testContact2};
		userID = TestingUtils.createOSUserID(TESTCONTACT_LASTNAME, testContact.ID, true);
		testCase =  TestingUtils.createCase('OS_User_ID__c', userID.id, false);
		testCase2 = new Case(ContactId = testContact2.ID);
		testCase3 = new Case(ContactId = testContact2.ID);
		
	}
	//This tests to see if Most_recently_opened_created_Case__c and Most_recently_modified_Case__c values are changed on Insert/Update
	static testMethod void testUpsertCaseForRecentDates() {
	   
		setup();
	   
		Test.startTest();
			Insert new List<Case>{testCase, testCase2};
			testcase2.Subject = TESTCASE_SUBJECT;
			Update testcase2;
		Test.stopTest();
		List<contact> testContacts = [Select Id, Most_recently_opened_created_Case__c, Most_recently_modified_case__c
									  from Contact where ID IN : (new list<ID>{testContact.ID, testContact2.ID})];
//		System.AssertEquals(2, testContacts.size(), 'An incorrect number of Contacts exist');
	//	System.AssertEquals(System.Today(), testContacts[0].Most_recently_opened_created_Case__c, 'TestContact1\'s Most_recently_opened_created_Case__c is  invalid');
//		System.AssertEquals(System.Today(), testContacts[1].Most_recently_opened_created_Case__c, 'TestContact2\'s Most_recently_opened_created_Case__c is  invalid');
	//	System.AssertEquals(System.Today(), testContacts[1].Most_recently_modified_Case__c, 'TestContact2\'s Most_recently_modified_Case__c null');
		
	}
	 
	 //This tests to see if Most_recently_opened_created_Case__c and Most_recently_modified_Case__c values are removed on Delete
	 static testMethod void testDeleteCaseForRecentDates() {
		   
		setup();
		Insert new List<Case>{testCase, testCase2, testCase3};
		testCase2.Subject = TESTCASE_SUBJECT;
		Update testCase2;
		
		//Set the contact date values to yesterday to confirm that the times will change when records are deleted.
		Date dToday = System.Today();
		Date dYesterday = dToday.addDays(-1);
		testContact2.Most_recently_opened_created_Case__c = dYesterday;
		testContact2.Most_recently_modified_case__c = dYesterday;
		WithoutSharingServices.updateWithoutSharing(new List<Contact>{testContact2});
		
		List<contact> testContacts = [Select Id, Most_recently_opened_created_Case__c, Most_recently_modified_case__c
									  from Contact where ID IN : (new list<ID>{testContact.ID, testContact2.ID}) order by Most_recently_opened_created_Case__c desc];
									  
	//	System.AssertEquals(System.Today(), testContacts[0].Most_recently_opened_created_Case__c, 'TestContact1\'s Most_recently_opened_created_Case__c case incorrect');
	//	System.AssertEquals(dYesterday, testContacts[1].Most_recently_opened_created_Case__c, 'TestContact2\'s Most_recently_opened_created_Case__c case incorrect');
		
		Test.startTest();
			Delete new list<Case>{testCase, testCase2};
		Test.stopTest(); 
		
		testContacts = [Select Id, Most_recently_opened_created_Case__c, Most_recently_modified_case__c
						from Contact where ID IN : (new list<ID>{testContact.ID, testContact2.ID}) order by Most_recently_opened_created_Case__c desc];
	   
	//	System.Assert(testContacts.size() == 2, 'An incorrect number of Contacts exist');
	//	System.Assert(testContacts[0].Most_recently_opened_created_Case__c == null, 'TestContact1\'s Most_recently_opened_created_Case__c should be null');
	//	System.Assert(testContacts[0].Most_recently_modified_case__c == null, 'TestContact2\'s Most_recently_modified_case__c should be null');
	//	
		//There are two Case assigned to testContacts[1], so deleting one should revert the dates back to today
	//	System.AssertEquals(System.Today(), testContacts[1].Most_recently_opened_created_Case__c, 'TestContact2\'s Most_recently_opened_created_Case__c should be today');
	//	System.AssertEquals(System.today(), testContacts[1].Most_recently_modified_case__c, 'TestContact2\'s Most_recently_modified_case__c should be today');
	}
	
	static testMethod void testPopulateCasesWithContacts() {
		setup();
	//	System.AssertEquals(null, testCase.ContactID, 'The case should not have a contact ID');
		Test.startTest();
		insert testCase;
		Test.stopTest();
		testCase = [Select ContactID, ID from Case where id =: testcase.id limit 1];
	//	System.AssertEquals(testContact.ID, testCase.ContactID, 'The case should have the same contact ID as the contact');
	}

}