/*
** Class:  TestSiteRequestBundle
** Created by OpFocus on May 2015
** Description: Unit tests for the LeadUtils.cs Class and Lead trigger
**              
*/  
@isTest
private class TestSiteRequestBundle {

    private static Boolean TestSiteRequestBundle         = true;

    static testMethod void TestSiteRequestBundle() {

    	System.assert(TestSiteRequestBundle, 'Test disabled.');
    	
    	//Create data
    	List<Site_Request_Bundle__c> lstBundles = new List<Site_Request_Bundle__c>();
    	Site_Request_Bundle__c bundle1 = new Site_Request_Bundle__c(Name='Test Bundle 1');
    	Site_Request_Bundle__c bundle2 = new Site_Request_Bundle__c(Name='Test Bundle 2');
    	lstBundles.add(bundle1);
    	lstBundles.add(bundle2);
    	insert lstBundles;

    	DbSchema__c schema = new DbSchema__c(Name='Test Schema', Display_Name__c='Test Name', Is_Trial__c = false);
        DbSchema__c schemaTrial = new DbSchema__c(Name='Test Schema', Display_Name__c='Test Name', Is_Trial__c = true);
    	insert schema;
        insert schemaTrial;

    	List<Site_Request_Bundle_Line_Item__c> lstLineItems = new List<Site_Request_Bundle_Line_Item__c>();
    	Site_Request_Bundle_Line_Item__c bli1 = new Site_Request_Bundle_Line_Item__c(Schema__c=schemaTrial.Id, Site_Request_Bundle__c=bundle1.id);
    	Site_Request_Bundle_Line_Item__c bli2 = new Site_Request_Bundle_Line_Item__c(Schema__c=schema.Id, Site_Request_Bundle__c=bundle2.id);
    	lstLineItems.add(bli1);
    	lstLineItems.add(bli2);
    	insert lstLineItems;

    	//first test creation of line items
    	bundle1 = refreshBundle(bundle1.Id);
    	bundle2 = refreshBundle(bundle2.Id);    	
    	System.assert(bundle1.Is_Trial_Bundle__c, 'Bundle 1 should be trial');
    	System.assert(!bundle2.Is_Trial_Bundle__c, 'Bundle 2 should NOT be trial');

    	//now test after changing parent
    	bli1.Site_Request_Bundle__c = bundle2.id;
    	update bli1;
    	bundle1 = refreshBundle(bundle1.Id);
    	bundle2 = refreshBundle(bundle2.Id);
    	System.assert(!bundle1.Is_Trial_Bundle__c, 'Bundle 1 should NOT be trial');
    	System.assert(bundle2.Is_Trial_Bundle__c, 'Bundle 2 should be trial');
    }

    static Site_Request_Bundle__c refreshBundle (Id id){
    	return [select Id, Name, Is_Trial_Bundle__c from Site_Request_Bundle__c where Id = :id];
    }
}