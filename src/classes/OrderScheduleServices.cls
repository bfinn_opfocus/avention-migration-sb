public with sharing class OrderScheduleServices
{
	static final Integer MAX_PRODUCTS = 15;
	public static void setDocuSignLineItemFields( Map<Id, SFDC_520_Quote__c> idToOrderSchedule, List<SFDC_520_Quote__c> oldOrderSchedules )
	{
		Set<Id> filteredOrderScheduleIds = new Set<Id>();
		for( SFDC_520_Quote__c oldOrderSchedule : oldOrderSchedules )
		{
			SFDC_520_Quote__c newOrderSchedule = idToOrderSchedule.get( oldOrderSchedule.Id );
			if( oldOrderSchedule.Product_Count__c != newOrderSchedule.Product_Count__c || newOrderSchedule.Products_Updated__c )
			{
				filteredOrderScheduleIds.add( newOrderSchedule.Id );
			}
		}
		List<SFDC_520_Quote__c> orderSchedulesWithLineItems = [ SELECT Id, 
																(SELECT Quote__c, Product2__r.Name, Users__c, Product_Code__c
																	FROM QuoteLineItem__r ORDER
																	BY Product2__r.Name ASC )
																FROM SFDC_520_Quote__c
																WHERE Id IN :filteredOrderScheduleIds ];

		for( SFDC_520_Quote__c orderSchedule : orderSchedulesWithLineItems )
		{
			SFDC_520_Quote__c orderScheduleTriggerInstance = idToOrderSchedule.get( orderSchedule.Id );
			orderScheduleTriggerInstance.Products_Updated__c = false;
			for( Integer i = 1; i <= MAX_PRODUCTS; i++ )
			{
				SFDC_520_QuoteLine__c lineItem = (i <= orderSchedule.QuoteLineItem__r.size() ) ? orderSchedule.QuoteLineItem__r[i - 1] : new SFDC_520_QuoteLine__c();
				String docusignLineItemUsersField = 'Product_' + i + '_Users__c';
				String docusignLineItemCodeField = 'Product_' + i + '_Code__c';
				String docusignLineItemNameField = 'Product_' + i + '_Name__c';
				
				orderScheduleTriggerInstance.put( docusignLineItemUsersField, String.ValueOf(lineItem.Users__c) );
				orderScheduleTriggerInstance.put( docusignLineItemCodeField, lineItem.Product_Code__c );
				orderScheduleTriggerInstance.put( docusignLineItemNameField, ( lineItem != NULL ? lineItem.Product2__r.Name : NULL ) );
			}
		}
	}
}