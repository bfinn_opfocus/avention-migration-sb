/*
** Class: AccountService
** Created by OpFocus on May 2015
** Description: Web service method to support calls to AccountHelper.GeneratePmdIdForExistingAccount
*/  
global class AccountService {
    // Webservices can not be directly called for test classes
    // so need to separate Webservice from the code that can and needs
    // to be tested (under CreateCsaMemberFromOpportunityWeb method)
	WebService static Utils.ButtonResult GeneratePmdIdForExistingAccountWeb(String strAccountId){
        try{
	        String result = AccountHelper.GeneratePmdIdForExistingAccount(strAccountId);
	        return new Utils.ButtonResult(true, result);
	    }catch(AccountHelper.AccountException ex){
	    	return new Utils.ButtonResult(false, ex.getMessage());
	    }
    }	


}