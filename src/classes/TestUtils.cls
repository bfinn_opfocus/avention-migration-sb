/*
** Class:  TestUtils
** Created by OpFocus on June 9, 2015
** Description: Unit tests for the LeadUtils.cs Class and Lead trigger
**              
*/  
@isTest
private class TestUtils {

    private static Boolean TestUtils         = true;

    static testMethod void TestUtils() {

    	System.assert(TestUtils, 'Test disabled.');

		Account a = new Account(Name='Account 1');
		insert a;		
		Contact c = new Contact(FirstName='John', LastName='Doe', AccountId=a.id, email='jdoe@test.com');
		insert c;
		EmailTemplate t = new EmailTemplate(Body='Hello World', Subject='test subject', DeveloperName='Test_template', Name='Test', FolderId=UserInfo.getUserId(), TemplateType='text', IsActive=true);
		insert t;

		

    	Utils.sendEmail('test@test.com', a.Id, t.DeveloperName);

    	Integer offset = Utils.getTimeZoneOffset();
    	System.assertNotEquals(null, offset);
    	
    }
}