@isTest
private class CompileAndTest {
	
	//Test Competitors
	static testMethod void TestAddComp (){
	
	Account ad = new Account(name = 'testc');
	insert ad;

	
	//test bulk insert
	Opportunity oa = new Opportunity(name = 'testa', RecordTypeId = '012300000000fbH',CloseDate=Date.newInstance(2009,4,30),
	Other_CRM__c = 'InternalApp', accountid=ad.id, stageName='10% - Prospect');
	Opportunity ob = new Opportunity(name = 'testb', RecordTypeId = '012300000000fbH',CloseDate=Date.newInstance(2009,4,30),
	Primary_Competitor__c = 'Advisen - New', accountid=ad.id, stageName='10% - Prospect');
	Opportunity oc = new Opportunity(name = 'testc', RecordTypeId = '012300000000fbH',CloseDate=Date.newInstance(2009,4,30),
	Competitors__c = 'BVD', accountid=ad.id, stageName='10% - Prospect');
	Opportunity od = new Opportunity(name = 'testd', RecordTypeId = '012300000000fbH',CloseDate=Date.newInstance(2009,4,30),
	Primary_Competitor__c = 'Thomson - New', Competitors__c = 'BVD', accountid=ad.id, stageName='D - Opportunity');
	
	Opportunity[] oppty = new Opportunity[] {oa, ob, oc, od};
	insert oppty;
	
	
	oa.Primary_Competitor__c='LexisNexis - Incumbent';
	ob.Competitors__c='BVD';
	update oppty;
		
	}
	static testMethod void TestAcctUpdate()
	{
			Account a = new Account(name = 'test', CRM__c = 'SAP');
			insert a;
			
			Opportunity o = new Opportunity(name = 'test', RecordTypeId = '012300000000fbH',CloseDate=Date.newInstance(2008,4,30),
			Current_CRM__c = 'Oracle', accountid=a.id, stageName='10% - Prospect');
			insert o;
			
			Account ab = new Account(name = 'testa');
			insert ab;
			
			Opportunity ob = new Opportunity(name = 'testa', RecordTypeId = '012300000000fbH',CloseDate=Date.newInstance(2008,4,30),
			Current_CRM__c = 'SAP', accountid=ab.id, stageName='10% - Prospect');
			insert ob;
			
			Account ac = new Account(name = 'testb', CRM__c = 'SAP');
			insert ac;
			
			Opportunity oc = new Opportunity(name = 'testb', RecordTypeId = '012300000000fbH',CloseDate=Date.newInstance(2008,4,30),
			Current_CRM__c = 'SAP', accountid=ac.id, stageName='10% - Prospect');
			insert oc;
			
			Account ad = new Account(name = 'testc');
			insert ad;
			
			Opportunity od = new Opportunity(name = 'testb', RecordTypeId = '012300000000fbH',CloseDate=Date.newInstance(2008,4,30),
			Other_CRM__c = 'InternalApp', accountid=ad.id, stageName='10% - Prospect');
			insert od;
			
			Account ae = new Account(name = 'testd', Other_CRM__c = 'Internal App');
			insert ae;
			
			Opportunity oe = new Opportunity(name = 'testb', RecordTypeId = '012300000000fbH',CloseDate=Date.newInstance(2008,4,30),
			Current_CRM__c = 'Oracle', accountid=ae.id, stageName='10% - Prospect');
			insert oe;

}
	static testMethod void TestBtoA(){
		
	Bargain_to_Advance_Scorecard__c btoa1 = new Bargain_to_Advance_Scorecard__c(Opportunity__c='0066000000AJmXz',Product_Discussion__c=True,NDA__c=True);
	Bargain_to_Advance_Scorecard__c btoa2 = new Bargain_to_Advance_Scorecard__c(Opportunity__c='0066000000AaTzB',Discount__c=True,Org_Chart__c=True);
	Bargain_to_Advance_Scorecard__c btoa3 = new Bargain_to_Advance_Scorecard__c(Opportunity__c='00660000009S15o');
	Bargain_to_Advance_Scorecard__c[] barg = new Bargain_to_Advance_Scorecard__c[] {btoa1, btoa2, btoa3};
	insert barg;
	
	btoa1.Free_Trial__c=True;
	btoa2.Demo__c=True;
	update barg;

}
static testMethod void TestNewLead(){
		
	Lead lead1 = new Lead(LastName='Test1', Company='Test1 Inc.', Email='test1@duptest.com', LeadSource = 'Web');
	Lead lead2 = new Lead(LastName='Test2', Company='Test2 Inc.', Email='test4@duptest.com', LeadSource = 'SFDC-TD|Account Intelligence');
	Lead lead3 = new Lead(LastName='Test3', Company='Test3 Inc.', Email='test5@duptest.com', LeadSource = 'SFDC-TD|Account Intelligence');
	Lead[] leads = new Lead[] {lead1, lead2, lead3};
	insert leads;
	
	lead2.Email = 'test2@duptest.com';
	lead3.Email = 'test3@duptest.com';
	update leads;

}
	
	TestMethod static void testAccountTrigger()
	{
		Account a = new Account(name = 'test', CRM__c = 'SAP', Admin_Credit_Hold_Type__c='BadCredit');
		insert a;
		a.Admin_Credit_Hold_Type__c = null;
		update a; 
		a.Admin_Credit_Hold_Type__c = 'Bad Credit';
		update a;
	}
	
	TestMethod static void testOrderScheduleTrigger()
	{
		SFDC_520_Quote__c q = new SFDC_520_Quote__c(Subscription_Start_Date__c=Date.Today(),Contract_Length__c=6);
		insert q;
		
		q.Contract_Length__c=12;
		update q;
		
	}
	
	TestMethod static void testUpdateOpptyOwner()
	{
		Opportunity o = new Opportunity(autocreated__c ='yes');
		Account a  = [Select Id from account where owner.isActive = true limit 1];
		o.AccountId = a.Id;
		o.Name='Test';
		o.StageName='Closed';
		o.CloseDate = Date.Today();
		
		insert o;
	}
	
	TestMethod static void testCreditMemo()
	{
		Order__c order1 = new Order__c(Contract_Amount__c=5000, Contract_End_Date__c=Date.Today().addMonths(12).addDays(-1), Contract_Start_Date__c=Date.Today(), Subscription_End_Date__c=Date.Today().addMonths(12).addDays(-1), Subscription_Start_Date__c=Date.Today());
		Order__c order2 = new Order__c(Contract_Amount__c=5000, Contract_End_Date__c=Date.Today().addMonths(12).addDays(-1), Contract_Start_Date__c=Date.Today(), Subscription_End_Date__c=Date.Today().addMonths(12).addDays(-1), Subscription_Start_Date__c=Date.Today());
		
		insert order1;
		insert order2;
		
		order1.Contract_Amount__c=4000;
		update order1;		
				
		Order_Product__c orderProduct1 = new  Order_Product__c(Order__c=order1.Id,Contract_Line_Amount__c=10,Sales_Price__c=10,Users__c=10,Contract_Line_Number__c=2);
		Order_Product__c orderProduct2 = new  Order_Product__c(Order__c=order2.Id,Contract_Line_Amount__c=10,Sales_Price__c=10,Users__c=10,Contract_Line_Number__c=2);
		
		insert orderProduct1;
		insert orderProduct2;
		
		
		Credit_Memo__c cmDropValid = new Credit_Memo__c(Type__c='Drop',Order_Drop_Date__c=Date.Today(),New_Contract_End_Date__c=Date.Today(),Order__c=order1.Id,Reason_Code__c='abc',Reason_Comments__c='test' );
		Credit_Memo__c cmDropNotValid = new Credit_Memo__c(Type__c='Drop',Order_Drop_Date__c=Date.Today().addMonths(1).addDays(-1),New_Contract_End_Date__c=Date.Today().addMonths(1).addDays(-1) ,Order__c=order2.Id,Reason_Code__c='abc',Reason_Comments__c='test');
	
		insert cmDropValid;
		insert cmDropNotValid;
		
		cmDropNotValid.Order_Drop_Date__c=Date.Today().addMonths(2).addDays(-1);
		cmDropNotValid.New_Contract_End_Date__c=Date.Today().addMonths(2).addDays(-1);
		
		update cmDropNotValid;
		Credit_Memo__c cmPriceAdjustment = new Credit_Memo__c(Type__c='Price Adjustment',Order__c=order1.Id,Installment_No_To_Adjust__c=1,Reason_Code__c='abc',Reason_Comments__c='test');
		insert cmPriceAdjustment;
		Credit_Memo_Line_Item__c cmliPriceAdjustment = new Credit_Memo_Line_Item__c(Contract_Line_Number__c=2,Credit_Memo__c =cmPriceAdjustment.Id);
		insert cmliPriceAdjustment;
	}
	TestMethod static void testDebitMemo()
	{
		Order__c order1 = new Order__c(Contract_Amount__c=5000, Contract_End_Date__c=Date.Today().addMonths(12).addDays(-1), Contract_Start_Date__c=Date.Today(), Subscription_End_Date__c=Date.Today().addMonths(12).addDays(-1), Subscription_Start_Date__c=Date.Today());
		Order__c order2 = new Order__c(Contract_Amount__c=5000, Contract_End_Date__c=Date.Today().addMonths(12).addDays(-1), Contract_Start_Date__c=Date.Today(), Subscription_End_Date__c=Date.Today().addMonths(12).addDays(-1), Subscription_Start_Date__c=Date.Today());
		
		insert order1;
		insert order2;
		
		order1.Contract_Amount__c=4000;
		update order1;		
				
		Order_Product__c orderProduct1 = new  Order_Product__c(Order__c=order1.Id,Contract_Line_Amount__c=10,Sales_Price__c=10,Users__c=10,Contract_Line_Number__c=2);
		Order_Product__c orderProduct2 = new  Order_Product__c(Order__c=order2.Id,Contract_Line_Amount__c=10,Sales_Price__c=10,Users__c=10,Contract_Line_Number__c=2);
		
		insert orderProduct1;
		insert orderProduct2;
		
		
		Credit_Memo__c cmDropValid = new Credit_Memo__c(Type__c='Drop',Order_Drop_Date__c=Date.Today(),New_Contract_End_Date__c=Date.Today(),Order__c=order1.Id,Reason_Code__c='abc',Reason_Comments__c='test' );
		Credit_Memo__c cmDropNotValid = new Credit_Memo__c(Type__c='Drop',Order_Drop_Date__c=Date.Today().addMonths(1).addDays(-1) ,New_Contract_End_Date__c=Date.Today().addMonths(1).addDays(-1),Order__c=order2.Id,Reason_Code__c='abc',Reason_Comments__c='test');
	
		insert cmDropValid;
		insert cmDropNotValid;
		
		cmDropNotValid.Order_Drop_Date__c=Date.Today().addMonths(2).addDays(-1);
		cmDropNotValid.New_Contract_End_Date__c=Date.Today().addMonths(2).addDays(-1);
		update cmDropNotValid;
		Credit_Memo__c cmPriceAdjustment = new Credit_Memo__c(Type__c='Price Adjustment',Order__c=order1.Id,Installment_No_To_Adjust__c=1,Reason_Code__c='abc',Reason_Comments__c='test');
		insert cmPriceAdjustment;
		Credit_Memo_Line_Item__c cmliPriceAdjustment = new Credit_Memo_Line_Item__c(Contract_Line_Number__c=2,Credit_Memo__c =cmPriceAdjustment.Id);
		insert cmliPriceAdjustment;
		
		Debit_Memo__c dmDrop = new Debit_Memo__c(Order__c=order1.Id,Credit_Memo__c = cmDropValid.Id,Reason_Code__c='abc',Reason_Comments__c='test', Type__c='Return for Credit');
		insert dmDrop;
		
		Debit_Memo__c dmPriceAdjustment = new Debit_Memo__c(Order__c=order1.Id,Credit_Memo__c = cmPriceAdjustment.Id,Reason_Code__c='abc',Reason_Comments__c='test', Type__c='Price Adjustment');
		insert dmPriceAdjustment;	

		dmDrop.Order__c=order2.Id;
		update dmDrop;
		
	}
	TestMethod static void TestOrderUpdate()
	{
		
		Account acc = new Account(Name='Testing');
		insert acc;
		Contact c = new Contact(FirstName='test',LastName='test',AccountId=acc.Id, Email='zzTestzz@zzz.com',Phone='1222234567');
		insert c;
		Order__c order1 = new Order__c(Contract_Amount__c=5000, Contract_End_Date__c=Date.Today().addMonths(12).addDays(-1), Contract_Start_Date__c=Date.Today(), Subscription_End_Date__c=Date.Today().addMonths(12).addDays(-1), Subscription_Start_Date__c=Date.Today());
	
		insert order1;
		User u = [Select Id from User where IsActive=true Limit 1];
		order1.OwnerId= u.Id;
		order1.Site_Account__c=acc.Id;
		order1.System_Administrator__c = c.Id;
		Order1.Bill_To_Account__c=acc.Id;
		order1.Bill_To_Contact__c = c.Id;
		order1.Return_To_Contact__c = c.Id;
		Order1.Return_To_Account__c= acc.Id;
		order1.Bill_To_Address_1__c=order1.Site_Address_1__c='Address1';
		order1.Bill_To_Address_2__c=order1.Site_Address_2__c='Address2';
		order1.Bill_To_City__c=order1.Site_City__c='City';
		order1.Bill_To_State__c=order1.Site_State__c='State';
		order1.Bill_To_Postal_Code__c=order1.Site_Postal_Code__c='12456';
		order1.Bill_To_Country__c=order1.Site_Country__c='Country';
		order1.Bill_To_Contact_Email__c=order1.System_Administrator_Email__c='someone@somewhere.com';
		order1.Bill_To_Contact_Phone__c=order1.System_Administrator_Phone__c='1234567890';
		
		update order1;
		
	}
	
}