@isTest
private class TestTaskTrigger {
 
	static final string TESTACTIVITY_SUBJECT = 'Test Subject';
	static final string TESTCONTACT_LASTNAME = 'testington';
	static final string TESTCONTACT_LASTNAME2 ='Smith';

	static Contact testContact;
	static Contact testContact2;
	static Task testActivity;
	static Task testActivity2;
	static Task testActivity3;
	
	static void setup(){
		testContact = TestingUtils.createContact(TESTCONTACT_LASTNAME, TESTCONTACT_LASTNAME, null,  false);
		testContact2 = TestingUtils.createContact(TESTCONTACT_LASTNAME2, TESTCONTACT_LASTNAME2, null, false);
	  
		insert new List<Contact>{testContact, testContact2};
		testActivity = new Task(WhoId = testContact.ID);
		testActivity2 = new Task(WhoId = testContact2.ID);
		testActivity3 = new Task(WhoId = testContact2.ID);
	}
	
	//This tests to see if Most_recently_opened_created_activity__c and Most_recently_modified_Activity__c values are changed on Insert/ Update
	static testMethod void testCaseInsertTrigger() {
		
		setup();
	   
		Test.startTest();
		  Insert new List<Task>{testActivity, testActivity2};
		  testActivity2.Subject = TESTACTIVITY_SUBJECT;
		  Update testActivity2;
		Test.stopTest();
		List<contact> testContacts = [Select Id, Most_recently_created_activity__c, Most_recently_modified_activity__c
									  from Contact where ID IN : (new list<ID>{testContact.ID, testContact2.ID})];
	   
		System.AssertEquals(2,testContacts.size(), 'An incorrect number of Contacts exist');
		System.AssertEquals(System.Today(), testContacts[0].Most_recently_created_activity__c, 'TestContact1\'s Most Recently Opened/Created activity null');
		System.AssertEquals(System.Today(), testContacts[1].Most_recently_created_activity__c, 'TestContact2\'s Most Recently Opened/Created activity null');
		System.AssertEquals(System.Today(), testContacts[1].Most_recently_modified_activity__c, 'TestContact2\'s Most Recently Modified activity null');
	  
	}
	
	 //This tests to see if Most_recently_opened_created_activity__c and Most_recently_modified_Activity__c values are removed on Delete
	static testMethod void testCaseDeleteTrigger() {
		   
		setup();
		Insert new List<Task>{testActivity, testActivity2, testActivity3};
		testActivity2.Subject = TESTACTIVITY_SUBJECT;
		Update testActivity2;
		
		//Set the contact date values to yesterday to confirm that the times will change when records are deleted.
		Date dToday = System.Today();
		Date dYesterday = dToday.addDays(-1);
		testContact2.Most_recently_created_activity__c = dYesterday;
		testContact2.Most_recently_modified_activity__c = dYesterday;
		WithoutSharingServices.updateWithoutSharing ( new List<contact>{testContact2});
		
		List<contact> testContacts = [Select Id, Most_recently_created_activity__c, Most_recently_modified_activity__c
									  from Contact where ID IN : (new list<ID>{testContact.ID, testContact2.ID}) order by most_recently_created_Activity__c desc];
									
		System.Assert(testContacts[0].Most_recently_created_activity__c != null, 'TestContact1\'s Most Recently Opened/Created activity null');
		System.Assert(testContacts[1].Most_recently_created_activity__c != null, 'TestContact2\'s Most Recently Opened/Created activity null');
		
		Test.startTest();
			Delete new list<Task>{testActivity, testActivity2};
		Test.stopTest(); 
		
		testContacts = [Select Id, Most_recently_created_activity__c, Most_recently_modified_activity__c
						from Contact where ID IN : (new list<ID>{testContact.ID, testContact2.ID}) order by most_recently_created_Activity__c desc];
	   
		System.AssertEquals(2, testContacts.size(), 'An incorrect number of Contacts exist');
		System.AssertEquals(null, testContacts[0].Most_recently_created_activity__c, 'TestContact1\'s Most_recently_created_activity__c should be null');
		System.AssertEquals(null, testContacts[0].Most_recently_modified_activity__c, 'TestContact2\'s Most_recently_modified_activity__c should be null');
		
		//There are two activities assigned to testContacts[1], so deleting one shouldn't change this
		System.AssertEquals(System.Today(), testContacts[1].Most_recently_created_activity__c, 'TestContact2\'s Most_recently_created_activity__c should be today');
		System.AssertEquals(System.today(), testContacts[1].Most_recently_modified_activity__c, 'TestContact2\'s Most_recently_modified_activity__c should be today');
	}
	
}