@isTest
public with sharing class TestingUtils {
    
    private static Integer offsetForDummyInsert = 1;
	static Integer productCounter = 0;
	public static User ADMIN_USER
	{
		get
		{
			if( ADMIN_USER == null )
			{
				ADMIN_USER = [ SELECT Id, Name FROM User WHERE Profile.Name = 'System Administrator' AND IsActive = TRUE LIMIT 1 ];
			}
			return ADMIN_USER;
		}
		private set;
	}
 
	public static List<Contact> createContacts( Integer numContacts, Id recordTypeId, Boolean doInsert )
	{
		List<Contact> testContacts = new List<Contact>();
		for( Integer index = 0; index < numContacts; index++ )
		{
			Contact newContact = new Contact();
			newContact.LastName = 'TestContact' + index;
			testContacts.add( newContact );
		}
		if( doInsert )
		{
			insert testContacts;
		}
		return testContacts;
	}
	public static Contact createContact(String firstName, String lastName, Id accId,Boolean doInsert){
		Contact con = new Contact();
		con.firstName = firstName;
		con.lastName = lastName;
		con.AccountId = accId;
		con.Title = 'Title';
		if(doInsert){
			insert con;
		}
		return con;
	}
	
	public static User createUser(String firstName, String lastName, ID profileId, Boolean doInsert){
		User testUser = new User();
		datetime currentTime = System.now();
		testUser.ProfileId = profileId;
		testUser.alias = 'test' + currentTime.millisecond();
		testUser.email ='test' + currentTime.millisecond()+'@user.com';
		testUser.emailencodingkey ='UTF-8';
		testUser.lastname ='test';
		testUser.languagelocalekey ='en_US';
		testUser.localesidkey ='en_US'; 
		testUser.timezonesidkey ='America/New_York';
		testUser.username ='tester' + currentTime.millisecond() +  '@user.com';
		testUser.Site_Provision_Access__c = true;
		if(doInsert){
			insert testUser;
		}
		return testUser;
	}

	
	
	public static Account createAccount(String name,Boolean doInsert){
		Account acc = new Account();
		acc.Name = name;
		acc.PMD_Ship_To_Customer_ID__c = '1234';
		if(doInsert){
			insert acc;
		}
		return acc;
	}
	
	public static List<Account> createAccounts(Integer size, String name,Boolean doInsert){
        List<Account> accountsToReturn = new List<Account>();
        for(integer i = 0; i < size ; i++ ){
            Account acc = new Account(Name = name + i, PMD_Ship_To_Customer_ID__c= '1234' + i);
            accountsToReturn.add(acc);
        }
        if(doInsert){
            insert accountsToReturn;
        }
        
        return accountsToReturn;
    }
	
	
	public static Case createCase(string attachField, string parentID, Boolean doInsert){
		Case ca = new Case();
		ca.put(attachField, parentID);
		
		if(doInsert){
			insert ca;
		}
		
		return ca;
		
	}
	
	public static Opportunity createOpportunity(String name,Id accId,Boolean doInsert){
		Opportunity opp = new Opportunity();
		opp.AccountId = accId;
		opp.CloseDate = Date.TODAY().addMonths(1);
		opp.StageName = 'Active';
		opp.Name = name;
		if(doInsert){
			insert opp;
		}
		return opp;
	}

   public static OS_User_ID__c createOSUserID(string name, ID conID, Boolean doInsert){
   	  OS_User_ID__c userID = new OS_User_ID__c();
   	  userID.contact__c = conID;
   	  
   	  userID.name = name;
   	  if(doInsert){
   	  	insert userID;
   	  }
   	  return userID;
   }
   

	public static Product2 buildProduct()
	{
		return buildProducts( 1 )[0];
	}

	public static Product2 createProduct()
	{
		Product2 testProduct = buildProduct();
		insert testProduct;
		return testProduct;
	}
	
	public static List<Product2> buildProducts( Integer numOfProducts )
	{
		List<Product2> testProducts = new List<Product2>();
		for( Integer i = 0; i < numOfProducts; i++ )
		{
			testProducts.add (new Product2( Name = 'Test Product' + productCounter,
											 isActive = true,
											 CurrencyIsoCode = 'USD' ) );
			productCounter++;
		}
		return testProducts;
	}

	public static List<Product2> createProducts( Integer numOfProducts )
	{
		List<Product2> testProducts = buildProducts( numOfProducts );
		insert testProducts;
		return testProducts;
	}
	
	public static List<SFDC_520_QuoteLine__c> buildOrderScheduleLineItems( Set<Id> productIds, Id orderScheduleId )
	{
		List<SFDC_520_QuoteLine__c> orderScheduleLineItems = new List<SFDC_520_QuoteLine__c>();
		Integer userCounter = 1;
		for( Id productId : productIds )
		{
			orderScheduleLineItems.add( new SFDC_520_QuoteLine__c( Product2__c = productId,
																	Quote__c = orderScheduleId,
																	Users__c = userCounter,
																	Product_Code__c = 'Code ' + userCounter) );
		}
		return orderScheduleLineItems;
	}
	
	public static List<SFDC_520_QuoteLine__c> createOrderScheduleLineItems( Set<Id> productIds, Id orderScheduleId )
	{
		List<SFDC_520_QuoteLine__c> orderScheduleLineItems = buildOrderScheduleLineItems( productIds, orderScheduleId );
		insert orderScheduleLineItems;
		return orderScheduleLineItems;
	}
	

	public static List<SFDC_520_Quote__c> buildOrderSchedules( Integer numOfOrderSchedules )
	{
		List<SFDC_520_Quote__c> orderSchedules = new List<SFDC_520_Quote__c>();
		for( Integer i = 0; i < numOfOrderSchedules; i++ )
		{
			orderSchedules.add( new SFDC_520_Quote__c() );
		}
		return orderSchedules;
	}
	
	public static List<SFDC_520_Quote__c> createOrderSchedules( Integer numOfOrderSchedules )
	{
		List<SFDC_520_Quote__c> orderSchedules = buildOrderSchedules( numOfOrderSchedules );
		insert orderSchedules;
		return orderSchedules;
	}
	
	/* Mark Robinson - mrobinson@IoniaSolutions.com */
	public static Site__c createSite(Account account) {
		Site__c site = new Site__c(Account__c = account.Id, Name='TheSite', Site_ID__c='TheSite', CUSTNUM__c='1234');
		insert site;
		return site;
	}
	
	public static Business_Contact_Email__c createBusinessContactEmail(Site__c site) {
		Business_Contact_Email__c bce = new Business_Contact_Email__c(Site__c=site.Id, Site_ID__c='TheSite');
		insert bce;
		return bce;
	}
	
	public static Cortera_Report_Credit__c createCorteraReportCredit(Site__c site) {
		Cortera_Report_Credit__c crc = new Cortera_Report_Credit__c(Site__c=site.Id, Site_ID__c='TheSite');
		insert crc;
		return crc;
	}
	
	public static CRM_Credit__c createCRMCredit(Site__c site) {
		CRM_Credit__c cc = new CRM_Credit__c(Site__c=site.Id, Site_ID__c='TheSite', Key__c = 'TheSite_1234');
		insert cc;
		return cc;
	}
	
	public static Email_Pool__c createEmailPool(Site__c site) {
		Email_Pool__c ep = new Email_Pool__c(Site__c=site.Id, Site_ID__c='TheSite');
		insert ep;
		return ep;
	}
	
	public static Site_Domain__c createSiteDomain(Site__c site) {
		Site_Domain__c sd = new Site_Domain__c(Site__c=site.Id, Site_ID__c='TheSite', Domain__c='10.10.10.10');
		insert sd;
		return sd;
	}
	
	public static Site_Priv__c createSitePriv(Site__c site) {
		Site_Priv__c sp = new Site_Priv__c(Site__c=site.Id, Site_ID__c='TheSite', Allowed_Users__c=1, Allocated_Users__c=1);
		insert sp;
		return sp;
	}
	
	public static List<Order__c> createOrdersCustom (Integer size, String name, boolean doinsert){
	    List<Order__c> ordersToReturn = new List<Order__c>();
	    for(integer i = 0 ; i < size ; i++){
	        ordersToReturn.add(new Order__c( name = name + i, Subscription_Start_Date__c =Date.today(), 
	                                         Subscription_End_Date__c =Date.today(),
	                                         Contract_Amount__c = 0
	                                        ));
	    }
	
	    if( doinsert){
	        insert ordersToReturn;
	    }
	    
	    return ordersToReturn;
	}
	
	public static List<Credit_Memo__c> createCreditMemos (List<Order__c> orders, boolean doinsert){
        List<Credit_Memo__c> memosToReturn = new List<Credit_Memo__c>();
        for(Order__c ord : orders){
            memosToReturn.add(new Credit_Memo__c(order__c = ord.id, reason_comments__c = 'Testing out memos', New_Contract_End_Date__c = Date.today()));
        }
    
        if( doinsert){
            insert memosToReturn;
        }
        
        return memosToReturn;
    }
    
	public static void dummyInsert(List<SObject> objects)
    {
           String Id_Prefix = objects[0].getSObjectType().getDescribe().getKeyPrefix();
           for( Integer i = 0; i < objects.size(); i++ )
           {
                   String idSuffix = String.valueOf( offsetForDummyInsert ).leftPad(12);
                   idSuffix = idSuffix.replace(' ', '0');
                   
                   objects[i].id= Id_Prefix + idSuffix;
                   offsetForDummyInsert+=1;
           }
   }

   public static List<PricebookEntry> generatePricebookEntries(Map<Id, Decimal> productPrices, Id pricebookId, Boolean doInsert) {
        List<PricebookEntry> entries = new List<PricebookEntry>();
        for (Id productId : productPrices.keySet()) {
            entries.add(new PricebookEntry(
                Product2Id = productId,
                Pricebook2Id = pricebookId,
                UnitPrice = productPrices.get(productId),
                IsActive = true
            ));
        }

        if (doInsert) {
            insert entries;
        }
        return entries;
    }

    public static List<Product2> generateProducts(Integer numProducts, Boolean doInsert) {
        List<Product2> products = new List<Product2>();
        for (Integer i = 0; i < numProducts; i++) {
            products.add(new Product2(name='Test' + i,Code_External_ID__c=Decimal.valueof(i)));
        }

        if (doInsert) {
            insert products;
        }
        return products;
    }

    public static Map<Id, Decimal> generateProductPrices(List<Product2> products, Decimal baseAmount) {
        Map<Id, Decimal> productPrices = new Map<Id, Decimal>();
        for (Integer i = 0; i < products.size(); i++) {
            productPrices.put(products.get(0).Id, baseAmount + i);
        }
        return productPrices;
    }

    public static List<Pricebook2> generatePricebooks(Integer numPricebooks, Boolean doInsert) {
        List<Pricebook2> pricebooks = new List<Pricebook2>();
        for (Integer i = 0; i < numPricebooks; i++) {
            pricebooks.add(new Pricebook2(
                IsActive = true,
                Name = 'Unit Test'
            ));
        }

        if (doInsert) {
            insert pricebooks;
        }
        return pricebooks;
    }
	
}