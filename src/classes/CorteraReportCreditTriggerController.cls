/******************************************************************************* 
Name              : CorteraReportCreditTriggerController
Revision History  : TBD
					 
Created/Modified by   Created/Modified Date     Requested by          Related Task/Issue             
----------------------------------------------------------------------------------------
1. Mark Robinson      02/12/2014         	    Jim Horty             
*******************************************************************************/
public class CorteraReportCreditTriggerController {

	public static void linkCorteraReportCreditToSite (List<Cortera_Report_Credit__c> corteraReportCredits) {
		List<String> siteIds = new List<String>();
		Map<String, String> siteIdBySiteIDString = new Map<String, String>();
		
		for (Cortera_Report_Credit__c corteraReportCredit : corteraReportCredits) {
			 if (corteraReportCredit.Site_ID__c != null) siteIds.add(corteraReportCredit.Site_ID__c);
		}
		
		for (Site__c site : [SELECT Id, Site_ID__c From Site__c WHERE Site_ID__c IN :siteIds]) {
			siteIdBySiteIDString.put(site.Site_ID__c, site.Id);
		}
		
		// SET PARENT SITE
		for (Cortera_Report_Credit__c corteraReportCredit : corteraReportCredits) {
			if (corteraReportCredit.Site__c == null) corteraReportCredit.Site__c = siteIdBySiteIDString.get(corteraReportCredit.Site_ID__c);
			
		}				
	}
	
	public static testMethod void linkCorteraReportCreditToSite_Test() {
		Account account = TestingUtils.createAccount('TestAccount', true);
		Site__c site = TestingUtils.createSite(account);
		Business_Contact_Email__c bce = TestingUtils.createBusinessContactEmail(site);
		Cortera_Report_Credit__c crc = TestingUtils.createCorteraReportCredit(site);
	}
	
}