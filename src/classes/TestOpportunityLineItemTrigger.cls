@isTest
private class TestOpportunityLineItemTrigger {
    
    @isTest static void opportunityLineItemScheduleTests_ut1() {

        Account acc = new Account(Name='Test Account');
        insert acc;

        Opportunity opp = new Opportunity();
        opp.Name = 'Test Opportunity ';
        opp.StageName = '10% - Prospect';
        opp.CloseDate = System.today();
        opp.AccountId = acc.Id;
        insert opp;

        List<Product2> products = TestingUtils.generateProducts(10, false);
        
        for (Product2 p : products) {
            p.Family = 'Subscription';
            p.Subscription_Term__c = '3 years';
            /** IF AND WHEN REVENUE SCHEDULES GET REINSTATED, REINSTATE THIS CODE
            p.CanUseRevenueSchedule = true;
            **/
        }

        insert products;
        
    // Generate standard prices (note Test.getStandardPricebookId())
        Map<Id, Decimal> standardProductPrices = TestingUtils.generateProductPrices(products, 5000);
        List<PricebookEntry> standardEntries = TestingUtils.generatePricebookEntries(standardProductPrices, Test.getStandardPricebookId(), true);   

        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;

        Map<Id, Decimal> customProductPrices = TestingUtils.generateProductPrices(products, 15000);
        List<PricebookEntry> customEntries = TestingUtils.generatePricebookEntries(customProductPrices, customPB.Id, true);   


        Test.startTest();

            OpportunityLineItem oppLineItem = new OpportunityLineItem();
            oppLineItem.Quantity = 1;
            oppLineItem.UnitPrice = 5000;
            oppLineItem.OpportunityId = opp.Id;
            oppLineItem.PricebookEntryId = customEntries[0].Id;
            insert oppLineItem;     

        Test.stopTest();
        
        /** IF AND WHEN REVENUE SCHEDULES GET REINSTATED, REINSTATE THIS CODE
        List<OpportunityLineItemSchedule> schedules = [Select Id, ScheduleDate, Revenue from OpportunityLineItemSchedule];
        System.debug('schedules ' + schedules);
        System.assertNotEquals(null,schedules);
        **/
    }
    
    // New Test method added to validate that Schedules are created when the flag on OpportunityLineItem is set.
    @isTest static void opportunityLineItemScheduleTests_ut2() {

        Account acc = new Account(Name='Test Account');
        insert acc;

        Opportunity opp = new Opportunity();
        opp.Name = 'Test Opportunity ';
        opp.StageName = '10% - Prospect';
        opp.CloseDate = System.today();
        opp.AccountId = acc.Id;
        insert opp;

        List<Product2> products = TestingUtils.generateProducts(10, false);
        
        for (Product2 p : products) {
            p.Family = 'Subscription';
            p.Subscription_Term__c = '3 years';
            /** IF AND WHEN REVENUE SCHEDULES GET REINSTATED, REINSTATE THIS CODE
            p.CanUseRevenueSchedule = true;
            **/
        }

        insert products;
        
    // Generate standard prices (note Test.getStandardPricebookId())
        Map<Id, Decimal> standardProductPrices = TestingUtils.generateProductPrices(products, 5000);
        List<PricebookEntry> standardEntries = TestingUtils.generatePricebookEntries(standardProductPrices, Test.getStandardPricebookId(), true);   

        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;

        Map<Id, Decimal> customProductPrices = TestingUtils.generateProductPrices(products, 15000);
        List<PricebookEntry> customEntries = TestingUtils.generatePricebookEntries(customProductPrices, customPB.Id, true);   


        Test.startTest();

            OpportunityLineItem oppLineItem = new OpportunityLineItem();
            oppLineItem.Quantity = 1;
            oppLineItem.UnitPrice = 5000;
            oppLineItem.OpportunityId = opp.Id;
            oppLineItem.PricebookEntryId = customEntries[0].Id;
            /** IF AND WHEN REVENUE SCHEDULES GET REINSTATED, REINSTATE THIS CODE
        	oppLineItem.Calculate_Revenue_Schedules__c = false;
            **/
            insert oppLineItem;     

            /**
            List<OpportunityLineItemSchedule> schedules = [Select Id, ScheduleDate, Revenue from OpportunityLineItemSchedule];
            System.debug('schedules ' + schedules);
            System.assertNotEquals(null,schedules);
            
            delete schedules; // deletes the schedules
        	// Now update the OpportunityLineItem with the flag calculate revenue schedules to true. This will create new schedules
        	
        	oppLineItem.Calculate_Revenue_Schedules__c = true;
        	update oppLineItem;
			        	
        	schedules = [Select Id, ScheduleDate, Revenue from OpportunityLineItemSchedule];
            System.debug('schedules ' + schedules);
            System.assertNotEquals(null,schedules);
            **/
            
        Test.stopTest();
        
    }

}