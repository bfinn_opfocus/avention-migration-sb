trigger AddCompetitors2 on Opportunity (before update) {

//this code will get all opportunities where the primary competitor or the other competitor fields have been changed
//for each of these records it will delete all of the records in the 



// The map allows us to keep track of the opptysthat have
// new comps

    Map<Id, Opportunity> OpptysWithNewComps = new Map<Id, Opportunity>();

    if(Trigger.old != null)
    {
    for (Integer i = 0; i < Trigger.new.size(); i++) {
        //if ( (Trigger.old[i].Primary_Competitor__c != Trigger.new[i].Primary_Competitor__c)
        //|| (Trigger.old[i].Competitors__c != Trigger.new[i].Competitors__c)) 
        //{

            OpptysWithNewComps.put(Trigger.old[i].id, Trigger.new[i]);
        //}
    }
    }
    
    OpportunityCompetitor[] opptycomps = [select id, opportunityid from OpportunityCompetitor where opportunityid in :OpptysWithNewComps.keySet()];
    try {
            delete opptycomps;
        } 
    catch (System.DmlException e) 
        {
            // Process exception here
        }

        
        

}