trigger BargainToAdvance on Bargain_to_Advance_Scorecard__c (after insert, after update) {
for (Bargain_to_Advance_Scorecard__c b : Trigger.new) {

     if(b.Score__c != null)
	{     
	     opportunity[] opp  = [select id from opportunity where id= :b.Opportunity__c];
     		opp[0].Bargain_To_Advance_Score__c = b.Score__c;
		

     	update opp;
   }


}
}