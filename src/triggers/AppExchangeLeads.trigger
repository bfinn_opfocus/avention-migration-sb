trigger AppExchangeLeads on Lead (before insert, before update) {
	for (Lead l : Trigger.new) {
		if(l.LeadSource != null)
		{
			if(l.LeadSource.contains ('Account Intelligence') || l.LeadSource.contains ('OneSource for Salesforce'))
			{
				l.Lead_Type__c = 'Inquiry';
			}
			
		}
	}
}