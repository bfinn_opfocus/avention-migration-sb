/*
** Trigger:  SiteRequest
** SObject:  Site_Request__c
** Created by OpFocus on May 2015
** Description: Trigger for Site object. Currently, these are the triggers defined.
**     +Converts a Site Request into an Site when it's status becomes approved 
*/
trigger SiteRequest on Site_Request__c (before update) {
    if(Trigger.isBefore && Trigger.isUpdate){
        List<Site_Request__c> lstRequestsToConvert = new List<Site_Request__c>();
        for(Site_Request__c sr : trigger.new){
            //If the Status had just become Approved
            if(sr.Status__c == 'Approved' && Trigger.oldMap.get(sr.id).Status__c != 'Approved'){
                lstRequestsToConvert.add(sr);
            }
        }
        if(!lstRequestsToConvert.isEmpty()){
        	SiteHelper.ConvertToSite(lstRequestsToConvert);
        }
    }	
}