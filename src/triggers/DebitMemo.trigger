trigger DebitMemo on Debit_Memo__c (before insert, before update,after insert , after update){
	
	if(Trigger.isBefore)
	{
			Set<Id> setAllCreditMemoId = new Set<Id>();
			set<Id> setAllOrderId  = new set<Id>();
			
			List<Debit_Memo__c> lstDebitMemoWithOutOrderId = new List<Debit_Memo__c>();
			List<Debit_Memo__c> lstDebitMemoWithOutContractEndDate = new List<Debit_Memo__c>();
			 
			for(Debit_Memo__c newMemo:System.Trigger.new)
			{
				setAllCreditMemoId.Add(newMemo.Credit_Memo__c);
				system.debug(logginglevel.INFO ,'***newDebitMemo***');
				system.debug(logginglevel.INFO ,newMemo);
				
				system.debug(logginglevel.INFO ,'***newDebitMemo.Credit_Memo__r***');
				system.debug(logginglevel.INFO ,newMemo.Credit_Memo__r);
				if(newMemo.New_Contract_End_Date__c==null)
				{
					lstDebitMemoWithOutContractEndDate.Add(newMemo);
					
				}
				
				if(newMemo.Order__c==null)
				{	lstDebitMemoWithOutOrderId.Add(newMemo);
				}
			}	
			Map<Id,Credit_Memo__c> mapIdCreditMemo = new Map<Id,Credit_Memo__c>([Select Id,Order__c,New_Contract_End_Date__c,CurrencyIsoCode from Credit_Memo__c where Id in:setAllCreditMemoId]);
			
			for(Debit_Memo__c newMemo:lstDebitMemoWithOutOrderId)
			{
				Credit_Memo__c creditMemo = mapIdCreditMemo.get(newMemo.Credit_Memo__c);
				newMemo.Order__c = creditMemo.Order__c;
				
				
			}
			for(Debit_Memo__c newMemo:lstDebitMemoWithOutContractEndDate)
			{
				Credit_Memo__c creditMemo = mapIdCreditMemo.get(newMemo.Credit_Memo__c);
				newMemo.New_Contract_End_Date__c = creditMemo.New_Contract_End_Date__c;
				
			}
			for(Debit_Memo__c newMemo:System.Trigger.new)
			{
				setAllOrderId.Add(newMemo.Order__c);
				Credit_Memo__c creditMemo = mapIdCreditMemo.get(newMemo.Credit_Memo__c);
				newMemo.CurrencyIsoCode = creditMemo.CurrencyIsoCode;
			}
			
			if(setAllOrderId.size()>0)
			{
			 
				Map<Id,Order__c> mapIdOrders = new Map<Id,Order__c>([Select Id,  Order_Number__c from Order__c where Id in :setAllOrderId]);
				for(Debit_Memo__c newMemo:System.Trigger.new)
				{
					Order__c order = mapIdOrders.get(newMemo.Order__c);
					newMemo.Order_Number__c = order.Order_Number__c;
				}
			}			
	}	

	if(Trigger.isAfter)
	{
		
		
		set<Id> setMemosWithOrderChange = new set<Id>();
		Map<Id,Id> mapMemoOrderId  = new Map<Id,Id>();
	
		Map<Id,Set<Id>> mapCreditDebitMemoSet = new Map<Id,Set<Id>>();
		
		Map<Id,Id> mapDebitCreditId = new Map<Id,Id>();
		
		Set<Id> setCreditMemo = new Set<Id>();
		
		if(Trigger.isInsert)
		{
			for(Debit_Memo__c newMemo:System.Trigger.new)
			{
				mapDebitCreditId.put(newMemo.Id,newMemo.Credit_Memo__c);
				set<Id> setMemoId = mapCreditDebitMemoSet.get(newMemo.Credit_Memo__c);
				if(setMemoId==null)
					setMemoId = new Set<Id>();
				setMemoId.Add(newMemo.Id);
				mapCreditDebitMemoSet.put(newMemo.Credit_Memo__c,setMemoId);
			}	
		}
		if(Trigger.isUpdate)
		{
			for(Debit_Memo__c newMemo:System.Trigger.new)
			{
				Debit_Memo__c oldMemo = System.Trigger.oldMap.get(newMemo.Id);
				if(newMemo.Order__c!=oldMemo.Order__c)
				{
					mapDebitCreditId.put(newMemo.Id,newMemo.Credit_Memo__c);
					set<Id> setMemoId = mapCreditDebitMemoSet.get(newMemo.Credit_Memo__c);
					if(setMemoId==null)
						setMemoId = new Set<Id>();
					setMemoId.Add(newMemo.Id);
					mapCreditDebitMemoSet.put(newMemo.Credit_Memo__c,setMemoId);
				}
				
			}
		}
		//Delete the old ones which are not needed
		if(mapDebitCreditId.KeySet().size()>0)
		{
			List<Debit_Memo_Line_Item__c> lstDebitMemoLineToDelete = new List<Debit_Memo_Line_Item__c>([select Id from Debit_Memo_Line_Item__c where Debit_Memo__c in :mapDebitCreditId.KeySet()]);
			if(lstDebitMemoLineToDelete.size()>0)
				delete lstDebitMemoLineToDelete;
		}
		
		//Create new Debit Memo Lines from Credit Memo
		if(mapDebitCreditId.Values().size()>0)
		{
			set<Id> setCMToCreateLines = new set<Id>();
			setCMToCreateLines.addAll(mapDebitCreditId.Values());
			set<Id> setCMWithLines = new set<Id>();
			 
			List<Credit_Memo_Line_Item__c> lstCreditMemoLine = new List<Credit_Memo_Line_Item__c>([Select  Contract_Line_Number__c, Product__c,Credit_Memo__c,Adjustment_Amount__c,CurrencyIsoCode from Credit_Memo_Line_Item__c where Credit_Memo__c in :setCMToCreateLines]);
			List<Debit_Memo_Line_Item__c> lstDebitMemoLineToInsert = new List<Debit_Memo_Line_Item__c>();
			//Same order can be present in multiple memos. so need to create memo line for each memo.
			for (Credit_Memo_Line_Item__c creditLine : lstCreditMemoLine) 
			{
				setCMWithLines.Add(creditLine.Credit_Memo__c);
				for(Id memoId:mapCreditDebitMemoSet.get(creditLine.Credit_Memo__c))
				{
					Debit_Memo_Line_Item__c newLine = new Debit_Memo_Line_Item__c();
				//newLine.Adjustment_Amount__c;
				 
				 newLine.Contract_Line_Number__c = creditLine.Contract_Line_Number__c;
				 newLine.Debit_Memo__c = memoId;
				 newLine.Product__c = creditLine.Product__c;
				 newLine.CurrencyIsoCode=creditLine.CurrencyIsoCode;
				
					lstDebitMemoLineToInsert.add(newLine);
				}
			}
			//Get the list of CMs to get line items from order.
			
			setCMToCreateLines.removeAll(setCMWithLines);
			
			if(setCMToCreateLines.size()>0)
			{
				Map<Id,Id> mapCreditIdOrderId = new Map<Id,Id>();
				List<Credit_Memo__c> lstCreditMemo = new List<Credit_Memo__c>([Select Id,Order__c from Credit_Memo__c where Id in :setCMToCreateLines]);
				
				Map<Id,Set<Id>> mapOrderMemoSet = new Map<Id,Set<Id>>();				
				for(Credit_Memo__c creditMemo:lstCreditMemo)
				{
					mapCreditIdOrderId.put(creditMemo.Id,creditMemo.Order__c);
					
					set<Id> setMemoId = mapOrderMemoSet.get(creditMemo.Order__c);
					 if(setMemoId==null)
					 	setMemoId = new Set<Id>();
					 setMemoId.Add(creditMemo.Id);
					 mapOrderMemoSet.put(creditMemo.Order__c,setMemoId);
				}
				
				set<Id> setOrderIdToCreateLines = new set<Id>();
				
				setOrderIdToCreateLines.addAll(mapCreditIdOrderId.Values());
				
				List<Order_Product__c> lstOrderProducts = new List<Order_Product__c>([Select Contract_Line_Amount__c, Contract_Line_Number__c, Product__c, Sales_Price__c, Users__c,Order__c,CurrencyIsoCode from Order_Product__c where Order__c in :setOrderIdToCreateLines]);
				
				//Same order can be present in multiple memos. so need to create memo line for each memo.
				for (Order_Product__c ordprod : lstOrderProducts) 
				{
					
					for(Id creditMemoId:mapOrderMemoSet.get(ordprod.Order__c))
					{
						
						for(Id debitMemoId:mapCreditDebitMemoSet.get(creditMemoId))
						{
						
							Debit_Memo_Line_Item__c newLine = new Debit_Memo_Line_Item__c();
					//newLine.Adjustment_Amount__c;
							 newLine.Contract_Line_Number__c = ordprod.Contract_Line_Number__c;
					 		 newLine.Debit_Memo__c = debitMemoId;
					 		 newLine.Product__c = ordprod.Product__c;
					 		 newLine.CurrencyIsoCode=ordprod.CurrencyIsoCode;
							lstDebitMemoLineToInsert.add(newLine);
						}
					}
				}
			}
			if(lstDebitMemoLineToInsert.size()>0)
			 insert lstDebitMemoLineToInsert;
		}
	}

}