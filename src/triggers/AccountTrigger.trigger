/*
** Trigger:  AccountTrigger
** SObject:  Account
** Created by Unknown
** Update by OpFocus on June 18, 2015
** Description: Trigger for Account object. Currently, these are the triggers defined:
**
** Credit-hold processing : Pre-dates Opfocus
**
** Populate PMD_Ship_To_Customer_ID__c with the value of PMD_Ship_To_Customer_ID_Auto_number__c if needed      
** PMD_Ship_To_Customer_ID__c is needed for integration, but was not originally implimented as an auto-number  
** This allows us to pupulate this value for all new Accounts going forward             
*/
trigger AccountTrigger on Account (after update, after insert, before update) {

	if (Trigger.isAfter && Trigger.isUpdate) 
 	{
 		////////////////////////
 		//Credit-hold processing
 		////////////////////////
 		Set<Id> setCreditHoldYesAccounts  = new Set<Id>();
 		Set<Id> setCreditHoldNoAccounts  = new Set<Id>();
 		
 		List<string> lstCreditHoldTypes = new List<Id>();
 		for (Account a : System.Trigger.new) 
		{
			//Calculate the subscription end date from the contract length
				Account oldAccount= System.Trigger.oldMap.get(a.Id);
				if(oldAccount!=null)
				{
					if(a.Admin_Credit_Hold_Type__c!=oldAccount.Admin_Credit_Hold_Type__c)
					{
						 if(a.Admin_Credit_Hold_Type__c!=null &&a.Admin_Credit_Hold_Type__c!='' )
						 {
						 	setCreditHoldYesAccounts.Add(a.Id);
						 }else
						 {
						 	setCreditHoldNoAccounts.Add(a.Id);
						 }
					}	
				}
		}
		//Now update the orders
		List<Order__c> lstOrdersToUpdate = new List<Order__c>();
		if(setCreditHoldYesAccounts.size()>0)
		{
			for(Order__c[] lstOrders:[Select Id,Admin_Credit_Hold__c from Order__c where (Site_Account__c in :setCreditHoldYesAccounts or Bill_To_Account__c in :setCreditHoldYesAccounts) and Think_Id__c=null])
			{
				for(Order__c order:lstOrders)
				{
					order.Admin_Credit_Hold__c='Yes';
					lstOrdersToUpdate.Add(order);
				}
			}
		}
		
		if(setCreditHoldNoAccounts.size()>0)
		{
			for(Order__c[] lstOrders:[Select Id,Admin_Credit_Hold__c from Order__c where (Site_Account__c in :setCreditHoldYesAccounts or Bill_To_Account__c in :setCreditHoldYesAccounts) and Think_Id__c=null])
			{
				for(Order__c order:lstOrders)
				{
					order.Admin_Credit_Hold__c='No';
					lstOrdersToUpdate.Add(order);
				}
			}
		}
		if(lstOrdersToUpdate.size()>0)
		{
			update lstOrdersToUpdate;
		}
 	}

 	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 	// Opfocus - June 18, 2015                                                                                     //
 	// Populate PMD_Ship_To_Customer_ID__c with the value of PMD_Ship_To_Customer_ID_Auto_number__c if needed      //
 	// PMD_Ship_To_Customer_ID__c is needed for integration, but was not originally implimented as an auto-number  //
 	// This allows us to pupulate this value for all new Accounts going forward                                    //
 	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 	
 	// During creation we the auto-number isn't generated until the after trigger.  So we flag it to update itself to populate
 	if(trigger.isAfter && trigger.isInsert){
 		Map<Id, Account> mapAccountsToFlagForPmdAccountNumberUpdate = new Map<Id, Account>();
 		for(Account a : Trigger.new){
 			if(a.PMD_Ship_To_Customer_ID__c == null && a.PMD_Ship_To_Customer_ID_Auto_number__c != null){
 				mapAccountsToFlagForPmdAccountNumberUpdate.put(a.Id, new Account(Id=a.Id, Do_Update_PMD_Customer_Id__c=true));
 			}
 		}
 		if(!mapAccountsToFlagForPmdAccountNumberUpdate.values().isEmpty()){
 			Update mapAccountsToFlagForPmdAccountNumberUpdate.values();
 		}
 	}

 	// If we flagged it for update, let's try to copy the Auto-Gen field to the PMD Id field
 	if(Trigger.isBefore && Trigger.isUpdate){
 		for(Account a : Trigger.new){
	 		if(a.Do_Update_PMD_Customer_Id__c && a.PMD_Ship_To_Customer_ID__c == null && a.PMD_Ship_To_Customer_ID_Auto_number__c != null){
				a.PMD_Ship_To_Customer_ID__c = a.PMD_Ship_To_Customer_ID_Auto_number__c;
				a.Do_Update_PMD_Customer_Id__c = false;
	 		}
	 	}
 	}


}