/******************************************************************************* 
Name              : SiteSetParent
Revision History  : TBD
					 
Created/Modified by   Created/Modified Date     Requested by          Related Task/Issue             
----------------------------------------------------------------------------------------
1. Mark Robinson      02/12/2014         	    Jim Horty             
*******************************************************************************/
trigger SiteSetParent on Site__c (before insert) {
	
	SiteTriggerController.linkSiteToAccount(trigger.new);

}