/*
** Trigger:  SiteRequestBundle
** SObject:  Site_Request_Bundle__c
** Created by OpFocus on May 2015
** Description: Trigger for SiteRequest object. Currently, these are the triggers defined.
**     +When Do_Update_IsTrial_Checkbox__c is checked we want to re-evaluate the it's IsTrial status based on it's line-items
*/
trigger SiteRequestBundle on Site_Request_Bundle__c (Before Update) {
    if(trigger.isBefore && trigger.isUpdate){
    	Map<Id, Site_Request_Bundle__c> mapBundlesToUpdateTrialStatus = new Map<Id, Site_Request_Bundle__c>();

    	//Find bundles that need to be updated
    	for(Site_Request_Bundle__c bundle : trigger.new){
    		if(bundle.Do_Update_IsTrial_Checkbox__c){
    			mapBundlesToUpdateTrialStatus.put(bundle.id, bundle);
    		}
    	}

    	//Do actual updates
    	SiteRequestBundleHelper.UpdateIsTrialStatus(mapBundlesToUpdateTrialStatus.values());
    }
}