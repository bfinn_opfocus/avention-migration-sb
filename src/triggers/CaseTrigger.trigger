trigger CaseTrigger on Case (after delete, after insert, after update, before insert) {
	List<ID> contactIds = new List<ID>();
	
	if(Trigger.isDelete){
		for (Case c : Trigger.Old) {
			if((c.ContactId != null))  
				contactIds.add(c.ContactID);
		}
	} else{
		for (Case c : Trigger.New) {
			if((c.ContactId != null))  
				contactIds.add(c.ContactID);
		}
	}

	if(Trigger.isBefore && Trigger.isInsert){
		//CaseServices.populateCasesWithContact(CaseServices.filterCasesToPopulateContact(Trigger.New));
	}	

	if(Trigger.isAfter && Trigger.isInsert){		
	//	CaseServices.adjustRecentlyOpened(contactIds);
	}
	
	if(Trigger.isAfter && Trigger.isUpdate){
	//  CaseServices.adjustRecentlyModified(contactIds);
	}
	
	if(Trigger.isAfter && Trigger.isDelete){
	//  CaseServices.adjustRecentlyModified(contactIds);
	//CaseServices.adjustRecentlyOpened(contactIds);
	}

	
}