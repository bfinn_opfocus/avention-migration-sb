/*
** Trigger:  SiteRequestBundleLineItem
** SObject:  Site_Request_Bundle_Line_Item__c
** Created by OpFocus on May 2015
** Description: Trigger for Site_Request_Bundle_Line_Item__c object. Currently, these are the triggers defined.
**     +When a Line item is created, it's isTrial checkbox is changed, or it's parent is change it flag's it's partent to update it's Trial checkbox
*/
trigger SiteRequestBundleLineItem on Site_Request_Bundle_Line_Item__c (After Insert, After Update) {
	//+When a Line item is created, it's isTrial checkbox is changed, or it's parent is change it flag's it's partent to update it's Trial checkbox
	if(trigger.isAfter && (trigger.isInsert || trigger.isUpdate)){
		Map<Id, Site_Request_Bundle__c> mapBundlesToUpdate = new Map<Id, Site_Request_Bundle__c>();
		for(Site_Request_Bundle_Line_Item__c lineItem : trigger.new){
			
			//for any insert we will update the parent if the line item is a trial (don't need to update parent for non-trial, becuase it won't change anything anyhow)
			if(trigger.isInsert){
				if(lineItem.Is_Trial__c){
					Site_Request_Bundle__c partent = new Site_Request_Bundle__c(Id=lineItem.Site_Request_Bundle__c);
					partent.Do_Update_IsTrial_Checkbox__c = true;
					if(!mapBundlesToUpdate.containsKey(partent.Id)){
						mapBundlesToUpdate.put(partent.Id, partent);
					}
				}
			}
			//for any updates, we want to update the parent if the line item's isTrial state has change or its got a new parent
			if(trigger.isUpdate){
				Boolean isTrialChange = lineItem.Is_Trial__c != trigger.oldMap.get(lineItem.id).Is_Trial__c;
				Boolean parentChanged = lineItem.Site_Request_Bundle__c != trigger.oldMap.get(lineItem.id).Site_Request_Bundle__c;
				if(isTrialChange || parentChanged){
					Site_Request_Bundle__c newPartent = new Site_Request_Bundle__c(Id=lineItem.Site_Request_Bundle__c);
					newPartent.Do_Update_IsTrial_Checkbox__c = true;
					if(!mapBundlesToUpdate.containsKey(newPartent.Id)){
						mapBundlesToUpdate.put(newPartent.Id, newPartent);
					}
					//if the parent changed we also need to update it's old parent, since it may no longer have any trials
					Site_Request_Bundle__c oldPartent = new Site_Request_Bundle__c(Id=trigger.oldMap.get(lineItem.id).Site_Request_Bundle__c);
					oldPartent.Do_Update_IsTrial_Checkbox__c = true;
					if(!mapBundlesToUpdate.containsKey(oldPartent.Id)){
						mapBundlesToUpdate.put(oldPartent.Id, oldPartent);
					}				
				}
			}
			update mapBundlesToUpdate.values();
		}
	}
}