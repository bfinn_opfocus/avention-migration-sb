trigger AddCompetitors on Opportunity (after insert, after update) {

//this code will get all opportunities where the primary competitor or the other competitor fields have been changed
//for each of these records it will first delete all of the competitor data in the related list
//and then it will add the values in these two fields to the ompetitor's related list



    List<OpportunityCompetitor> opptycomp = new List<OpportunityCompetitor>();

    
    for (Opportunity newOppty: Trigger.New) {
        if (newOppty.Primary_Competitor__c != null || newOppty.Competitors__c != null ) {
        
        
            opptycomp.add(new OpportunityCompetitor(CompetitorName = newOppty.Primary_Competitor__c, OpportunityId = newOppty.Id));
    
            if(newOppty.Competitors__c != null)
            {
            
                String[] stringOC = newOppty.Competitors__c.split(';');
                         
                Integer HowMany = stringOC.size();
                for (Integer i = 0; i < HowMany; i++) 
                {
                    String Value =  (stringOC[i]);
          
                    if (Value != null)
                    { 
                        opptycomp.add(new OpportunityCompetitor(CompetitorName = Value, OpportunityId = newOppty.Id));
                    }    
                }
            }
        }
    }

    insert opptycomp;
}