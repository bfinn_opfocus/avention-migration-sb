trigger OrderScheduleTrigger on SFDC_520_Quote__c (before insert, before update) {
    
    if (Trigger.isBefore) 
    {
        for (SFDC_520_Quote__c q : System.Trigger.new) 
        {
            //Calculate the subscription end date from the contract length it is empty
            	SFDC_520_Quote__c oldVersion = null;
            	if(System.Trigger.OldMap !=null)
            		oldVersion = System.Trigger.OldMap.get(q.Id);
            	//Update end date if it is empty (by admins) 
            	//or if the start date and the contract length has changed(by reps)
            	
            	if(q.Subscription_End_Date__c==null
            		||(oldVersion!=null && 
            		 (
            		 	(q.Subscription_Start_Date__c!=oldVersion.Subscription_Start_Date__c)
            			||
      					(q.Contract_Length__c!=oldVersion.Contract_Length__c)
        			 )
            		)
            	)
            	{
            		Date dtSubscriptionEnd =  q.Subscription_Start_Date__c;
            		if(dtSubscriptionEnd!=null && q.Contract_Length__c !=null)
            		{
            			dtSubscriptionEnd = dtSubscriptionEnd.addMonths(q.Contract_Length__c.intValue());
            			dtSubscriptionEnd = dtSubscriptionEnd.addDays(-1);          
            			q.Subscription_End_Date__c = dtSubscriptionEnd ;
            		}
            	}
        }        
        if( Trigger.isUpdate)
        {
            OrderScheduleServices.setDocuSignLineItemFields( trigger.newMap, trigger.old );
        }
    }
}