/******************************************************************************* 
Name              : EmailPoolSetParent
Revision History  : TBD
					 
Created/Modified by   Created/Modified Date     Requested by          Related Task/Issue             
----------------------------------------------------------------------------------------
1. Mark Robinson      02/12/2014         	    Jim Horty             
*******************************************************************************/
trigger EmailPoolSetParent on Email_Pool__c (before insert) {

	EmailPoolTriggerController.linkEmailPoolCreditToSite(trigger.new);
	
}