/*
 * Trigger:  Opportunity
 * Created by OpFocus on 04/16/2015
 * Description: Opportunity Trigger on all events
 *              
 */
trigger Opportunity on Opportunity (before insert, before update, before delete, after insert, 
                                    after update, after delete, after undelete) {

    if(TriggerDispatcher.isEnabled('Opportunity') && !TriggerDispatcher.getExcludeUsers('Opportunity').containsIgnoreCase(UserInfo.getUserName())) {
        new TriggerDispatcher()                
            .bind(TriggerDispatcher.Evt.afterUpdate, new OpportunityHelper.OpportunitySplits())
            .manage();
    }
}