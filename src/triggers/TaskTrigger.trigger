trigger TaskTrigger on Task (after delete, after insert, after update) {
	List<ID> contactIds = new List<ID>();
	
	if(Trigger.isDelete){
		for (task t : Trigger.Old) {
			if((t.WhoID != null ))  
				contactIds.add(t.WhoID);
			}
	} else{
		for (task t : Trigger.New) {
			if((t.WhoID != null ))  
				contactIds.add(t.WhoID);
		}
	}
	
	if(Trigger.isAfter && Trigger.isInsert){		
		TaskServices.adjustRecentlyOpened(contactIds);
	}
	if(Trigger.isAfter && Trigger.isUpdate){
		TaskServices.adjustRecentlyModified(contactIds);
	}
	
	if(Trigger.isAfter && Trigger.isDelete){
		TaskServices.adjustRecentlyModified(contactIds);
		TaskServices.adjustRecentlyOpened(contactIds);
	}
 
}