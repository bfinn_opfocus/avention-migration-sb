trigger UpdateAccountCRM on Opportunity (before insert, before update) {
for (Opportunity o : Trigger.new) {

     if(o.Current_CRM__c != null)
	{     
	     account[] acc = [select id, CRM__c from account where id= :o.AccountId];
   
     	if(acc[0].CRM__c == null)
		{     
			acc[0].CRM__c = o.Current_CRM__c;
		}
		else if (acc[0].CRM__c.contains (o.Current_CRM__c))
		{
			acc[0].CRM__c = acc[0].CRM__c;
		}
		else
		{         
			acc[0].CRM__c = acc[0].CRM__c +'; '+o.Current_CRM__c;
		}

     	update acc;
   }



	if(o.Other_CRM__c != null)
	{     
	     account[] acc = [select id, Other_CRM__c from account where id= :o.AccountId];
        if(acc[0].Other_CRM__c == null)
		{     
			acc[0].Other_CRM__c = o.Other_CRM__c;
		}
		else if (acc[0].Other_CRM__c.contains (o.Other_CRM__c))
		{
			acc[0].Other_CRM__c = acc[0].Other_CRM__c;
		}
		else
		{         
			acc[0].Other_CRM__c = acc[0].Other_CRM__c +'; '+o.Other_CRM__c;
		}
	     update acc;
     
    }
}
}