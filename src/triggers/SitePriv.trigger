/*
** Trigger:  SitePriv
** SObject:  Site_Priv__c
** Created by OpFocus on May 27, 2015
** Description: Trigger for Site_Priv__c object. Currently, these are the triggers defined.
**     +After created, deleted, undleted, Partent changes, or IsTrial changes then flag partent site to Do_Has_Trial_SitePriv__c
**     +After created, deleted, undleted, Partent changes, or IsTrial changes then flag partent site to Do_Update_Latest_Trial_Expiration_Date__c
**     +After created, deleted, undleted, Partent changes, or IsLegacyProduct changes then flag partent site to Do_Has_Legacy_SitePriv__c
**     +Before Update & Insert: When a Site has a Extension Request Approved we will update this field to signal the site to update it's actual "Active Till" date
**     +Before insert - When SitePriv is created, is SchemaName is empty, populated it from the related DBSchema
*/
trigger SitePriv on Site_Priv__c (after Update, after Insert, after Delete, after Undelete, before Insert, before update) {
	//+If created, Partent changes, or IsTrial changes then flag partent site to Do_Has_Trial_SitePriv__c
	Map<Id, Site__c> mapSitesToUpdateHasTrial = new Map<Id, Site__c>();

    //+If created, deleted, undleted, Partent changes, or IsTrial changes then flag partent site to Do_Update_Latest_Trial_Expiration_Date__c
	Map<Id, Site__c> mapSitesToUpdateLatestTrialExpirationDate = new Map<Id, Site__c>();

	//+If created, Partent changes, or IsLegacyProduct changes then flag partent site to Do_Has_Trial_SitePriv__c
	Map<Id, Site__c> mapSitesToUpdateHasLegacy = new Map<Id, Site__c>();

	//Before insert - When SitePriv is created, is SchemaName is empty, populated it from the related DBSchema
	Map<Id, Site_Priv__c> mapSitePrivsToUpdateSchemaName = new Map<Id, Site_Priv__c>();

	//Before insert - When SitePriv is created, is SchemaName is empty, populated it from the related DBSchema
	if(Trigger.isBefore && Trigger.isInsert){
		for(Site_Priv__c sitePriv : trigger.new){
			if(sitePriv.Schema_Name__c == null && sitePriv.Schema__c != null){
				mapSitePrivsToUpdateSchemaName.put(sitePriv.Schema__c, sitePriv);
			}
		}
	}


	//When a Site has a Extension Request Approved we will update this field to signal the site to update it's actual "Active Till" date
	if(trigger.isBefore && (trigger.isUpdate || trigger.isInsert)){
		for(Site_Priv__c sitePriv : trigger.new){
			if(sitePriv.Extension_Request_Date__c != null){
				sitePriv.Active_Till__c = sitePriv.Extension_Request_Date__c.addHours(-(Utils.getTimeZoneOffset()));
				sitePriv.Extension_Request_Date__c = null;
				SiteHelper.sitesThatExtendedEndDates.add(sitePriv.Site__c);
			}
		}
	}

	if(trigger.isAfter && (trigger.isUpdate || trigger.isInsert || trigger.isUndelete)){
		for(Site_Priv__c sitePriv : trigger.new){
			if(trigger.isUpdate){
				Boolean parentChanged = sitePriv.Site__c != trigger.oldMap.get(sitePriv.id).Site__c;
				Boolean isTrialChanged = sitePriv.Is_Trial__c != trigger.oldMap.get(sitePriv.id).Is_Trial__c;
				Boolean isLegacyChanged = sitePriv.Is_Legacy_Product__c != trigger.oldMap.get(sitePriv.id).Is_Legacy_Product__c;
				Boolean expirationDateChanged = sitePriv.Active_Till__c != trigger.oldMap.get(sitePriv.id).Active_Till__c;
				//We have to do this when expiration date changes too since an expired trial priv makes the site live
				if((parentChanged || isTrialChanged || expirationDateChanged) && expirationDateChanged){
					//if we already extended the end date of the Site__c we won't flag the Site__c to update again (Recursion!)
					if(!SiteHelper.sitesThatExtendedEndDates.contains(sitePriv.Site__c)){
						mapSitesToUpdateHasTrial.put(sitePriv.Site__c, New Site__c(Id=sitePriv.Site__c, Do_Has_Trial_SitePriv__c=True));
						mapSitesToUpdateLatestTrialExpirationDate.put(sitePriv.Site__c, New Site__c(Id=sitePriv.Site__c, Do_Update_Latest_Trial_Expiration_Date__c=True));
					}
				}
				if((parentChanged || isTrialChanged) && isLegacyChanged){
					//if we already extended the end date of the Site__c we won't flag the Site__c to update again (Recursion!)
					if(!SiteHelper.sitesThatExtendedEndDates.contains(sitePriv.Site__c)){
						mapSitesToUpdateHasLegacy.put(sitePriv.Site__c, New Site__c(Id=sitePriv.Site__c, Do_Has_Lecacy_SitePriv__c=True));
					}
				}
			}
			if(trigger.isInsert || trigger.isUndelete){
				mapSitesToUpdateHasTrial.put(sitePriv.Site__c, New Site__c(Id=sitePriv.Site__c, Do_Has_Trial_SitePriv__c=True));
				mapSitesToUpdateLatestTrialExpirationDate.put(sitePriv.Site__c, New Site__c(Id=sitePriv.Site__c, Do_Update_Latest_Trial_Expiration_Date__c=True));
				mapSitesToUpdateHasLegacy.put(sitePriv.Site__c, New Site__c(Id=sitePriv.Site__c, Do_Has_Lecacy_SitePriv__c=True));
			}
		}
	}
	if(trigger.isAfter && trigger.isDelete){
		for(Site_Priv__c sitePriv : trigger.old){
			mapSitesToUpdateHasTrial.put(sitePriv.Site__c, New Site__c(Id=sitePriv.Site__c, Do_Has_Trial_SitePriv__c=True));
			mapSitesToUpdateLatestTrialExpirationDate.put(sitePriv.Site__c, New Site__c(Id=sitePriv.Site__c, Do_Update_Latest_Trial_Expiration_Date__c=True));
			mapSitesToUpdateHasLegacy.put(sitePriv.Site__c, New Site__c(Id=sitePriv.Site__c, Do_Has_Lecacy_SitePriv__c=True));
		}
	}

	if(!mapSitePrivsToUpdateSchemaName.values().isEmpty()){
		SitePrivHelper.updateSchemaName(mapSitePrivsToUpdateSchemaName.values());
	}
	if(!mapSitesToUpdateHasTrial.values().isEmpty()){
		update mapSitesToUpdateHasTrial.values();
	}
	if(!mapSitesToUpdateLatestTrialExpirationDate.values().isEmpty()){
		update mapSitesToUpdateLatestTrialExpirationDate.values();
	}
	if(!mapSitesToUpdateHasLegacy.values().isEmpty()){
		update mapSitesToUpdateHasLegacy.values();
	}
}