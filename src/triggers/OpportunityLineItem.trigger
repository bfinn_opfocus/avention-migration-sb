/*
 * Trigger:  OpportunityLineItem
 * Created by OpFocus on 04/15/2015
 * Description: All trigger logic OpportunityLineItem object (also known as Opportunity Products)
 *              
 */
trigger OpportunityLineItem on OpportunityLineItem (before insert, before update, before delete, 
    after insert, 
    after update, 
    after delete, 
    after undelete) {

        if(TriggerDispatcher.isEnabled('OpportunityLineItem') && !TriggerDispatcher.getExcludeUsers('OpportunityLineItem').containsIgnoreCase(UserInfo.getUserName())) {
            new TriggerDispatcher()                
                // Create the Schedules for the Opportunities based on the new formulae fields. 
                .bind(TriggerDispatcher.Evt.afterInsert, new OpportunityLineItemHelper.OpportunitySchedules()) 
				.bind(TriggerDispatcher.Evt.afterUpdate, new OpportunityLineItemHelper.OpportunitySchedules()) 
                // Get the actual amounts from the Opportunity Line Item Schedules, if present and populate on 
                //Actual_Year_1_Amount__c, Actual_Year_2_Amount__c and Actual_Year_3_Amount__c
                .bind(TriggerDispatcher.Evt.beforeUpdate, new OpportunityLineItemHelper.OpportunityProductActualAmounts(false))
                .manage();
        }

}