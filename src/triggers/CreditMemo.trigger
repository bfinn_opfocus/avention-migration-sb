trigger CreditMemo on Credit_Memo__c (before insert,before update,after insert, after update, after delete) {

	if(Trigger.isBefore)
	{
		set<Id> setDropOrderId  = new set<Id>();
		set<Id> setAllOrderId  = new set<Id>();
		
		List<Credit_Memo__c> lstDropCreditMemoToUpdate = new List<Credit_Memo__c>();
		List<Credit_Memo__c> lstCreditMemoToUpdateOrderNo = new List<Credit_Memo__c>();
		if(Trigger.isInsert)
		{
			
			
			for(Credit_Memo__c newMemo:System.Trigger.new)
			{
				if(newMemo.Type__c=='Drop')
				{
					setDropOrderId.Add(newMemo.Order__c);
					lstDropCreditMemoToUpdate.Add(newMemo);
				}
				setAllOrderId.Add(newMemo.Order__c);
				lstCreditMemoToUpdateOrderNo.Add(newMemo);
			}
			
		}
		if(Trigger.isUpdate)
		{
			for(Credit_Memo__c newMemo:System.Trigger.new)
			{
				Credit_Memo__c oldMemo = System.Trigger.oldMap.get(newMemo.Id);
				//Making Estimated Drop Amount as read only
				newMemo.Estimated_Drop_Amount__c = oldMemo.Estimated_Drop_Amount__c;
				if (newMemo.Type__c=='Drop')
				 {
					if((newMemo.Order__c!=oldMemo.Order__c) || (newMemo.New_Contract_End_Date__c!=oldMemo.New_Contract_End_Date__c))
					{
				 		setDropOrderId.Add(newMemo.Order__c);
				 		lstDropCreditMemoToUpdate.Add(newMemo);
					}
				 }
				 setAllOrderId.Add(newMemo.Order__c);
				 lstCreditMemoToUpdateOrderNo.Add(newMemo);
				 
				 /*if(newMemo.Order__c!=oldMemo.Order__c)
				 {
				 	setAllOrderId.Add(newMemo.Order__c);
					lstCreditMemoToUpdateOrderNo.Add(newMemo);
				 } else
				 {
				 	 newMemo.Order_Number__c = oldMemo.Order_Number__c;
				 }*/
			}
		
		}
		
		if(setAllOrderId.size()>0)
		{
			
			Map<Id,Order__c> mapIdOrders = new Map<Id,Order__c>([Select Id, Contract_Amount__c, Contract_End_Date__c, Contract_Start_Date__c, Subscription_End_Date__c, Subscription_Start_Date__c, Order_Number__c,CurrencyIsoCode from Order__c where Id in :setAllOrderId]);
			for(Credit_Memo__c newMemo:lstDropCreditMemoToUpdate)
			{				
				Order__c order = mapIdOrders.get(newMemo.Order__c);
				newMemo.CurrencyIsoCode= order.CurrencyIsoCode;
				Date dtDropDate =newMemo.New_Contract_End_Date__c;
				Date dtStartDate =order.Subscription_Start_Date__c;  
				if(dtDropDate<dtStartDate)
					newMemo.New_Contract_End_Date__c.addError('Drop date should be latter than Contract Start Date :' + order.Contract_Start_Date__c);
				
				Integer intUsedLength = dtStartDate.monthsBetween(dtDropDate.addDays(1));
				if(dtDropDate==dtStartDate)
					intUsedLength = 0;
				if(intUsedLength==0)
				{
					if(dtDropDate!=dtStartDate)
					{
						newMemo.New_Contract_End_Date__c.addError('Drop date should be in month increments of '+ dtStartDate);
					}
				}else
				{
					for(integer i=0;;i++)
					{
						Date dtMonthEnd = dtStartDate.addMonths(i);
						dtMonthEnd = dtMonthEnd.addDays(-1);
						if(dtMonthEnd>dtDropDate)
						{
							newMemo.New_Contract_End_Date__c.addError('Drop date should be in month increments  of '+ dtStartDate);
							break;
						}
						if(dtMonthEnd==dtDropDate)
							break;
					}
				}
					
				Integer intContractLength = order.Subscription_Start_Date__c.monthsBetween(order.Subscription_End_Date__c.addDays(1));
				decimal curContractAmount = order.Contract_Amount__c;
				
				system.debug(logginglevel.INFO ,'***CreditMemo Contract Amount***');
				system.debug(logginglevel.INFO ,'Contract Amount:$' + curContractAmount +';Used Length:'+ intUsedLength + ';Contract Length:' + intContractLength);
				
				decimal  curDropAmount = (intContractLength != 0) ? (curContractAmount*(intContractLength-intUsedLength))/intContractLength : 0;
				newMemo.Estimated_Drop_Amount__c = curDropAmount;
				//newMemo.
			}
			for(Credit_Memo__c newMemo:lstCreditMemoToUpdateOrderNo)
			{
				Order__c order = mapIdOrders.get(newMemo.Order__c);
				newMemo.Order_Number__c = order.Order_Number__c;
			}
				
		}
	}

	if(Trigger.isAfter)
	{
		
		set<Id> setOrderIdToCreateLines = new set<Id>();
		set<Id> setMemosWithOrderChange = new set<Id>();
		Map<Id,Id> mapMemoOrderId  = new Map<Id,Id>();
		Map<Id,Set<Id>> mapOrderMemoSet = new Map<Id,Set<Id>>();
		Map<Id,Id> mapDropMemoIdOrderId = new Map<Id,Id> ();	
	
		if(Trigger.isInsert)
		{
			for(Credit_Memo__c newMemo:System.Trigger.new)
			{
				//if(newMemo.Type__c=='Price Adjustment')
				{
					mapMemoOrderId.put(newMemo.Id,newMemo.Order__c);
			 		set<Id> setMemoId = mapOrderMemoSet.get(newMemo.Order__c);
		 
			 		if(setMemoId==null)
			 		{
			 			setMemoId = new Set<Id>();
			 		}
			 		setMemoId.Add(newMemo.Id);
			 		
			 		mapOrderMemoSet.put(newMemo.Order__c,setMemoId);
				}
                List<Credit_Memo__c> filteredMemos = CreditMemoServices.filterCreditMemo(Trigger.new);
                if(! filteredMemos.isEmpty()){
                   CreditMemoServices.memosDroppingOrder(filteredMemos);
                }
			}	
		}
		if(Trigger.isDelete) {
		    CreditMemoServices.updateMemosWithoutDroppedOrders(trigger.old);
		}
		if(Trigger.isUpdate)
		{
			for(Credit_Memo__c newMemo:System.Trigger.new)
			{
				Credit_Memo__c oldMemo = System.Trigger.oldMap.get(newMemo.Id);
				
				if(oldMemo.Memo_Status__c!='Approved' && newMemo.Memo_Status__c=='Approved')
				{
					//Do what is required for Approval event
					if(newMemo.Type__c=='Drop')
					{
						mapDropMemoIdOrderId.put(newMemo.Id,newMemo.Order__c);
					}
				}
				
				//if ((newMemo.Type__c=='Price Adjustment')||(oldMemo.Type__c =='Price Adjustment'))
				 {
					if(newMemo.Order__c!=oldMemo.Order__c)
					{
				 		
				 		mapMemoOrderId.put(newMemo.Id,newMemo.Order__c);
				 		
				 		set<Id> setMemoId = mapOrderMemoSet.get(newMemo.Order__c);
				 		 
				 		if(setMemoId==null)
				 		{
				 			setMemoId = new Set<Id>();
				 		}
				 		setMemoId.Add(newMemo.Id);
				 		mapOrderMemoSet.put(newMemo.Order__c,setMemoId);
				 		
					}
				 }
			}
		}		
		//Delete the old ones which are not needed
		if(mapMemoOrderId.KeySet().size()>0)
		{
			List<Credit_Memo_Line_Item__c> lstCreditMemoLineToDelete = new List<Credit_Memo_Line_Item__c>([select Id from Credit_Memo_Line_Item__c where Credit_Memo__c in :mapMemoOrderId.KeySet()]);
			if(lstCreditMemoLineToDelete.size()>0)
				delete lstCreditMemoLineToDelete;
		}
		Map<string,Id> mapCMLRecordTypeIdName = new Map<string,Id> ();
		 
		//Create new Memo Lines from Order_Product__c
		if(mapMemoOrderId.Values().size()>0)
		{
			//set<Id> setOrderIdToCreateLines = new set<Id>();
			setOrderIdToCreateLines.addAll(mapMemoOrderId.Values());
			 
			Map<Id,Order__c> mapIdOrders = new Map<Id,Order__c>([Select Id, Contract_Amount__c, Contract_End_Date__c, Contract_Start_Date__c, Subscription_End_Date__c, Subscription_Start_Date__c, Order_Number__c from Order__c where Id in :setOrderIdToCreateLines]);
			List<Order_Product__c> lstOrderProducts = new List<Order_Product__c>([Select Contract_Line_Amount__c, Contract_Line_Number__c, Product__c, Sales_Price__c, Users__c,Order__c,CurrencyIsoCode from Order_Product__c where Order__c in :setOrderIdToCreateLines]);
			List<Credit_Memo_Line_Item__c> lstCreditMemoLineToInsert = new List<Credit_Memo_Line_Item__c>();
			
			List<RecordType>lstRecordType = new List<RecordType>([Select BusinessProcessId, Id,Name from RecordType where IsActive=true and SobjectType='Credit_Memo_Line_Item__c']); 
			for(RecordType rt:lstRecordType)
			{	
				mapCMLRecordTypeIdName.put(rt.Name,rt.Id);
			}
			//Same order can be present in multiple memos. so need to create memo line for each memo.
			
			for (Order_Product__c ordprod : lstOrderProducts) 
			{
				for(Id memoId:mapOrderMemoSet.get(ordprod.Order__c))
				{
					Credit_Memo_Line_Item__c newLine = new Credit_Memo_Line_Item__c();
				//newLine.Adjustment_Amount__c;
				 newLine.Contract_Line_Amount__c = ordprod.Contract_Line_Amount__c;
				 newLine.Contract_Line_Number__c = ordprod.Contract_Line_Number__c;
				 
				 newLine.Credit_Memo__c = memoId;
				 newLine.Product__c = ordprod.Product__c;
				 newLine.CurrencyIsoCode=ordprod.CurrencyIsoCode;
				 newLine.Sales_Price__c = ordprod.Sales_Price__c;
				 newLine.Users__c =ordprod.Users__c;
				 //Determine Record Type for the Credit Memo Line
				 Credit_Memo__c newMemo = Trigger.newMap.get(memoId);
				 if(newMemo.Type__c=='Drop')
				 {
				 	newLine.RecordTypeId = mapCMLRecordTypeIdName.get('Drop');
				 	if(newLine.Contract_Line_Amount__c>0)
				 	{
				 		Order__c order = mapIdOrders.get(newMemo.Order__c);
						Date dtDropDate =newMemo.New_Contract_End_Date__c;
						Date dtStartDate =order.Subscription_Start_Date__c;
						//If dtDropDate and dtStartDate are same then used length is 0
						Integer intUsedLength =0; 
						if(dtDropDate>dtStartDate)
							intUsedLength = dtStartDate.monthsBetween(dtDropDate.addDays(1));
									
						Integer intContractLength = order.Subscription_Start_Date__c.monthsBetween(order.Subscription_End_Date__c.addDays(1));
						decimal curContractAmount = newLine.Contract_Line_Amount__c;
						
						decimal  curDropAmount =(curContractAmount*(intContractLength-intUsedLength))/intContractLength;
						decimal curFinalContractLineAmount = curContractAmount - curDropAmount;
				 		newLine.Final_Contract_Amount__c = curFinalContractLineAmount;
				 	}				 	
				 }
				 else if(newMemo.Type__c=='Price Adjustment')
				 	newLine.RecordTypeId =  mapCMLRecordTypeIdName.get('Adjustment');
				 else if(newMemo.Type__c=='Bad Debt')
				 	newLine.RecordTypeId = mapCMLRecordTypeIdName.get('Bad Debt');	
				 
				 lstCreditMemoLineToInsert.add(newLine);
				}
			}//end for
			
			if(lstCreditMemoLineToInsert.size()>0)
			 insert lstCreditMemoLineToInsert;
		}//newmapmemo
		if(mapDropMemoIdOrderId.KeySet().size()>0)
		{
			 	List<Order__c> lstOrderToUpdateDrop = new List<Order__c> (); 
			 	Map<Id,Order__c> mapDropIdOrder = new Map<Id,Order__c>([Select Id,Dropped__c, Contract_Amount__c, Contract_End_Date__c, Contract_Start_Date__c, Subscription_End_Date__c, Subscription_Start_Date__c, Order_Number__c from Order__c where Id in :mapDropMemoIdOrderId.values()]);
			 	for(Id memoId:mapDropMemoIdOrderId.KeySet())
			 	{
			 		Credit_Memo__c newMemo = Trigger.newMap.get(memoId);
			 		Order__c orderToUpdate = mapDropIdOrder.get(mapDropMemoIdOrderId.get(memoId));
			 		if(newMemo.New_Contract_End_Date__c==orderToUpdate.Contract_Start_Date__c)
			 		{
			 			orderToUpdate.Dropped__c=true;
			 			lstOrderToUpdateDrop.Add(orderToUpdate);
			 		}	
			 	}
			 	if(lstOrderToUpdateDrop.size()>0)
			 	{
			 		
			 		update lstOrderToUpdateDrop;
			 		
			 	}
			 	
		}
	}
}