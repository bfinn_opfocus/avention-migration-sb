/******************************************************************************* 
Name              : SitePrivIncrementCreditPoolUsers
Revision History  : TBD
					 
Created/Modified by   Created/Modified Date     Requested by          Related Task/Issue             
----------------------------------------------------------------------------------------
1. Mark Robinson      02/12/2014         	    Jim Horty             
*******************************************************************************/
trigger SitePrivIncrementCreditPoolUsers on Site_Priv__c (before update) {
	
	SitePrivTriggerController.incrementCreditPoolUsers(trigger.new, trigger.oldMap);

}