/******************************************************************************* 
Name              : BusinessContactEmailSetParent
Revision History  : TBD
					 
Created/Modified by   Created/Modified Date     Requested by          Related Task/Issue             
----------------------------------------------------------------------------------------
1. Mark Robinson      02/12/2014         	    Jim Horty             
*******************************************************************************/
trigger BusinessContactEmailSetParent on Business_Contact_Email__c (before insert) {

	BusinessContactEmailTriggerController.linkBusinessContactEmailToSite(trigger.new);
	
}