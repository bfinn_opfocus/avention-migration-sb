/******************************************************************************* 
Name              : SiteDomainSetParent
Revision History  : TBD
					 
Created/Modified by   Created/Modified Date     Requested by          Related Task/Issue             
----------------------------------------------------------------------------------------
1. Mark Robinson      02/12/2014         	    Jim Horty             
*******************************************************************************/
trigger SiteDomainSetParent on Site_Domain__c (before insert) {
	
	SiteDomainTriggerController.assignKeyValue(trigger.new);
	SiteDomainTriggerController.linkSiteDomainToSite(trigger.new);	

}