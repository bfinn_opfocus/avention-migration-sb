/******************************************************************************* 
Name              : CRMCreditSetParent
Revision History  : TBD
					 
Created/Modified by   Created/Modified Date     Requested by          Related Task/Issue             
----------------------------------------------------------------------------------------
1. Mark Robinson      02/12/2014         	    Jim Horty             
*******************************************************************************/
trigger CRMCreditSetParent on CRM_Credit__c (before insert) {

	CRMCreditTriggerController.linkCRMCreditToSite(trigger.new);
	
}