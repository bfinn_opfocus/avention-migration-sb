trigger AppExchangeCampMemb on Lead (after insert) {

List<CampaignMember> campmemb = new List<CampaignMember>();

	
	for (Lead newLead: Trigger.New) {
		if(newLead.LeadSource != null)
		{
			if (newLead.LeadSource.contains ('Account Intelligence')) 
			{
				if (newLead.LeadSource.contains ('dup'))
				{
					continue;
				}
				else
				{
					campmemb.add(new CampaignMember(CampaignId = '701300000004HKY', LeadId = newLead.Id, Status = 'Responded'));
				}	
			}
			if (newLead.LeadSource.contains ('OneSource for Salesforce')) 
			{
				if (newLead.LeadSource.contains ('dup'))
				{
					continue;
				}
				else
				{
					campmemb.add(new CampaignMember(CampaignId = '70160000000I3K3', LeadId = newLead.Id, Status = 'Responded'));
				}	
			}
		}
	}
	insert campmemb;


}