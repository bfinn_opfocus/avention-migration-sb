/*
** Trigger:  Site
** SObject:  Site__c
** Created by OpFocus on May 27, 2015
** Description: Trigger for Site object. Currently, these are the triggers defined.
**     + Recalculates the Has_Trial_SitePriv__c field if Do_Has_Trial_SitePriv__c field is checked
**     + Recalculates the Is_Legacy_Product__c field if Do_Has_Trial_SitePriv__c field is checked
**     + Recalculates the Latest_Trial_Expiration_Date__c field if Do_Update_Latest_Trial_Expiration_Date__c field is checked
**     + Update static contact fields if contact lookup is changed
**     + When Requested_Extension_Status__c becomes Approved then extend the date of all the SitePrivs
**     + Update Type__c field (CLIENT / TRIAL) if Has_Legacy_SitePriv__c is false
*/
trigger Site on Site__c (Before Update, Before Insert) {
	Map<Id, Site__c> mapSitesToRecalculateHasTrial = new Map<Id, Site__c>();
	Map<Id, Site__c> mapSitesToRecalculateLatestTrialExipiration = new Map<Id, Site__c>();
	Map<Id, Site__c> mapSitesToRecalculateHasLegacy = new Map<Id, Site__c>();
	Map<Id, Site__c> mapSiteToUpdatContacts = new Map<Id, Site__c>();
	Map<Id, Site__c> mapSiteToExtendTrial = new Map<Id, Site__c>();	
	Map<Id, Site__c> mapSiteToCalculateType = new Map<Id, Site__c>();	

	
	if(Trigger.isBefore && (trigger.isInsert)){
		for(Site__c site : trigger.new){
			//+ Recalculates the Has_Trial_SitePriv__c field if Do_Has_Trial_SitePriv__c field is checked
			if(site.Do_Has_Trial_SitePriv__c){
				mapSitesToRecalculateHasTrial.put(site.Id, site);
				site.Do_Has_Trial_SitePriv__c = false;
			}
			//+ Recalculates the Latest_Trial_Expiration_Date__c field if Do_Update_Latest_Trial_Expiration_Date__c field is checked
			if(site.Do_Update_Latest_Trial_Expiration_Date__c){
				mapSitesToRecalculateLatestTrialExipiration.put(site.Id, site);
				site.Do_Update_Latest_Trial_Expiration_Date__c = false;
			}
			//+ Recalculates the Is_Legacy_Product__c field if Do_Has_Lecacy_SitePriv__c field is checked
			if(site.Do_Has_Lecacy_SitePriv__c){
				mapSitesToRecalculateHasLegacy.put(site.Id, site);
				site.Do_Has_Lecacy_SitePriv__c = false;
			}

			//+ Update static contact fields if contact lookup is changed
			if(site.Contact__c != null){
				mapSiteToUpdatContacts.put(site.Id, site);
			}
			//+ When Requested_Extension_Status__c becomes Approved then extend the date of all the SitePrivs
			if(site.Requested_Extension_Status__c == 'Approved' && site.Requested_Extension_Date__c != null){
				mapSiteToExtendTrial.put(site.Id, site);
			}
			//+ Update Type__c field (CLIENT / TRIAL) if Has_Legacy_SitePriv__c is false
			if(!site.Has_Legacy_SitePriv__c){
				mapSiteToCalculateType.put(site.Id, site);
			}
		}
	}
	//Update trigger
	if(Trigger.isBefore && (trigger.isUpdate)){
		for(Site__c site : trigger.new){

			//+ Recalculates the Has_Trial_SitePriv__c field if Do_Has_Trial_SitePriv__c field is checked
			if(site.Do_Has_Trial_SitePriv__c){
				mapSitesToRecalculateHasTrial.put(site.Id, site);
				mapSiteToCalculateType.put(site.Id, site);
				site.Do_Has_Trial_SitePriv__c = false;
			}

			//+ Recalculates the Latest_Trial_Expiration_Date__c field if Do_Update_Latest_Trial_Expiration_Date__c field is checked
			if(site.Do_Update_Latest_Trial_Expiration_Date__c){
				mapSitesToRecalculateLatestTrialExipiration.put(site.Id, site);
				mapSiteToCalculateType.put(site.Id, site);
				site.Do_Update_Latest_Trial_Expiration_Date__c = false;
			}

			//+ Recalculates the Has_Legacy_SitePriv__c field if Do_Has_Lecacy_SitePriv__c field is checked
			if(site.Do_Has_Lecacy_SitePriv__c){
				mapSitesToRecalculateHasLegacy.put(site.Id, site);
				mapSiteToCalculateType.put(site.Id, site);
				site.Do_Has_Lecacy_SitePriv__c = false;
			}
			//+ Update Type__c field (CLIENT / TRIAL) if Has_Legacy_SitePriv__c is false
			// We need to flag it to recalculate is Has_Legacy_SitePriv__c has change OR Has_Trial_SitePriv__c has changed OR Do_Has_Trial_SitePriv__c is already checked (because this could cause Has_Trial_SitePriv__c to change anyhow) 
			if((site.Has_Legacy_SitePriv__c <> trigger.oldMap.get(site.Id).Has_Legacy_SitePriv__c) || (site.Has_Trial_SitePriv__c <> trigger.oldMap.get(site.Id).Has_Trial_SitePriv__c) || site.Do_Has_Trial_SitePriv__c || site.CUSTTYPE__c == null){
				mapSiteToCalculateType.put(site.Id, site);
			}
			//+ Update static contact fields if contact lookup is changed
			if(site.Contact__c != null && site.Contact__c != trigger.oldMap.get(site.Id).Contact__c){
				mapSiteToUpdatContacts.put(site.Id, site);
			}
			//+ When Requested_Extension_Status__c becomes Approved then extend the date of all the SitePrivs
			if(site.Requested_Extension_Status__c == 'Approved' && trigger.oldMap.get(site.Id).Requested_Extension_Status__c != 'Approved' && site.Requested_Extension_Date__c != null){
				mapSiteToExtendTrial.put(site.Id, site);
			}
		}
	}

	//Do updates to records
	if(!mapSitesToRecalculateHasTrial.values().isEmpty()){
		SiteHelper.UpdateHasTrialSitePriv(mapSitesToRecalculateHasTrial.values());
	}
	if(!mapSitesToRecalculateLatestTrialExipiration.values().isEmpty()){
		SiteHelper.UpdateLatestTrialSitePrivDate(mapSitesToRecalculateLatestTrialExipiration.values());
	}
	if(!mapSitesToRecalculateHasLegacy.values().isEmpty()){
		SiteHelper.UpdateHasLegacySitePriv(mapSitesToRecalculateHasLegacy.values());
	}
	if(!mapSiteToUpdatContacts.values().isEmpty()){
		SiteHelper.UpdateContactFields(mapSiteToUpdatContacts.values());
	}
	if(!mapSiteToExtendTrial.values().isEmpty()){
		SiteHelper.ExtendSitePrivDates(mapSiteToExtendTrial.values());
		for(Site__c site : mapSiteToExtendTrial.values()){
			site.Requested_Extension_Date__c = null;
			site.Requested_Extension_Status__c = null;
		}
	}
	if(!mapSiteToCalculateType.values().isEmpty()){
		SiteHelper.CalculateSiteType(mapSiteToCalculateType.values());
	}
}