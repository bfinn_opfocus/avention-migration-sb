/*
** Trigger:  OrderTrigger
** SObject:  Order
** Description:  Trigger for Order
** 		The trigger logic in place is to handle different functionality as described below.
** 		 
**      ++ afterUpdate  
**			  When Order has been submitted to Intacct, they update the Order's Intaact_Id field.
**			  When this is done, we need to update the associated Opportunity to mark it as 
**			  Closed/Won. Also we want to make sure all Order Products are carried over to 
**			  Opportunity Products.
** 		++ afterUpdate (added 10/23/2015 OpFocus)
**			When an Order is created from a Steelbrick Quote, we need to pull the QLIs over to the
**			Order Products. There may be multiple QLIs for the same Product Code and if so, we will
**			merge them and only create a single Order Product record. This is done in an update
**			trigger as it is executed after the process that creates it. So we key off a field
**			set by the Process.
**/ 
trigger OrderTrigger on Order__c (before insert, after insert, before update, after update)
{	/*
	if (Trigger.isBefore) 
 	{
 		if(Trigger.isInsert)
 		{
 			set<Id> setOwnerId = new set<Id>();
 			for (Order__c o : System.Trigger.new) 
			{
				setOwnerId.Add(o.OwnerId);
			}
			Map<Id,User> mapIdUser = new Map<Id,User>([Select ManagerId, Manager.Email, UserRoleId,UserRole.Name from User where Id in :setOwnerId]);
			
			for (Order__c o : System.Trigger.new) 
			{
				User owner =mapIdUser.get(o.OwnerId);
				o.Owner_Manager__c = owner.ManagerId;
				o.Owner_Manager_Email__c = owner.Manager.Email;
				o.Owner_Role__c = owner.UserRole.Name;				
			}
		}
		
		if(Trigger.isUpdate)
 		{
 			set<Id> setOwnerId = new set<Id>();
 			List<Order__c> lstOrdersToUpdateOwnerDependencies = new List<Order__c>();
 			for (Order__c newOrder : System.Trigger.new) 
			{
				
				Order__c oldOrder = System.Trigger.oldMap.get(newOrder.Id);
				if(newOrder.OwnerId!=oldOrder.OwnerId)
				{
					lstOrdersToUpdateOwnerDependencies.Add(newOrder);
					setOwnerId.Add(newOrder.OwnerId);	
				}
			}
			Map<Id,User> mapIdUser = new Map<Id,User>([Select ManagerId, Manager.Email, UserRoleId,UserRole.Name from User where Id in :setOwnerId]);
			
			for (Order__c o : lstOrdersToUpdateOwnerDependencies) 
			{
				User owner =mapIdUser.get(o.OwnerId);
				o.Owner_Manager__c = owner.ManagerId;
				o.Owner_Manager_Email__c = owner.Manager.Email;
				o.Owner_Role__c = owner.UserRole.Name;				
			}
 		}
	}*/

	List<Site_Order__c> lstSiteOrdersToCreate = new List<Site_Order__c>();

	if (Trigger.isBefore) 
 	{
 		if(Trigger.isInsert)
 		{
 			set<Id> setOwnerId = new set<Id>();
 			for (Order__c o : System.Trigger.new) 
			{
				setOwnerId.Add(o.OwnerId);
			}
			Map<Id,User> mapIdUser = new Map<Id,User>([Select ManagerId, Manager.Email, UserRoleId,UserRole.Name from User where Id in :setOwnerId]);
			
			for (Order__c o : System.Trigger.new) 
			{
				User owner =mapIdUser.get(o.OwnerId);
				o.Owner_Manager__c = owner.ManagerId;
				o.Owner_Manager_Email__c = owner.Manager.Email;
				o.Owner_Role__c = owner.UserRole.Name;


				// + Create Site_Opportunity__c junction record
				if(o.Site__c != null){
					lstSiteOrdersToCreate.add(new Site_Order__c(Order__c = o.id, Site__c = o.Site__c));
				}			
			}

			//Do updates to records
			if(!lstSiteOrdersToCreate.isEmpty()){
				Insert lstSiteOrdersToCreate;
			}						
		}
		
		if(Trigger.isUpdate)
 		{
 			
 			set<Id> setOwnerId = new set<Id>();
 			set<Id> setReturnToAccountIdsToRetrieve = new set<Id>();
 			set<Id> setReturnToContactIdsToRetrieve = new set<Id>();
 			List<Order__c> lstOrdersToUpdateReturnToAddress= new List<Order__c>();
 			List<Order__c> lstOrdersToUpdateReturnToContact= new List<Order__c>();
 			
 			List<Order__c> lstOrdersToUpdateOwnerDependencies = new List<Order__c>();
 			for (Order__c newOrder : System.Trigger.new) 
			{
				boolean billToAddressUpdated = false;
 				boolean siteAddressUpdated = false;
 				boolean siteContactUpdated=false;
 				boolean billToContactUpdated=false;
				
				Order__c oldOrder = System.Trigger.oldMap.get(newOrder.Id);
				if(newOrder.OwnerId!=oldOrder.OwnerId)
				{
					lstOrdersToUpdateOwnerDependencies.Add(newOrder);
					setOwnerId.Add(newOrder.OwnerId);	
				}
				
				//Return to Address and Contact updates as this is not handled when final approval/update order is clicked.
				//<change>
				if((oldOrder.Site_Address_1__c!=newOrder.Site_Address_1__c) ||
				(oldOrder.Site_Address_2__c!=newOrder.Site_Address_2__c) ||
				(oldOrder.Site_City__c!=newOrder.Site_City__c) ||
				(oldOrder.Site_State__c!=newOrder.Site_State__c) ||
				(oldOrder.Site_Postal_Code__c!=newOrder.Site_Postal_Code__c)||
				(oldOrder.Site_Country__c!=newOrder.Site_Country__c)
				)
				{
					siteAddressUpdated = true;
				}
				
				if(
				(oldOrder.System_Administrator_Email__c!=newOrder.System_Administrator_Email__c)||
				(oldOrder.System_Administrator_Phone__c!=newOrder.System_Administrator_Phone__c)
				)
				{
					siteContactUpdated=true;
				}
				
				if((oldOrder.Bill_To_Address_1__c!=newOrder.Bill_To_Address_1__c) ||
				(oldOrder.Bill_To_Address_2__c!=newOrder.Bill_To_Address_2__c) ||
				(oldOrder.Bill_To_City__c!=newOrder.Bill_To_City__c) ||
				(oldOrder.Bill_To_State__c!=newOrder.Bill_To_State__c) ||
				(oldOrder.Bill_To_Postal_Code__c!=newOrder.Bill_To_Postal_Code__c)||
				(oldOrder.Bill_To_Country__c!=newOrder.Bill_To_Country__c)
				)
				{
					billToAddressUpdated = true;
				}
				
				if(
				(oldOrder.Bill_To_Contact_Email__c!=newOrder.Bill_To_Contact_Email__c)||
				(oldOrder.Bill_To_Contact_Phone__c!=newOrder.Bill_To_Contact_Phone__c)
				)
				{
					billToContactUpdated=true;
				}
				if(siteAddressUpdated||siteContactUpdated)
				{
					// if Site Account and Return to Account are the same,
					// set Return To (Sys Admin) address to Site address details.
					// If they are different, we handle it below.
					if(newOrder.Site_Account__c== newOrder.Return_To_Account__c)
					{
						// Return_To is System_Adminstrator labled fields.
						newOrder.Return_To_Address_1__c = newOrder.Site_Address_1__c;
						newOrder.Return_To_Address_2__c=newOrder.Site_Address_2__c;
						newOrder.Return_To_City__c=newOrder.Site_City__c;
						newOrder.Return_To_State__c=newOrder.Site_State__c;
						newOrder.Return_To_Country__c=newOrder.Site_Country__c;
						newOrder.Return_To_Postal_Code__c=newOrder.Site_Postal_Code__c;
					}
					if(newOrder.System_Administrator__c==newOrder.Return_To_Contact__c)
					{
						newOrder.Return_To_Contact_Email__c=newOrder.System_Administrator_Email__c;
						newOrder.Return_To_Contact_Phone__c=newOrder.System_Administrator_Phone__c;
					}
				}
				if(billToAddressUpdated|| billToContactUpdated)
				{
					// bill to address has been updated. If the Bill To Account
					// and System Administrator (Return to) Account are the same,
					// update the System Admiistrator account
					if(newOrder.Bill_To_Account__c== newOrder.Return_To_Account__c)
					{
						newOrder.Return_To_Address_1__c = newOrder.Bill_To_Address_1__c;
						newOrder.Return_To_Address_2__c=newOrder.Bill_To_Address_2__c;
						newOrder.Return_To_City__c=newOrder.Bill_To_City__c;
						newOrder.Return_To_State__c=newOrder.Bill_To_State__c;
						newOrder.Return_To_Country__c=newOrder.Bill_To_Country__c;
						newOrder.Return_To_Postal_Code__c=newOrder.Bill_To_Postal_Code__c;
					}
					if(newOrder.Bill_To_Contact__c==newOrder.Return_To_Contact__c)
					{
						newOrder.Return_To_Contact_Email__c=newOrder.Bill_To_Contact_Email__c;
						newOrder.Return_To_Contact_Phone__c=newOrder.Bill_To_Contact_Phone__c;
					}
				}
				if(newOrder.Return_To_Account__c!=null &&
				(newOrder.Bill_To_Account__c!=newOrder.Return_To_Account__c) &&
				(newOrder.Site_Account__c!=newOrder.Return_To_Account__c)
				)
				{
					setReturnToAccountIdsToRetrieve.add(newOrder.Return_To_Account__c);
					lstOrdersToUpdateReturnToAddress.add(newOrder);
				}
				
				if(newOrder.Return_To_Contact__c!=null &&
				(newOrder.Bill_To_Contact__c!=newOrder.Return_To_Contact__c) &&
				(newOrder.System_Administrator__c!=newOrder.Return_To_Contact__c)
				)
				{
					setReturnToContactIdsToRetrieve.add(newOrder.Return_To_Contact__c);
					lstOrdersToUpdateReturnToContact.add(newOrder);
				}
				
				//</change>
			}
			//<change>
			if(setReturnToAccountIdsToRetrieve.size()>0)
			{
				// get Site Address from Account and populate our Return Address
				// TODO : Change query below to get standard shipping address
				// TODO : Confirm name of Site Address fields on Order
				Map<Id,Account> mapIdAccount =new Map<Id,Account>([Select Id,
					Site_Address_1__c,Site_Address_2__c,Site_City__c,
					Site_State__c, Site_Postal_Code__c, Site_Country__c 
					from Account where Id in:setReturnToAccountIdsToRetrieve]);
/*
				Map<Id,Account> mapIdAccount =new Map<Id,Account>([Select Id,
					ShippingStreet, ShippingCity, ShippingState, 
					ShippingPostalCode, ShippingCountry  
					from Account where Id in:setReturnToAccountIdsToRetrieve]);
*/
				for(Order__c oneOrder:lstOrdersToUpdateReturnToAddress)
				{
					Account oneAccount = mapIdAccount.get(oneOrder.Return_To_Account__c);
					if(oneAccount!=null)
					{
						oneOrder.Return_To_Address_1__c  =  oneAccount.Site_Address_1__c;
						oneOrder.Return_To_Address_2__c  =  oneAccount.Site_Address_2__c;
						oneOrder.Return_To_City__c       =  oneAccount.Site_City__c;
						oneOrder.Return_To_State__c      =  oneAccount.Site_State__c;
						oneOrder.Return_To_Country__c    =  oneAccount.Site_Country__c;
						oneOrder.Return_To_Postal_Code__c = oneAccount.Site_Postal_Code__c;
						/* TODO : Need to add this in and remove above 
						oneOrder.Return_To_Address_1__c  =  oneAccount.ShippingStreet;
						oneOrder.Return_To_Address_2__c  =  null;
						oneOrder.Return_To_City__c       =  oneAccount.ShippingCity;
						oneOrder.Return_To_State__c      =  oneAccount.ShippingState;
						oneOrder.Return_To_Country__c    =  oneAccount.ShippingCountry;
						oneOrder.Return_To_Postal_Code__c = oneAccount.ShippingPostalCode;						
						*/
					}
				}
			}
				
			if(setReturnToContactIdsToRetrieve.size()>0)
			{
				Map<Id,Contact> mapIdContact =new Map<Id,Contact>([Select Id, Email,Phone,Name from Contact where Id in:setReturnToContactIdsToRetrieve]);
				for(Order__c oneOrder:lstOrdersToUpdateReturnToContact)
				{
					Contact oneContact = mapIdContact.get(oneOrder.Return_To_Contact__c);
					if(oneContact!=null)
					{
						oneOrder.Return_To_Contact_Email__c=oneContact.Email;
						oneOrder.Return_To_Contact_Phone__c=oneContact.Phone;
					}
				}
			}
			//</change>			
			Map<Id,User> mapIdUser = new Map<Id,User>([Select ManagerId, Manager.Email, UserRoleId,UserRole.Name from User where Id in :setOwnerId]);
			
			for (Order__c o : lstOrdersToUpdateOwnerDependencies) 
			{
				User owner =mapIdUser.get(o.OwnerId);
				o.Owner_Manager__c = owner.ManagerId;
				o.Owner_Manager_Email__c = owner.Manager.Email;
				o.Owner_Role__c = owner.UserRole.Name;				
			}
 		}
 	}

 	// update Opportunity to Closed/Won and all Order Products -> Opportunity Products
 	// when Order has been released for billing (sent to Intacct)
 	if (Trigger.isAfter) {    
 		if (Trigger.isUpdate) {
 			// if we are already in the update trigger, then return now to avoid recursion
 			if (OrderHelper.blnAlreadyInsideTrigger) {
 				return;
 			}
 			// BFS 11/19/2015 - added logic to handle Orders created by Order Schedule differently 
    		//  than those created by a Quote as follows:
    		// 1. If Order is created by Order Schedule, the Auto-Close process would work the same 
    		// as current state. (Orders created from an Order Schedule have a SFDC_520_Quote__c value
    		// 2. If the Order is created by a Quote (its Quote__c value will be non-null), 
    		// then the Auto-Close process will work as follows
    		// 	 	a. update the Opportunity Stage as “Stage 6 Closed Won” 
    		//		b. set the Close Date same as is true now
    		//		c. DO NOT copy Order Products to Opportunity Line Items
    		// If both Quote__c and SFDC_520_Quote__c are both defined, Quote__c rules (Rule #2) take precedence

	 		Map<Id, Order__c> mapOrdersForQuoteByOppId = new Map<Id, Order__c>();
	 		Map<Id, Order__c> mapOrdersForOSByOppId = new Map<Id, Order__c>();
 			List<Order__c> lstOrders = new List<Order__c>();
	 		for (Order__c order : Trigger.new) {
	 			if (order.Intacct_Order_ID__c != null && Trigger.oldMap.get(order.Id).Intacct_Order_ID__c == null) {
	 				System.debug('==========> Putting Order for Opportunity ' + 
	 					order.Opportunity__c + ' and Order ' + order.Id + ' into map.');
	 				if (order.Quote__c != null) {
	 					System.debug('Order created by a Quote - NOT copying Order Products.');
	 					mapOrdersForQuoteByOppId.put(order.Opportunity__c, order);
	 				} else if (order.SFDC_520_Quote__c != null) { 
	 					System.debug('Order created by an Order Schedule - Copying Order Products.');
		 				mapOrdersForOSByOppId.put(order.Opportunity__c, order);
	 				} else {
		 				//  TODO : what do we do if both quote fields are null?
	 					System.debug('Both quotes are null. Not handling order for billing.');
	 				}
	 			}
				// handle copying QLIs to Order Products - done in an update trigger because
				// process executes after insert triggers and that is where our field is set.	
	 			if (order.Copy_and_Merge_QLIs__c) {
		 			System.debug('======> Copying QLIs to Order Product for Order ' + order.Id);
	 				lstOrders.add(order);
	 			}
	 		}
	 		if (!mapOrdersForOSByOppId.isEmpty() || !mapOrdersForQuoteByOppId.isEmpty()) {
	 			OrderHelper.handleOrdersForBilling(mapOrdersForQuoteByOppId ,mapOrdersForOSByOppId);
	 		}
	 		if (!lstOrders.isEmpty()) {
		 		OrderHelper.copyQLIsToOrderProducts(lstOrders);
	 		}
	 	}
 	}
}